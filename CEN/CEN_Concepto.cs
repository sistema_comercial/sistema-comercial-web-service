﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Concepto
    {
        public int codConcepto { get; set; } 
        public int correlativo { get; set;  }
        public string descripcion { get; set; }

    }
    public class CEN_Data_Concepto
    {
        //DESCRIPCION: Entidad de precio
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Concepto concepto { get; set; }
        public List<CEN_Concepto> listConcepto { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Concepto()
        {
            concepto = new CEN_Concepto();
            ErrorWebSer = new CEN_ErrorWebSer();
            listConcepto = new List<CEN_Concepto>();
        }

    }
}
