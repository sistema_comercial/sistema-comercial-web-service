﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Localizacion
    {
        public int codPersona { get; set; } //Codigo de persona
        public string coordenadaX { get; set; }// Coordenada x
        public string coordenadaY { get; set; } //Coordenada y
        public string numDocumento { get; set; } //DNI/RUC/CE
        public string nombreCompleto { get; set; } //Nombre completo/RUC
        public CEN_Localizacion()
        {
            codPersona = CEN_Constantes.g_const_0;
            coordenadaX = CEN_Constantes.g_const_vacio;
            coordenadaY = CEN_Constantes.g_const_vacio;
            numDocumento = CEN_Constantes.g_const_vacio;
            nombreCompleto = CEN_Constantes.g_const_vacio;
        }

    }

    public class CEN_Data_Localizacion
    {
        //DESCRIPCION: data de localizacion
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Localizacion localizacion { get; set; }
        public List<CEN_Localizacion> listLocalizacion { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Localizacion()
        {
            localizacion = new CEN_Localizacion();
            ErrorWebSer = new CEN_ErrorWebSer();
            listLocalizacion = new List<CEN_Localizacion>();
        }

    }
}
