﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Parametro
    {
        public int codParametro { get; set; }
        public int tipo { get; set; }
        public int valorEntero1 { get; set; }
        public int valorEntero2 { get; set; }
        public string valorCaneda1 { get; set; }
        public string valorCaneda2 { get; set; }
        public double valorMoneda1 { get; set; }
        public double valorMoneda2 { get; set; }
        public float valorFloat1 { get; set; }
        public float valorFloat2 { get; set; }
        public DateTime valorFecha1 { get; set; }
        public DateTime valorFecha2 { get; set; }

    }

    public class CEN_Data_Parametro
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Parametro parametro { get; set; } //clase
        public List<CEN_Parametro> listParametro { get; set; } // lista de clase
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Parametro()
        {
            parametro = new CEN_Parametro();
            ErrorWebSer = new CEN_ErrorWebSer();
            listParametro = new List<CEN_Parametro>();
        }

    }
}
