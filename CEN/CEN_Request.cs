﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Request
    {
        public short proceso { get; set; }          //proceso 1: registro 2:modificacion
        public int codPersona { get; set; }         //codigo de persona
        public short tipoPersona { get; set; }      //tipo persona
        public short tipoDocumento { get; set; }    //tipo documento
        public string numDocumento { get; set; }    //numero documento
        public string ruc { get; set; }             //ruc
        public string razonSocial { get; set; }     //razon social
        public string nombres { get; set; }         //nombres
        public string apePaterno { get; set; }      //apellido paterno
        public string apeMaterno { get; set; }      //apellido materno
        public string direccion { get; set; }       //direccion
        public string correo { get; set; }          //correo
        public string telefono { get; set; }        //telefono
        public string celular { get; set; }         //celular
        public string ubigeo { get; set; }          //ubigeo
        public short ? ordenAtencion { get; set; }  //orden de atencion
        public short ? perfilCliente { get; set; }  //perfil del cliente
        public short ? clasificacion { get; set; }  //clasificacion
        public short frecuencia { get; set; }       //frecuencia
        public short tipoListaPrecio { get; set; }  //tipoListaPrecio
        public int codRuta { get; set; }            //codRuta
        public String coordenadaX { get; set; } //coordenadaX
        public String coordenadaY { get; set; } //coordenadaY
        public string usuario { get; set; }         //usuario
        public string ip { get; set; }              //direccion ip
        public string mac { get; set; }             //mac

       

    }
}
