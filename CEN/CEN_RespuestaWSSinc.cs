﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RespuestaWSSinc
    {
        public object ResultadoSincro { get; set; }               //Resultado de RespuestaWS
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaWSSinc()
        {
            ResultadoSincro = null;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }
}
