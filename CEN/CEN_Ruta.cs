﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Ruta
    {
        public int ntraRutas { get; set; }
        public int ntraUsuario { get; set; } //codigo de usuario
        public short correlativo { get; set; }
        public int orden { get; set; }
        public string abreviatura { get; set; }
        public string descRuta { get; set; }
    }

    public class CEN_Data_Ruta
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Ruta ruta { get; set; }
        public List<CEN_Ruta> listRutas { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Ruta()
        {
            ruta = new CEN_Ruta();
            ErrorWebSer = new CEN_ErrorWebSer();
            listRutas = new List<CEN_Ruta>();
        }

    }
}
