﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Request_Login
    {
        //DESCRIPCION: Parametros de entrada de metodo
        public string usuario { get; set; } //Usuario
        public string contra { get; set; } //Contraseña
        public int sucursal { get; set; } // sucursal
        public int tipo { get; set; } //Tipo de consulta
        public CEN_Request_Login()
        {
            usuario = CEN_Constantes.g_const_vacio;
            contra = CEN_Constantes.g_const_vacio;
            sucursal = CEN_Constantes.g_const_0;
            tipo = CEN_Constantes.g_const_0;
        }
    }

    public class CEN_RespuestaWSLogin
    {
        public string DescripcionResp { get; set; }               //Resultado de RespuestaWS
        public int respuesta { get; set; }
        public int ntraUsuario { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaWSLogin()
        {
            DescripcionResp = CEN_Constantes.g_const_vacio;
            respuesta = CEN_Constantes.g_const_0;
            ntraUsuario = CEN_Constantes.g_const_0;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }

    public class CEN_RequestLoginUsu
    {
        public int codUsuario { get; set; } //Codigo de usuario
        public short tipoLogueo { get; set; } // 1: web, 2: movil
        public short tipoRegistro { get; set; } // 1: registro inicio, 2: actualizacion fin
    }

    public class CEN_RespuestaWSLoginUsu
    {
        public string DescripcionResp { get; set; }               //Resultado de RespuestaWS

        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaWSLoginUsu()
        {
            DescripcionResp = CEN_Constantes.g_const_vacio;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }

    public class CEN_DetalleSesionUsuario
    {
        public long ntraLogueo { get; set; }
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public int contador { get; set; }
        public CEN_DetalleSesionUsuario()
        {
            ntraLogueo = CEN_Constantes.g_const_0;
            codigo = CEN_Constantes.g_const_0;
            mensaje = CEN_Constantes.g_const_vacio;
            contador = CEN_Constantes.g_const_0;
        }
    }

    public class CEN_DataSalidaLogin
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public CEN_DataSalidaLogin()
        {
            codigo = CEN_Constantes.g_const_0;
            mensaje = CEN_Constantes.g_const_vacio;
        }
    }

    public class CEN_LoginFallido
    {
        public int codUsu { get; set; }
        public short cantFall { get; set; }
        public DateTime fecha { get; set; }
        public string ip { get; set; }
        public string mac { get; set; }

        public CEN_LoginFallido()
        {
            codUsu = CEN_Constantes.g_const_0;
            cantFall = CEN_Constantes.g_const_0;
            fecha = DateTime.Now;
            ip = CEN_Constantes.g_const_vacio;
            mac = CEN_Constantes.g_const_vacio;
        }
    }




}
