﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Preventa
    {
        public int ntraPreventa { get; set; }
        public int codCliente { get; set; }
        public int codUsuario { get; set; }
        public int codPuntoEntrega { get; set; }
        public short tipoMoneda { get; set; }
        public short tipoVenta { get; set; }
        public short tipoDocumentoVenta { get; set; }
        //public DateTime fecha { get; set; }
        public string fecha { get; set; }
        public string fechaEntrega { get; set; }
        public string fechaPago { get; set; }
        public short flagRecargo { get; set; }
        public double recargo { get; set; }
        public double igv { get; set; }
        public double isc { get; set; }
        public double total { get; set; }
        public short estado { get; set; }
        public string usuario { get; set; }         //usuario
        public string ip { get; set; }              //direccion ip
        public string mac { get; set; }             //mac
        public int tipoDocumento { get; set; }
        public string numeroDocumento { get; set; }
        public short origenVenta { get; set; }
        public string horaEntrega { get; set; }
        public int codSucursal { get; set; }
        public short proceso { get; set; }

        public List<CEN_Detalle_Preventa> listDetPreventa { get; set; }
        public List<CEN_Preventa_Promocion> listPrevPromocion { get; set; }
        public List<CEN_Preventa_Descuento> listPrevDescuento { get; set; }

        public CEN_Preventa()
        {
            listDetPreventa = new List<CEN_Detalle_Preventa>();
            listPrevPromocion = new List<CEN_Preventa_Promocion>();
            listPrevDescuento = new List<CEN_Preventa_Descuento>();
        }
    }
    public class CEN_Data_Preventa
    {
        //DESCRIPCION: Entidad de preventa
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Preventa preventa { get; set; }
        public List<CEN_Preventa> listPreventa { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Preventa()
        {
            preventa = new CEN_Preventa();
            ErrorWebSer = new CEN_ErrorWebSer();
            listPreventa = new List<CEN_Preventa>();
        }

    }


    public class CEN_Detalle_Preventa
    {
        public int codPreventa { get; set; }
        public short itemPreventa { get; set; }
        public int codPresentacion { get; set; }
        public string codProducto { get; set; }
        public int codAlmacen { get; set; }
        public int cantidadPresentacion { get; set; }
        public int cantidadUnidadBase { get; set; }
        public double precioVenta { get; set; }
        public short TipoProducto { get; set; }
    }

    public class CEN_Data_Detalle_Preventa
    {
        //DESCRIPCION: Entidad 
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Detalle_Preventa det_preventa { get; set; }
        public List<CEN_Detalle_Preventa> listdet_Preventa { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Detalle_Preventa()
        {
            det_preventa = new CEN_Detalle_Preventa();
            ErrorWebSer = new CEN_ErrorWebSer();
            listdet_Preventa = new List<CEN_Detalle_Preventa>();
        }

    }

    public class CEN_Preventa_Promocion
    {
        public int codPreventa { get; set; }
        public int codPromocion { get; set; }
        public short itemPreventa { get; set; }
        public short itemPromocionado { get; set; }
    }
    public class CEN_Data_Preventa_Promocion
    {
        //DESCRIPCION: Entidad 
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Preventa_Promocion preventa_prom { get; set; }
        public List<CEN_Preventa_Promocion> list_Preventa_prom { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Preventa_Promocion()
        {
            preventa_prom = new CEN_Preventa_Promocion();
            ErrorWebSer = new CEN_ErrorWebSer();
            list_Preventa_prom = new List<CEN_Preventa_Promocion>();
        }

    }

    public class CEN_Preventa_Descuento
    {
        public int codPreventa { get; set; }
        public int codDescuento { get; set; }
        public short itemPreventa { get; set; }
        public double importe { get; set; }
    }

    public class CEN_Data_Preventa_Descuento
    {
        //DESCRIPCION: Entidad 
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Preventa_Descuento preventa_desc { get; set; }
        public List<CEN_Preventa_Descuento> list_Preventa_desc { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Preventa_Descuento()
        {
            preventa_desc = new CEN_Preventa_Descuento();
            ErrorWebSer = new CEN_ErrorWebSer();
            list_Preventa_desc = new List<CEN_Preventa_Descuento>();
        }

    }
    
    public class CEN_Data_Entrega
    {
        //DESCRIPCION: Clase de datos para obtener datos de la preventa.
        public int ntraAnt { get; set; } //Codigo de transaccion anterior
        public int ntraLocal { get; set; } //Numero de transaccion local
        public int ntraServ { get; set; } //Numero de transaccion servidor
        public CEN_Data_Entrega()
        {
            ntraLocal = CEN_Constantes.g_const_0;
            ntraServ = CEN_Constantes.g_const_0;
            ntraAnt = CEN_Constantes.g_const_0;
        }
    }

    public class CEN_Data_ClienteSinc
    {
        //DESCRIPCION: Clase de datos para obtener datos de la preventa.
        public int codCliente { get; set; } //Codigo de transaccion anterior
        public int codPuntoEntrega { get; set; } //Numero de transaccion local
        public int tipoPersona { get; set; }
        public string numDocumento { get; set; }
        public string ruc { get; set; }             //ruc
        //public int ntraServ { get; set; } //Numero de transaccion servidor
        public CEN_Data_ClienteSinc()
        {
            codCliente = CEN_Constantes.g_const_0;
            codPuntoEntrega = CEN_Constantes.g_const_0;
            tipoPersona = CEN_Constantes.g_const_0;
            numDocumento = CEN_Constantes.g_const_vacio;
            ruc = CEN_Constantes.g_const_vacio;
        }
    }


}
