﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
   public class CEN_RespuestaEstadoRutasAsigWS
    {
        public object ResultadoEstadoRA { get; set; }

        public CEN_ErrorWebSer ErrorWebSer { get; set; }

        public CEN_RespuestaEstadoRutasAsigWS()
        {
            ResultadoEstadoRA = null;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }
}
