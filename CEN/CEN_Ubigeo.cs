﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Departamento
    {
        public string codDepartamento { get; set; }
        public string nombre { get; set; }

        public CEN_Departamento()
        {
            codDepartamento = CEN_Constantes.g_const_vacio;
            nombre = CEN_Constantes.g_const_vacio;
        }
    }

    public class CEN_ListaDepartamento
    {
        public List<CEN_Departamento> listDepartamentos { get; set; }
        public CEN_ListaDepartamento()
        {
            listDepartamentos = new List<CEN_Departamento>();
        }

    }

    public class CEN_Provincia
    {
        public string codDepartamento { get; set; }
        public string codProvincia { get; set; }
        public string nombre { get; set; }
        public CEN_Provincia()
        {
            codDepartamento = CEN_Constantes.g_const_vacio;
            codProvincia = CEN_Constantes.g_const_vacio;
            nombre = CEN_Constantes.g_const_vacio;
        }
    }

    public class CEN_ListaProvincia
    {
        public List<CEN_Provincia> listProvincias { get; set; }
        public CEN_ListaProvincia()
        {
            listProvincias = new List<CEN_Provincia>();
        }
    }

    public class CEN_Distrito
    {
        public string codDepartamento { get; set; }
        public string codProvincia { get; set; }
        public string codDistrito { get; set; }
        public string nombre { get; set; }
        public string ubigeo { get; set; }
        public CEN_Distrito()
        {
            codDepartamento = CEN_Constantes.g_const_vacio;
            codProvincia = CEN_Constantes.g_const_vacio;
            codDistrito = CEN_Constantes.g_const_vacio;
            nombre = CEN_Constantes.g_const_vacio;
            ubigeo = CEN_Constantes.g_const_vacio;
        }
    }

    public class CEN_ListaDistrito
    {
        public List<CEN_Distrito> listDistritos { get; set; }
        public CEN_ListaDistrito()
        {
            listDistritos = new List<CEN_Distrito>();
        }
    }

    public class CEN_RequestUbigeo
    {
        public string codUbigeo { get; set; }

        public CEN_RequestUbigeo()
        {
            codUbigeo = CEN_Constantes.g_const_vacio;
        }
    }

    public class CEN_ResponseUbigeo
    {
        public List<CEN_Departamento> listDepartamentos { get; set; }
        public List<CEN_Provincia> listProvincias { get; set; }
        public List<CEN_Distrito> listDistritos { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_ResponseUbigeo()
        {
            listDepartamentos = new List<CEN_Departamento>();
            listProvincias = new List<CEN_Provincia>();
            listDistritos = new List<CEN_Distrito>();
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }

}
