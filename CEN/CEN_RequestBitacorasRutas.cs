﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RequestBitacorasRutas
    {
        public int codRuta { get; set; }
        public String codCliente { get; set; }
        public string fecha { get; set; }
        public int visita { get; set; }
        public string motivo { get; set; }
        public string usuario { get; set;}
        public string ip { get; set; }   
        public string mac { get; set; }
        public string cordenadaX { get; set; }     
        public string cordenadaY { get; set; }
        public Int16 estado { get; set; }
    }
}
