﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Almacen
    {
        public int ntraAlmacen { get; set; } 
        public string abreviatura { get; set; } 
        public string descripcion { get; set; } 
    }

    public class CEN_Data_Almacen
    {
        //DESCRIPCION: Entidad de almacen
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Almacen almacen { get; set; }
        public List<CEN_Almacen> listAlmacen { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Almacen()
        {
            almacen = new CEN_Almacen();
            ErrorWebSer = new CEN_ErrorWebSer();
            listAlmacen = new List<CEN_Almacen>();
        }

    }
}
