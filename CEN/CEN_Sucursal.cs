﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RespuestaWSSucursal
    {
        public object ResultadoSucursal { get; set; }               //Resultado de RespuestaWS
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaWSSucursal()
        {
            ResultadoSucursal = null;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }

    public class CEN_Response_Sucursal
    {
        //DESCRIPCION: Entidad de salida 
        public string DescripcionResp { get; set; } // Descripción de la respuesta

        public List<CEN_Sucursal> listSucursales { get; set; }

        public CEN_Response_Sucursal()
        {
            DescripcionResp = CEN_Constantes.g_const_vacio;
            listSucursales = new List<CEN_Sucursal>();
        }
    }

    public class CEN_Sucursal
    {
        public int ntraSucursal { get; set; }
        public string descripcion { get; set; }
        public string codUbigeo { get; set; }
        public double factor { get; set; }

        public CEN_Sucursal()
        {
            ntraSucursal = CEN_Constantes.g_const_0;
            descripcion = CEN_Constantes.g_const_vacio;
            codUbigeo = CEN_Constantes.g_const_vacio;
            factor = CEN_Constantes.g_const_0;
        }
    }
}
