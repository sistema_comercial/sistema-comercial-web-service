﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RequestSunat
    {
        public string numDocumento { get; set; }
    }
    public class CEN_Sunat
    {
        public string RUC { get; set; }
        public string razonSocial { get; set; }
        public string estCont { get; set; }
        public string condDom { get; set; }
        public string ubigeo { get; set; }
        public string tipVia { get; set; }
        public string nomVia { get; set; }
        public string codZona { get; set; }
        public string nomZona { get; set; }
        public string numero { get; set; }
        public string interior { get; set; }
        public string lote { get; set; }
        public string departamento { get; set; }
        public string manzana { get; set; }
        public string kilometro { get; set; }

        public string direccionFizcal { get; set; }

        public CEN_Sunat()
        {
            RUC = CEN_Constantes.g_const_vacio;
            razonSocial = CEN_Constantes.g_const_vacio;
            estCont = CEN_Constantes.g_const_vacio;
            condDom = CEN_Constantes.g_const_vacio;
            ubigeo = CEN_Constantes.g_const_vacio;
            tipVia = CEN_Constantes.g_const_vacio;
            nomVia = CEN_Constantes.g_const_vacio;
            codZona = CEN_Constantes.g_const_vacio;
            nomZona = CEN_Constantes.g_const_vacio;
            numero = CEN_Constantes.g_const_vacio;
            interior = CEN_Constantes.g_const_vacio;
            lote = CEN_Constantes.g_const_vacio;
            departamento = CEN_Constantes.g_const_vacio;
            manzana = CEN_Constantes.g_const_vacio;
            kilometro = CEN_Constantes.g_const_vacio;
            direccionFizcal = CEN_Constantes.g_const_vacio;
        }

        
    }

    public class CEN_ResponseClienSunat
    {
        //DESCRIPCION: Entidad de vendedor
        public CEN_Sunat dataSunat { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_ResponseClienSunat()
        {
            dataSunat = new CEN_Sunat();
            ErrorWebSer = new CEN_ErrorWebSer();
        }

    }
}
