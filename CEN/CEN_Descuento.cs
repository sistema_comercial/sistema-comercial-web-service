﻿using System;
using System.Collections.Generic;

namespace CEN
{
    public class CEN_Descuento
    {
        public int ntraDescuento { get; set; }
        public int flag { get; set; }
        public string ntra_flag { get; set; }
        public string desc_desc { get; set; }
        public string horaInicial { get; set; }
        public string horaFin { get; set; }
        public string desc_det { get; set; }
        public string valorInicial { get; set; }
        public string valorFinal { get; set; }
        public int detalle { get; set; }
        public int estado { get; set; }
        public int tipoDescuento { get; set; }
        public List<CEN_Detalle_Descuento> listDetalleDescuento { get; set; }
        public CEN_Descuento()
        {
            listDetalleDescuento = new List<CEN_Detalle_Descuento>();
        }
    }

    public class CEN_Detalle_Descuento
    {
        public string ntra_flag { get; set; }
        public int flag { get; set; }
        public string descripcion { get; set; }
        public int valorEntero1 { get; set; }
        public int valorEntero2 { get; set; }
        public double valorMoneda1 { get; set; }
        public double valorMoneda2 { get; set; }
        public string valorCadena1 { get; set; }
        public string valorCadena2 { get; set; }
        public DateTime valorFecha1 { get; set; }
        public DateTime valorFecha2 { get; set; }
        public int estado { get; set; }
    }

    public class CEN_Data_Descuento
    {
        //DESCRIPCION: Entidad de 
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Descuento descuento { get; set; }
        public List<CEN_Descuento> listDescuento { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Descuento()
        {
            descuento = new CEN_Descuento();
            ErrorWebSer = new CEN_ErrorWebSer();
            listDescuento = new List<CEN_Descuento>();
        }

    }


}
