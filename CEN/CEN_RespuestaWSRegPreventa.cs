﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RespuestaWSRegPreventa
    {
        public object ResultadoRegPreventa { get; set; }    //Resultado de RespuestaWS
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaWSRegPreventa()
        {
            ResultadoRegPreventa = null;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }
}
