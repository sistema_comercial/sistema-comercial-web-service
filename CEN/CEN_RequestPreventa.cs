﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RequestPreventa:CEN_Preventa
    {
       
        public CEN_Request cliente { get; set; }    //cliente
        public CEN_RequestPuntoEntrega entrega { get; set; }
    }

    public class CEN_RequestAnularPreventa
    {
        public int ntraPreventa { get; set; }
        public int codUsuario { get; set; }
        public CEN_RequestAnularPreventa()
        {
            ntraPreventa = CEN_Constantes.g_const_0;
            codUsuario = CEN_Constantes.g_const_0;
        }
    }

}
