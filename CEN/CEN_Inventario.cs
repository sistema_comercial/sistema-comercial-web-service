﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Inventario
    {
        public int ntraInventario { get; set; }
        public int codAlmacen { get; set; }
        public string codProducto { get; set; }
        public int stock { get; set; }
    }
    public class CEN_Data_Inventario
    {
        //DESCRIPCION: Entidad de inventario
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Inventario inventario { get; set; }
        public List<CEN_Inventario> listInventario { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Inventario()
        {
            inventario = new CEN_Inventario();
            ErrorWebSer = new CEN_ErrorWebSer();
            listInventario = new List<CEN_Inventario>();
        }

    }
}
