﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Constantes
    {
        public const string ConexionSQL = "SqlConexion";    // Conexión sql server
        public const int g_const_0 = 0;                     // Constante 0
        public const int g_const_1 = 1;                     // Constante 1
        public const int g_const_2 = 2;                     // Constante 2
        public const int g_const_3 = 3;                     // Constante 3
        public const int g_const_4 = 4;                     // Constante 4
        public const int g_const_5 = 5;                     // Constante 5 
        public const int g_const_6 = 6;                     // Constante 6
        public const int g_const_7 = 7;                     // Constante 7
        public const int g_const_8 = 8;                     // Constante 8
        public const int g_const_9 = 9;                     // Constante 9
        public const int g_const_10 = 10;                   // Constante 10
        public const int g_const_11 = 11;                   // Constante 11
        public const int g_const_12 = 12;                   // Constante 12


        public const int g_const_15 = 15;                   // Constante 15
        public const int g_const_20 = 20;                   // Constante 20
        public const int g_const_30 = 30;                   // Constante 30
        public const int g_const_50 = 50;                   // Constante 50
        public const int g_const_60 = 60;                   // Constante 60
        public const int g_const_100 = 100;                 // Constante 100
        public const int g_const_200 = 200;                 // Constante 200
        public const int g_const_2000 = 2000;               // Constante 2000
        public const int g_const_1000 = 1000;               // Constante 1000
        public const int g_const_1001 = 1001;               // Constante 1001
        public const int g_const_1002 = 1002;               // Constante 1002
        public const int g_const_1003 = 1003;               // Constante 1003
        public const int g_const_1004 = 1004;               // Constante 1004
        public const int g_const_1005 = 1005;               // Constante 1005
        public const int g_const_1006 = 1006;               // Constante 1006
        public const int g_const_1007 = 1007;               // Constante 1007
        public const int g_const_1008 = 1008;               // Constante 1008
        public const int g_const_1009 = 1009;               // Constante 1009
        public const int g_const_1010 = 1010;               // Constante 1010
        public const int g_const_1011 = 1011;               // Constante 1011
        public const int g_const_1012 = 1012;               // Constante 1012
        public const int g_const_1013 = 1013;               // Constante 1013
        public const int g_const_1014 = 1014;               // Constante 1014
        public const int g_const_1015 = 1015;               // Constante 1015
        public const int g_const_1016 = 1016;               // Constante 1016
        public const int g_const_1017 = 1017;               // Constante 1017
        public const int g_const_1018 = 1018;               // Constante 1018
        public const int g_const_1019 = 1019;               // Constante 1019
        public const int g_const_1020 = 1020;               // Constante 1020
        public const int g_const_1021 = 1021;               // Constante 1021
        public const int g_const_1022 = 1022;               // Constante 1022
        public const int g_const_1023 = 1023;               // Constante 1023
        public const int g_const_1024 = 1024;               // Constante 1024
        public const int g_const_1025 = 1025;               // Constante 1025
        public const int g_const_1026 = 1026;               // Constante 1026
        public const int g_const_1027 = 1027;               // Constante 1027
        public const int g_const_1028 = 1028;               // Constante 1028
        public const int g_const_1029 = 1029;               // Constante 1029
        public const int g_const_1030 = 1030;               // Constante 1030
        public const int g_const_1031 = 1031;               // Constante 1031
        public const int g_const_1032 = 1032;               // Constante 1032
        public const int g_const_1033 = 1033;               // Constante 1033
        public const int g_const_1034 = 1034;               // Constante 1034
        public const int g_const_1035 = 1035;               // Constante 1035
        public const int g_const_1036 = 1036;               // Constante 1036
        public const int g_const_1037 = 1037;               // Constante 1037
        public const int g_const_1038 = 1038;               // Constante 1038
        public const int g_const_1039 = 1039;               // Constante 1039
        public const int g_const_1041 = 1041;               // Constante 1041
        public const int g_const_1042 = 1042;               // Constante 1042
        public const int g_const_1043 = 1043;               // Constante 1043
        public const int g_const_1044 = 1044;               // Constante 1044
        public const int g_const_3000 = 3000;               // Constante 3000
        public const string g_const_vacio = "";             // vacio
        public const string g_const_espacio = " ";          // espacio

        public const int g_const_101 = 101;
        public const int g_const_menos1 = -1;
        public const string g_const_guion = "-";
        public const string g_const_formfech = "dd/MM/yyyy"; //Constante para formato de fecha
        public const string g_const_es_PE = "es-PE";
        public const string g_const_formHora = "HH:mm:ss"; //Constante para formato de fecha
        public const string g_const_consultaExitosa = "Consulta exitosa"; //Constante de consulta exitosa
        public const string g_const_keyLogin = "keyUserLogin";    // Clave de login

        public const int g_const_1040 = 1040; //Constante 1040
        public const string g_const_var_intento = ", INTENTO "; //Constante de intento
        public const string g_const_slash = "/"; //Constante /
        public const string g_const_00 = "00";                     // Constante 00
        public const int g_const_1045 = 1045; //Constante 1045
    }
}
