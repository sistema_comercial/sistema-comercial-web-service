﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: CEN_Vendedor.cs
 VERSION : 1.0
 OBJETIVO: Clase entidad de vendedor 
 FECHA   : 14/01/2020
 AUTOR   : Wilmer Sanchez - IS
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |       USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System.Collections.Generic;

namespace CEN
{
    public class CEN_Vendedor
    {
        //DESCRIPCION: Entidad de vendedor
        public int ntraUsuario { get; set; } //codigo de usuario
        public int codPerfil { get; set; } //Codigo de perfil
        public short tipoDocumento { get; set; } // tipo de documento
        public string numeroDocumento { get; set; } // numero de documento
        public string apellidoPaterno { get; set; } // apellido paterno
        public string apellidoMaterno { get; set; } // apellido materno
        public string nombres { get; set; } // nombres
        public string nombreCompleto { get; set; } // nombre completo
        public string direccion { get; set; } // direccion
        public string correo { get; set; } //Correo
        public string telefono { get; set; } //Telefono
        public string celular { get; set; } //Celular
        public int codRuta { get; set; } // codRuta 
        public string descRuta { get; set; } //Descripcion de la ruta


    }
    public class CEN_Data_Vendedor
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Vendedor vendedor { get; set; }
        public List<CEN_Vendedor> listVendedor { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Vendedor()
        {
            vendedor = new CEN_Vendedor();
            ErrorWebSer = new CEN_ErrorWebSer();
            listVendedor = new List<CEN_Vendedor>();
        }

    }


}
