﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Response_Sincronizacion
    {
        //DESCRIPCION: Entidad de salida de sincronizacion
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public int estadoUsuario { get; set; } // Estado de usuario

        public CEN_Vendedor vendedor { get; set; }
        public List<CEN_Categoria> listCategorias { get; set; }
        public List<CEN_Producto> listProductos { get; set; }
        public List<CEN_Presentacion> listPresentaciones { get; set; }
        public List<CEN_Cliente> listClientes { get; set; }
        public List<CEN_Promocion> listPromociones { get; set; }
        public List<CEN_Promocion> listPromocionesDesc { get; set; }
        public List<CEN_Descuento> listDescuentos { get; set; }
        public List<CEN_Almacen> listAlmacenes { get; set; }
        public List<CEN_Inventario> listInventarios { get; set; }
        public List<CEN_PuntosEntrega> listEntregas { get; set; }
        public List<CEN_Preventa> listPreventas { get; set; }
        public List<CEN_Precio> listPrecios { get; set; }
        public List<CEN_Concepto> listConceptos { get; set; }
        public List<CEN_Parametro> listParametros { get; set; }
        public List<CEN_Localizacion> listLocalizacion { get; set; }



        public CEN_Response_Sincronizacion()
        {
            DescripcionResp = CEN_Constantes.g_const_vacio;
            estadoUsuario = CEN_Constantes.g_const_0;
            vendedor = new CEN_Vendedor();
            listCategorias = new List<CEN_Categoria>();
            listProductos = new List<CEN_Producto>();
            listPresentaciones = new List<CEN_Presentacion>();
            listClientes = new List<CEN_Cliente>();
            listPromociones = new List<CEN_Promocion>();
            listPromocionesDesc = new List<CEN_Promocion>();
            listAlmacenes = new List<CEN_Almacen>();
            listInventarios = new List<CEN_Inventario>();
            listDescuentos = new List<CEN_Descuento>();
            listEntregas = new List<CEN_PuntosEntrega>();
            listPreventas = new List<CEN_Preventa>();
            listPrecios = new List<CEN_Precio>();
            listConceptos = new List<CEN_Concepto>();
            listParametros = new List<CEN_Parametro>();
            listLocalizacion = new List<CEN_Localizacion>();
        }
    }
}
