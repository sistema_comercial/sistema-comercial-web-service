﻿using System.Collections.Generic;

namespace CEN
{
    public class CEN_Precio
    {
        public int ntraPrecio { get; set; }
        public string codProducto { get; set; }
        public short tipoListaPrecio { get; set; }
        public double precioVenta { get; set; }
        public string descripcion { get; set; }
    }
    public class CEN_Data_Precio
    {
        //DESCRIPCION: Entidad de precio
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Precio precio { get; set; }
        public List<CEN_Precio> listPrecio { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Precio()
        {
            precio = new CEN_Precio();
            ErrorWebSer = new CEN_ErrorWebSer();
            listPrecio = new List<CEN_Precio>();
        }

    }
}
