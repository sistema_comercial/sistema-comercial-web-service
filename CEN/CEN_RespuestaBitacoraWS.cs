﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RespuestaBitacoraWS
    {
        public object ResultadoBitacora { get; set; }               //Resultado de RespuestaWS
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaBitacoraWS()
        {
            ResultadoBitacora = null;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }
}
