﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Request_Sincronizacion
    {
        public int codVendedor { get; set; }      //Codigo de vendedor
        public string codUbigeo { get; set; } // Codigo ubigeo de la sucursal
        public List<CEN_Request> listClientes { get; set; } //Lista de clientes
        public List<CEN_Preventa> listPreventas { get; set; } //Lista de preventas
        public List<CEN_RequestPuntoEntrega> listEntregas { get; set; } //Lista de punto de entregas
        public List<CEN_RequestBitacorasRutas> listaBitacoras { get; set; } //Lista de bitacoras
    }
}
