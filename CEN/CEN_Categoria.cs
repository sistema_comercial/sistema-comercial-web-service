﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Categoria
    {
        public int ntraCategoria { get; set; } //Codigo de perfil
        public string descripcion { get; set; } // numero de documento
    }
    public class CEN_Data_Categoria
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Categoria categoria { get; set; }
        public List<CEN_Categoria> listCategoria { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Categoria()
        {
            categoria = new CEN_Categoria();
            ErrorWebSer = new CEN_ErrorWebSer();
            listCategoria = new List<CEN_Categoria>();
        }

    }
}
