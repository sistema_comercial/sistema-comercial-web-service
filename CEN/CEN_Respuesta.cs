﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    class CEN_Respuesta
    {
    }

    public class RptaRegistroCliente
    {
        public int codCliente { get; set; } //codigo de cliente
        public int codPuntoEntrega { get; set; } //codigo de cliente
    }

    public class RptaRegistroPreventa
    {
        public int ntraPreventa { get; set; } //numero de transaccion preventa
        public int codPersona { get; set; } //codigo persona
        public int ntraPuntoEntrega { get; set; } //numero de transaccion punto entrega
    }

    public class RptaRegistroPuntoEntrega
    {
        public int ntraPuntoEntrega { get; set; } //numero de transaccion punto entrega
    }
}
