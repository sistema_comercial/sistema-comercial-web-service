﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_PuntosEntrega
    {
        public int ntraPuntoEntrega { get; set; }
        public int idLocal { get; set; }
        public string coordenadaX { get; set; }
        public string coordenadaY { get; set; }
        public string codUbigeo { get; set; }
        public string direccion { get; set; }
        public string referencia { get; set; }
        public short ordenEntrega { get; set; }
        public int codPersona { get; set; }
        public int tipoDocumento { get; set; }
        public string numeroDocumento { get; set; }
        public string usuario { get; set; }         //usuario
        public string ip { get; set; }              //direccion ip
        public string mac { get; set; }             //mac
    }
    public class CEN_Data_PuntosEntrega
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_PuntosEntrega entrega { get; set; }
        public List<CEN_PuntosEntrega> listEntrega { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_PuntosEntrega()
        {
            entrega = new CEN_PuntosEntrega();
            ErrorWebSer = new CEN_ErrorWebSer();
            listEntrega = new List<CEN_PuntosEntrega>();
        }

    }
}
