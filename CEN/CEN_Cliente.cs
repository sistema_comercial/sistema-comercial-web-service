﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Cliente
    {
        public int codPersona { get; set; } //Codigo de perfil
        public int tipoPersona { get; set; } //Codigo de perfil
        public short tipoDocumento { get; set; } //Codigo de perfil
        public string numeroDocumento { get; set; } // numero de documento
        public string ruc { get; set; } // numero de documento
        public string razonSocial { get; set; } // numero de documento
        public string nombres { get; set; } // numero de documento
        public string apellidoPaterno { get; set; } // numero de documento
        public string apellidoMaterno { get; set; } // numero de documento
        public string nombreCodigo { get; set; } // numero de documento
        public string direccion { get; set; } // numero de documento
        public string correo { get; set; } // numero de documento
        public string telefono { get; set; } // numero de documento
        public string celular { get; set; } // numero de documento
        public int frecuenciaCliente { get; set; } // frecuenciaCliente
        public int tipoListaPrecio { get; set; } // tipoListaPrecio
        public int codRuta { get; set; } // codRuta
        public int ordenAtencion { get; set; } // ordenAtencion
        public string codUbigeo { get; set; } //Codigo de ubigeo
    }

    public class CEN_Data_Cliente
    {
        //DESCRIPCION: Entidad de cliente
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Cliente cliente { get; set; }
        public List<CEN_Cliente> listCliente { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Cliente()
        {
            cliente = new CEN_Cliente();
            ErrorWebSer = new CEN_ErrorWebSer();
            listCliente = new List<CEN_Cliente>();
        }

    }
}
