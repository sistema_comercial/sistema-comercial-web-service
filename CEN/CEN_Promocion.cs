﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Promocion
    {
        public int ntraPromocion { get; set; }
        public int flag { get; set; }
        public string ntra_flag { get; set; }
        public string desc_promo { get; set; }
        public string horaInicial { get; set; }
        public string horaFin { get; set; }
        public string desc_det { get; set; }
        public string valorInicial { get; set; }
        public string valorFinal { get; set; }
        public int detalle { get; set; }
        public int estado { get; set; }
        public List<CEN_Detalle_Promocion> listDetallePromocion { get; set; }
        public CEN_Promocion()
        {
            listDetallePromocion = new List<CEN_Detalle_Promocion>();
        }
    }

    public class CEN_Detalle_Promocion
    {
        public string ntra_flag { get; set; }
        public int flag { get; set; }
        public string descripcion { get; set; }
        public int valorEntero1 { get; set; }
        public int valorEntero2 { get; set; }
        public double valorMoneda1 { get; set; }
        public double valorMoneda2 { get; set; }
        public string valorCadena1 { get; set; }
        public string valorCadena2 { get; set; }
        public DateTime valorFecha1 { get; set; }
        public DateTime valorFecha2 { get; set; }
        public int estado { get; set; }
    }

    public class CEN_Data_Promocion
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Promocion promocion { get; set; }
        public List<CEN_Promocion> listPromocion { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Promocion()
        {
            promocion = new CEN_Promocion();
            ErrorWebSer = new CEN_ErrorWebSer();
            listPromocion = new List<CEN_Promocion>();
        }

    }


}
