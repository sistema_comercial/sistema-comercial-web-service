﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_RespuestaWS
    {
        public object Resultado { get; set; }               //Resultado de RespuestaWS
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaWS()
        {
            Resultado = null;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }

    public class CEN_RespuestaW
    {
        public int Resultado { get; set; }               //Resultado de RespuestaWS
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService
        public CEN_RespuestaW()
        {
            Resultado = CEN_Constantes.g_const_0;
            ErrorWebSer = new CEN_ErrorWebSer();
        }
    }
}
