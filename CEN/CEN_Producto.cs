﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CEN
{
    public class CEN_Producto
    {
        public string codProducto { get; set; } //Codigo de producto
        public string descripcion { get; set; } // descripcion 
        public string cod_des { get; set; } // union de codigo mas descripcion del producto
        public int codCategoria { get; set; }
        public int codUnidadBaseventa { get; set; }
        public int tipoProducto { get; set; } //Tipo de producto

    }

    public class CEN_Data_Producto
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Producto producto { get; set; }
        public List<CEN_Producto> listProducto { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Producto()
        {
            producto = new CEN_Producto();
            ErrorWebSer = new CEN_ErrorWebSer();
            listProducto = new List<CEN_Producto>();
        }

    }

    public class CEN_Presentacion
    {
        public string codProducto { get; set; } //Codigo de producto
        public int codPresentancion { get; set; }
        public int cantidadUnidadBase { get; set; }
    }
    public class CEN_Data_Presentacion
    {
        //DESCRIPCION: Entidad de vendedor
        public string DescripcionResp { get; set; } // Descripción de la respuesta
        public CEN_Presentacion presentacion { get; set; }
        public List<CEN_Presentacion> listPresentacion { get; set; }
        public CEN_ErrorWebSer ErrorWebSer { get; set; }    //ErrorWerbService

        public CEN_Data_Presentacion()
        {
            presentacion = new CEN_Presentacion();
            ErrorWebSer = new CEN_ErrorWebSer();
            listPresentacion = new List<CEN_Presentacion>();
        }

    }
}
