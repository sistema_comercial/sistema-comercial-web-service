﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: DistribuidoraController.cs
 VERSION : 1.0
 OBJETIVO: Clase controller con los métodos 
 FECHA   : 03/01/2020
 AUTOR   : Wilmer Sanchez - IS
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |       USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using CEN;
using CLN;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace ws_wa_DistribuidoraVDC.Controllers
{
    public class DistribuidoraController : ApiController
    {
        [HttpGet]
        [Route("ExisteRecurso")]
        [ActionName("ExisteRecurso")]
        public bool ExisteRecurso()
        {
            //DESCRIPCION : MÉTODO DEL SERVICIO UTILIZADO SOLO PARA VALIDAR SI EXISTE EL RECURSO DE LOS WEB SERVICES
            return true;
        }


        [HttpPost]
        [ActionName("RegistrarModificarCliente")]
        public IHttpActionResult RegistrarModificarCliente(CEN_Request DatosCliente)
        {
            //DESCRIPCION: registro de cliente
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaWS rpt = new CEN_RespuestaWS();
            CLN_Cliente cliente = new CLN_Cliente();

            int contUsu = CEN_Constantes.g_const_0; //Validar si el usuario esta activo o no
            CLN_Usuario cln_usuario = new CLN_Usuario();
            RptaRegistroCliente rc = new RptaRegistroCliente();
            CLN_Concepto cln_concepto = new CLN_Concepto();
            try
            {
                contUsu = cln_usuario.ValidarUsuarioActivo(Int32.Parse(DatosCliente.usuario));
                if (contUsu == CEN_Constantes.g_const_1)
                {
                    rpt = cliente.registro_modificacion_cliente(DatosCliente, JsonConvert.SerializeObject(DatosCliente));

                }
                else
                {
                    //USUARIO NO SE ENCUENTRA ACTIVO
                    CEN_Concepto desConcepto = new CEN_Concepto();
                    desConcepto.codConcepto = CEN_Constantes.g_const_100;
                    desConcepto.correlativo = CEN_Constantes.g_const_1045;
                    String descConcepto = CEN_Constantes.g_const_vacio;
                    descConcepto = cln_concepto.obtener_descripcion_concepto_general(desConcepto);
                    rpt.Resultado = new { reg_mod = rc };
                    rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    rpt.ErrorWebSer.CodigoErr = desConcepto.correlativo;
                    rpt.ErrorWebSer.DescripcionErr = descConcepto;
                }
                result = Ok(rpt);
            }
            catch(Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("Sincronizacion")]
        public IHttpActionResult Sincronizacion(CEN_Request_Sincronizacion DatosCliente)
        {
            //DESCRIPCION: Sincronizacion de movil
            IHttpActionResult result;
            CLN_Sincronizacion sincronizacion = new CLN_Sincronizacion();
            CEN_Response_Sincronizacion data_sincronizacion = new CEN_Response_Sincronizacion();
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaWSSinc rpt = new CEN_RespuestaWSSinc();
            CEN_RespuestaBitacoraWS rpt_bitacora = new CEN_RespuestaBitacoraWS();
            CEN_RespuestaWS rpta2 = new CEN_RespuestaWS();
            CEN_RespuestaWS rpta3 = new CEN_RespuestaWS();
            CEN_RespuestaWS rpta_entrega = new CEN_RespuestaWS();
            CEN_RespuestaWS rpta3_preventa = new CEN_RespuestaWS();
            CLN_BitacorasRutas SinRuBi = new CLN_BitacorasRutas();
            List<CEN_Data_Entrega> list_ent = new List<CEN_Data_Entrega>();

            CEN_RespuestaWSLogin rptLogin = new CEN_RespuestaWSLogin();
            CLN_Login cln_login = new CLN_Login();
            CEN_Request_Login request = new CEN_Request_Login();

            List<CEN_Data_ClienteSinc> listClienteSinc = new List<CEN_Data_ClienteSinc>();

            CLN_Usuario cln_usuario = new CLN_Usuario();
            CLN_Concepto cLN_Concepto = new CLN_Concepto();
            int contUsu = CEN_Constantes.g_const_0; //Validar si el usuario esta activo o no
            try
            {

                contUsu = cln_usuario.ValidarUsuarioActivo(DatosCliente.codVendedor);
                if (contUsu == CEN_Constantes.g_const_1)
                {

                    //USUARIO ACTIVO
                    request.tipo = CEN_Constantes.g_const_3;
                    rptLogin = cln_login.ValidarLogin(request, DatosCliente.codVendedor);

                    if (rptLogin.respuesta == CEN_Constantes.g_const_1 && rptLogin.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                    {
                        //Registro de clientes
                        if (DatosCliente.listClientes != null)
                        {
                            listClienteSinc = sincronizacion.registroClienteSincro(DatosCliente.listClientes);
                        }
                        //Registro de entregas
                        /*
                        if (DatosCliente.listEntregas != null)
                        {
                            list_ent = sincronizacion.registroPunEntregaSincro(DatosCliente.listEntregas);
                        }
                        */
                        //Registro de bitacoras
                        if (DatosCliente.listaBitacoras != null)
                        {
                            for (int i = CEN_Constantes.g_const_0; i < DatosCliente.listaBitacoras.Count; i++)
                            {
                                rpt_bitacora = SinRuBi.RegistrarRutasBitacoras(DatosCliente.listaBitacoras[i], JsonConvert.SerializeObject(DatosCliente.listaBitacoras[i]));

                            }
                        }
                        //Registro de preventas
                        if (DatosCliente.listPreventas != null)
                        {
                            rpta3 = sincronizacion.registroPreventaSincro(DatosCliente.listPreventas, list_ent, listClienteSinc);
                        }

                        rpt = sincronizacion.Sincronizacion(DatosCliente, rptLogin.respuesta);

                    }
                    else
                    {
                        //USUARIO NO ACTIVO
                        data_sincronizacion.estadoUsuario = rptLogin.respuesta;
                        rpt.ResultadoSincro = new { data_sincronizacion };
                        rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                        rpt.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_consultaExitosa;
                    }

                }
                else
                {
                    //USUARIO NO SE ENCUENTRA ACTIVO
                    CEN_Concepto desConcepto = new CEN_Concepto();
                    desConcepto.codConcepto = CEN_Constantes.g_const_100;
                    desConcepto.correlativo = CEN_Constantes.g_const_1045;
                    String descConcepto = CEN_Constantes.g_const_vacio;
                    descConcepto = cLN_Concepto.obtener_descripcion_concepto_general(desConcepto);
                    rpt.ResultadoSincro = new { data_sincronizacion };
                    rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    rpt.ErrorWebSer.CodigoErr = desConcepto.correlativo;
                    rpt.ErrorWebSer.DescripcionErr = descConcepto;
                }




                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("ObtenerDataUbigeo")]
        public IHttpActionResult ObtenerDataUbigeo(CEN_RequestUbigeo request)
        {
            //DESCRIPCION: Lista de sucursales.
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_ResponseUbigeo rpt = new CEN_ResponseUbigeo();
            CLN_Ubigeo cln_ubigeo = new CLN_Ubigeo();
            try
            {
                rpt = cln_ubigeo.ObtenerUbigeoData(request);
                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("ConsultaRUCSunatBD")]
        public IHttpActionResult ConsultaRUCSunatBD(CEN_RequestSunat request)
        {
            //DESCRIPCION: Consulta de RUC en la base de datos.
            IHttpActionResult result;
            CEN_ResponseClienSunat rpt = new CEN_ResponseClienSunat();
            CLN_Sunat cln_sunat = new CLN_Sunat();
            try
            {
                rpt = cln_sunat.ObtenerDataSunatBD(request);
                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("ObtenerSucursales")]
        public IHttpActionResult ObtenerSucursales()
        {
            //DESCRIPCION: Lista de sucursales.
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaWSSucursal rpt = new CEN_RespuestaWSSucursal();
            CLN_Sucursal cln_sucursal = new CLN_Sucursal();
            try
            {
                rpt = cln_sucursal.ObtenerSucursales();
                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("ValidarLogin")]
        public IHttpActionResult ValidarLogin(CEN_Request_Login request)
        {
            //DESCRIPCION: Lista de sucursales.
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaWSLogin rpt = new CEN_RespuestaWSLogin();
            CLN_Login cln_login = new CLN_Login();
            try
            {

                rpt = cln_login.ValidarLogin(request, CEN_Constantes.g_const_0);


                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("ControlSesioninUsu")]
        public IHttpActionResult ControlSesioninUsu(CEN_RequestLoginUsu request)
        {
            //DESCRIPCION: Control de usuario.
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaWSLoginUsu rpt = new CEN_RespuestaWSLoginUsu();
            CLN_Login cln_login = new CLN_Login();
            try
            {

                rpt = cln_login.ControlSesioninUsu(request);

                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("RegistrarRutasBitacoras")]
        public IHttpActionResult RegistrarRutasBitacoras(CEN_RequestBitacorasRutas ObjBitRu)
        {
            //DESCRIPCION: registro de rutas en bitacoras.
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaBitacoraWS rpt = new CEN_RespuestaBitacoraWS();
            CLN_BitacorasRutas bitRu = new CLN_BitacorasRutas();
            try
            {
                rpt = bitRu.RegistrarRutasBitacoras(ObjBitRu, JsonConvert.SerializeObject(ObjBitRu));
                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("ActualizarEstadoRutaAsignada")]
        public IHttpActionResult ActualizarEstadoRutaAsignada(CEN_RequestEstadoRutasAsig ObjUpdateRA)
        {
            //DESCRIPCION: registro de rutas en bitacoras.
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaEstadoRutasAsigWS rpt = new CEN_RespuestaEstadoRutasAsigWS();
            CLN_ActualizarEstadoRutaAsignada AcbitRu = new CLN_ActualizarEstadoRutaAsignada();
            try
            {
                rpt = AcbitRu.ActualizarEstadoRutaAsignada(ObjUpdateRA, JsonConvert.SerializeObject(ObjUpdateRA));
                result = Ok(rpt);
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("SincronizarRutasBitacora")]
        public IHttpActionResult SincronizarRutaBitacora(CEN_Request_Sincronizacion_Rutas ObjSinRB)
        {
            //DESCRIPCION: registro de rutas en bitacoras.
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaBitacoraWS rpt = new CEN_RespuestaBitacoraWS();
            CLN_BitacorasRutas SinRuBi = new CLN_BitacorasRutas();
            try
            {
                if (ObjSinRB.listaBitacoras != null)
                {
                    for (int i = CEN_Constantes.g_const_0; i < ObjSinRB.listaBitacoras.Count; i++)
                    {
                        rpt = SinRuBi.RegistrarRutasBitacoras(ObjSinRB.listaBitacoras[i], JsonConvert.SerializeObject(ObjSinRB.listaBitacoras[i]));

                    }
                    //result = Ok(rpt);
                }
                result = Ok(rpt);


            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }


        [HttpPost]
        [ActionName("RegistrarModificarPreventa")]
        public IHttpActionResult RegistrarModificarPreventa(CEN_RequestPreventa requestPreventa)
        {
            //DESCRIPCION: registro de preventa
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaWSRegPreventa rpt = new CEN_RespuestaWSRegPreventa();
            CLN_Preventa preventa = new CLN_Preventa();
            RptaRegistroPreventa rp = new RptaRegistroPreventa(); 

            CLN_Usuario cln_usuario = new CLN_Usuario();
            CLN_Concepto cLN_Concepto = new CLN_Concepto();
            int contUsu = CEN_Constantes.g_const_0; //Validar si el usuario esta activo o no

            try
            {
                if (requestPreventa != null)
                {
                    contUsu = cln_usuario.ValidarUsuarioActivo(Int32.Parse(requestPreventa.usuario));
                    if (contUsu == CEN_Constantes.g_const_1)
                    {
                        rpt = preventa.registro_modificacion_preventa(requestPreventa, JsonConvert.SerializeObject(requestPreventa));
                        
                    }
                    else
                    {
                        //USUARIO NO SE ENCUENTRA ACTIVO
                        CEN_Concepto desConcepto = new CEN_Concepto();
                        desConcepto.codConcepto = CEN_Constantes.g_const_100;
                        desConcepto.correlativo = CEN_Constantes.g_const_1045;
                        String descConcepto = CEN_Constantes.g_const_vacio;
                        descConcepto = cLN_Concepto.obtener_descripcion_concepto_general(desConcepto);
                        rpt.ResultadoRegPreventa = new { rp };
                        rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        rpt.ErrorWebSer.CodigoErr = desConcepto.correlativo;
                        rpt.ErrorWebSer.DescripcionErr = descConcepto;
                    }
                    result = Ok(rpt);
                }
                else
                {
                    rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                    rpt.ErrorWebSer.DescripcionErr = "PARAMETROS NULOS";
                    result = Content(HttpStatusCode.BadRequest, rpt);
                }
               
                
            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }

        [HttpPost]
        [ActionName("AnularPreventa")]
        public IHttpActionResult AnularPreventa(CEN_RequestAnularPreventa requestAP)
        {
            //DESCRIPCION: registro de preventa
            IHttpActionResult result;
            CEN_ErrorWebSer ErrorWebSer = new CEN_ErrorWebSer();
            CEN_RespuestaWS rpt = new CEN_RespuestaWS();
            CLN_Preventa preventa = new CLN_Preventa();

            CLN_Usuario cln_usuario = new CLN_Usuario();
            CLN_Concepto cLN_Concepto = new CLN_Concepto();
            int contUsu = CEN_Constantes.g_const_0; //Validar si el usuario esta activo o no

            try
            {
                if (requestAP != null)
                {
                    contUsu = cln_usuario.ValidarUsuarioActivo(requestAP.codUsuario);
                    if (contUsu == CEN_Constantes.g_const_1)
                    {
                        rpt = preventa.anular_preventa(requestAP, JsonConvert.SerializeObject(requestAP));
                        
                    }
                    else
                    {
                        //USUARIO NO SE ENCUENTRA ACTIVO
                        CEN_Concepto desConcepto = new CEN_Concepto();
                        desConcepto.codConcepto = CEN_Constantes.g_const_100;
                        desConcepto.correlativo = CEN_Constantes.g_const_1045;
                        String descConcepto = CEN_Constantes.g_const_vacio;
                        descConcepto = cLN_Concepto.obtener_descripcion_concepto_general(desConcepto);
                        rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        rpt.ErrorWebSer.CodigoErr = desConcepto.correlativo;
                        rpt.ErrorWebSer.DescripcionErr = descConcepto;
                    }
                    result = Ok(rpt);
                }
                else
                {
                    rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                    rpt.ErrorWebSer.DescripcionErr = "PARAMETROS NULOS";
                    result = Content(HttpStatusCode.BadRequest, rpt);
                }


            }
            catch (Exception ex)
            {
                rpt.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                rpt.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                rpt.ErrorWebSer.DescripcionErr = ex.Message + CEN_Constantes.g_const_espacio + ex.StackTrace;
                result = Content(HttpStatusCode.BadRequest, rpt);
            }
            return result;
        }


    }
}
