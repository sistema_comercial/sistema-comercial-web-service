﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CAD_Cliente
    {
        private SqlConnection Connection;
        public CEN_RespuestaWS registro_modificacion_cliente(CEN_Request DatosCliente)
        {
            //DESCRIPCION: REGISTRAR CLIENTE
            CAD_Conector CadCx = new CAD_Conector();            //conexion
            SqlDataReader dr;                                   //data reader
            CEN_RespuestaWS Respuesta = new CEN_RespuestaWS();  //clase de registro de respuesta
            RptaRegistroCliente rc = null;                      //clase respuesta
            try
            {
                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    using (SqlCommand Command = new SqlCommand("pa_registrar_modificar_cliente", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_proceso", SqlDbType.TinyInt).Value = DatosCliente.proceso;
                        Command.Parameters.Add("@p_codigo", SqlDbType.Int).Value = DatosCliente.codPersona;
                        Command.Parameters.Add("@p_tipoPersona", SqlDbType.TinyInt).Value = DatosCliente.tipoPersona;
                        Command.Parameters.Add("@p_tipoDocumento", SqlDbType.TinyInt).Value = DatosCliente.tipoDocumento;
                        if (DatosCliente.numDocumento == null)
                            Command.Parameters.Add("@p_numDocumento", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_numDocumento", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = DatosCliente.numDocumento.Trim();

                        if (DatosCliente.ruc == null)
                            Command.Parameters.Add("@p_ruc", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_ruc", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = DatosCliente.ruc.Trim();

                        if (DatosCliente.razonSocial == null)
                            Command.Parameters.Add("@p_razonSocial", SqlDbType.VarChar, CEN_Constantes.g_const_50).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_razonSocial", SqlDbType.VarChar, CEN_Constantes.g_const_50).Value = DatosCliente.razonSocial.Trim();

                        if (DatosCliente.nombres == null)
                            Command.Parameters.Add("@p_nombres", SqlDbType.VarChar, CEN_Constantes.g_const_30).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_nombres", SqlDbType.VarChar, CEN_Constantes.g_const_30).Value = DatosCliente.nombres.Trim();

                        if (DatosCliente.apePaterno == null)
                            Command.Parameters.Add("@p_apePaterno", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_apePaterno", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = DatosCliente.apePaterno.Trim();

                        if (DatosCliente.apeMaterno == null)
                            Command.Parameters.Add("@p_apeMaterno", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_apeMaterno", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = DatosCliente.apeMaterno.Trim();

                        if (DatosCliente.direccion == null)
                            Command.Parameters.Add("@p_direccion", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_direccion", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = DatosCliente.direccion.Trim();

                        if (DatosCliente.correo == null)
                            Command.Parameters.Add("@p_correo", SqlDbType.VarChar, CEN_Constantes.g_const_60).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_correo", SqlDbType.VarChar, CEN_Constantes.g_const_60).Value = DatosCliente.correo.Trim();

                        if (DatosCliente.telefono == null)
                            Command.Parameters.Add("@p_telefono", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_telefono", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = DatosCliente.telefono.Trim();

                        if(DatosCliente.celular == null)
                            Command.Parameters.Add("@p_celular", SqlDbType.Char, CEN_Constantes.g_const_9).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_celular", SqlDbType.Char, CEN_Constantes.g_const_9).Value = DatosCliente.celular.Trim();

                        if(DatosCliente.ubigeo == null)
                            Command.Parameters.Add("@p_ubigeo", SqlDbType.Char, CEN_Constantes.g_const_6).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_ubigeo", SqlDbType.Char, CEN_Constantes.g_const_6).Value = DatosCliente.ubigeo.Trim();

                        Command.Parameters.Add("@p_ordenAtencion", SqlDbType.SmallInt).Value = DatosCliente.ordenAtencion;
                        Command.Parameters.Add("@p_perfilCliente", SqlDbType.TinyInt).Value = DatosCliente.perfilCliente;
                        Command.Parameters.Add("@p_clasificacion", SqlDbType.TinyInt).Value = DatosCliente.clasificacion;
                        Command.Parameters.Add("@p_frecuencia", SqlDbType.TinyInt).Value = DatosCliente.frecuencia;
                        Command.Parameters.Add("@p_tipoListaPrecio", SqlDbType.TinyInt).Value = DatosCliente.tipoListaPrecio;
                        Command.Parameters.Add("@p_codRuta", SqlDbType.TinyInt).Value = DatosCliente.codRuta;

                        if (DatosCliente.coordenadaX == null)
                            Command.Parameters.Add("@p_coordenadaX", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_coordenadaX", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = DatosCliente.coordenadaX;

                        if (DatosCliente.coordenadaY == null)
                            Command.Parameters.Add("@p_coordenadaY", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_coordenadaY", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = DatosCliente.coordenadaY;

                        if (DatosCliente.usuario == null)
                            Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = DatosCliente.usuario.Trim();

                        if(DatosCliente == null)
                            Command.Parameters.Add("@p_ip", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_ip", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = DatosCliente.ip.Trim();

                        if(DatosCliente.mac == null)
                            Command.Parameters.Add("@p_mac", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_mac", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = DatosCliente.mac.Trim();
                        
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["flag"] != DBNull.Value && Convert.ToInt32(dr["flag"].ToString()) == CEN_Constantes.g_const_0)
                            {
                                rc = new RptaRegistroCliente();
                                rc.codCliente = int.Parse(dr["codPersona"].ToString());
                                rc.codPuntoEntrega = int.Parse(dr["codPuntoEntrega"].ToString());
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                                Respuesta.Resultado = new { reg_mod = rc };
                            }
                            else
                            {
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                                return Respuesta;
                            }
                        }

                    }
                    dr.Close();
                }
                return Respuesta;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }
        

        public int verificarExistenciaConceptos(int p_concepto, short p_correlativo)
        {
            //DESCRIPCION: VERIFICAR EXISTENCIA DE CONCEPTOS
            CAD_Conector CadCx = new CAD_Conector();    //conexión
            int cont = 0;                               //contador
            try
            {
                string l_sql = "SELECT COUNT (correlativo) FROM tblConcepto WHERE codConcepto = " + p_concepto + " AND correlativo = " + p_correlativo + " AND NOT correlativo = " + CEN_Constantes.g_const_0 + " AND marcaBaja = " + CEN_Constantes.g_const_0;
                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    SqlCommand cmd = new SqlCommand(l_sql, Connection);
                    cmd.CommandTimeout = CEN_Constantes.g_const_0;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        cont = Convert.ToInt32(dr[CEN_Constantes.g_const_0]);
                    }

                    dr.Close();
                }

                return cont;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public string obtener_descripcion_concepto(int p_concepto, int p_correlativo)
        {
            //DESCRIPCION: OBTENER DESCRIPCION DE CONCEPTO
            CAD_Conector CadCx = new CAD_Conector();    //conexión
            string descripcion = null;                  //descripcion
            try
            {
                string l_sql = "SELECT descripcion FROM tblConcepto WHERE codConcepto = " + p_concepto + " AND correlativo = " + p_correlativo + " AND marcaBaja = " + CEN_Constantes.g_const_0;
                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    SqlCommand cmd = new SqlCommand(l_sql, Connection);
                    cmd.CommandTimeout = CEN_Constantes.g_const_0;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        descripcion = Convert.ToString(dr[CEN_Constantes.g_const_0]);
                    }

                    dr.Close();
                }

                return descripcion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public int countPorFlag(int p_flag, string p_codigo)
        {
            //DESCRIPCION: CONTEO DE TABLAS SEGUN FLAG
            CAD_Conector CadCx = new CAD_Conector();    // Conexión
            int cont = 0;                               //contador
            string l_sql = null;                        //cadena de consulta
            try
            {
                switch (p_flag)
                {
                    case CEN_Constantes.g_const_1:
                        l_sql = "SELECT COUNT (ntraRutas) FROM tblRutas WHERE ntraRutas = " + p_codigo.Trim() + " AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                    case CEN_Constantes.g_const_2:
                        l_sql = "SELECT COUNT (numeroDocumento) FROM tblPersona WHERE numeroDocumento = '" + p_codigo.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                    case CEN_Constantes.g_const_3:
                        l_sql = "SELECT COUNT (ruc) FROM tblPersona WHERE ruc = '" + p_codigo.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                    case CEN_Constantes.g_const_4:
                        l_sql = "SELECT COUNT (codPersona) FROM tblPersona WHERE codPersona = '" + p_codigo.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                    case CEN_Constantes.g_const_5:
                        l_sql = "SELECT COUNT (codPersona) FROM tblPuntoEntrega WHERE ntraPuntoEntrega = '" + p_codigo.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                }

                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    SqlCommand cmd = new SqlCommand(l_sql, Connection);
                    cmd.CommandTimeout = CEN_Constantes.g_const_0;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        cont = Convert.ToInt32(dr[CEN_Constantes.g_const_0]);
                    }

                    dr.Close();
                }

                return cont;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public int obtenerCodigoPersona(int p_tipoDoc, string p_numDoc)
        {
            //DESCRIPCION: obtener codigo de persona 
            CAD_Conector CadCx = new CAD_Conector();    // Conexión
            int codPersona = 0;                         //codigo de persona
            string l_sql = null;                        //cadena de consulta
            try
            {
                l_sql = "SELECT codPersona FROM tblPersona WHERE (numeroDocumento = '" + p_numDoc.Trim() + "' OR ruc = '" + p_numDoc.Trim() + "') AND marcaBaja = " + CEN_Constantes.g_const_0;

                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    SqlCommand cmd = new SqlCommand(l_sql, Connection);
                    cmd.CommandTimeout = CEN_Constantes.g_const_0;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        codPersona = Convert.ToInt32(dr[CEN_Constantes.g_const_0]);
                    }

                    dr.Close();
                }

                return codPersona;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }



        public CEN_Data_Ruta obtenerRutaVendedor(CEN_Request_Sincronizacion datos)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Ruta data_ruta = new CEN_Data_Ruta(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_lista_rutas_vendedor_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@codUsuario", SqlDbType.Int).Value = datos.codVendedor;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            
                            CEN_Ruta ruta = new CEN_Ruta();
                            if (dr["ntraRutas"] != DBNull.Value)
                                ruta.ntraRutas = Convert.ToInt32(dr["ntraRutas"].ToString());
                            if (dr["ntraUsuario"] != DBNull.Value)
                                ruta.ntraUsuario = Convert.ToInt32(dr["ntraUsuario"].ToString());
                            if (dr["correlativo"] != DBNull.Value)
                                ruta.correlativo = Convert.ToInt16(dr["correlativo"].ToString());
                            if (dr["ORDEN"] != DBNull.Value)
                                ruta.orden = Convert.ToInt32(dr["ORDEN"].ToString());
                            if (dr["ABREVIATURA"] != DBNull.Value)
                                ruta.abreviatura = dr["ABREVIATURA"].ToString();
                            if (dr["RUTA"] != DBNull.Value)
                                ruta.descRuta = dr["RUTA"].ToString();

                            data_ruta.listRutas.Add(ruta);
                            ruta = new CEN_Ruta();
                        }

                        if (data_ruta.listRutas.Count > CEN_Constantes.g_const_0)
                        {
                            //Buscamos la ruta asignada del dia
                            data_ruta.listRutas = data_ruta.listRutas.OrderBy(o => o.orden).ToList();
                            data_ruta.ruta = data_ruta.listRutas[CEN_Constantes.g_const_0];

                            data_ruta.DescripcionResp = "";
                            data_ruta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_ruta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_ruta.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_ruta.DescripcionResp = "NO HAY RUTAS ASIGNADAS PARA EL VENDEDOR";
                            data_ruta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_ruta.ErrorWebSer.CodigoErr = 1000;
                            data_ruta.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_ruta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Cliente obtenerClientesxRutas(int ntraRutas)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Cliente data_cliente = new CEN_Data_Cliente(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_clientes_ruta_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("p_codRuta", SqlDbType.Int).Value = ntraRutas;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Cliente cliente = new CEN_Cliente();
                                if (dr["codPersona"] != DBNull.Value)
                                    cliente.codPersona = Convert.ToInt32(dr["codPersona"].ToString());
                                if (dr["tipoPersona"] != DBNull.Value)
                                    cliente.tipoPersona = Convert.ToInt32(dr["tipoPersona"].ToString());
                                if (dr["tipoDocumento"] != DBNull.Value)
                                    cliente.tipoDocumento = Convert.ToInt16(dr["tipoDocumento"].ToString());
                                if (dr["numeroDocumento"] != DBNull.Value)
                                    cliente.numeroDocumento = dr["numeroDocumento"].ToString();
                                if (dr["ruc"] != DBNull.Value)
                                    cliente.ruc = dr["ruc"].ToString();
                                if (dr["razonSocial"] != DBNull.Value)
                                    cliente.razonSocial = dr["razonSocial"].ToString();
                                if (dr["nombres"] != DBNull.Value)
                                    cliente.nombres = dr["nombres"].ToString();
                                if (dr["apellidoPaterno"] != DBNull.Value)
                                    cliente.apellidoPaterno = dr["apellidoPaterno"].ToString();
                                if (dr["apellidoMaterno"] != DBNull.Value)
                                    cliente.apellidoMaterno = dr["apellidoMaterno"].ToString();
                                if (dr["nombreCodigo"] != DBNull.Value)
                                    cliente.nombreCodigo = dr["nombreCodigo"].ToString();
                                if (dr["direccion"] != DBNull.Value)
                                    cliente.direccion = dr["direccion"].ToString();
                                if (dr["correo"] != DBNull.Value)
                                    cliente.correo = dr["correo"].ToString();
                                if (dr["telefono"] != DBNull.Value)
                                    cliente.telefono = dr["telefono"].ToString();
                                if (dr["celular"] != DBNull.Value)
                                    cliente.celular = dr["celular"].ToString();
                                if (dr["frecuenciaCliente"] != DBNull.Value)
                                    cliente.frecuenciaCliente = Convert.ToInt32(dr["frecuenciaCliente"].ToString());
                                if (dr["tipoListaPrecio"] != DBNull.Value)
                                    cliente.tipoListaPrecio = Convert.ToInt32(dr["tipoListaPrecio"].ToString());
                                if (dr["codRuta"] != DBNull.Value)
                                    cliente.codRuta = Convert.ToInt32(dr["codRuta"].ToString());
                                if (dr["ordenAtencion"] != DBNull.Value)
                                    cliente.ordenAtencion = Convert.ToInt32(dr["ordenAtencion"].ToString());
                                if (dr["codUbigeo"] != DBNull.Value)
                                    cliente.codUbigeo = dr["codUbigeo"].ToString().Trim();
                                data_cliente.listCliente.Add(cliente);
                                cliente = new CEN_Cliente();
                            }
                            else
                            {
                                data_cliente.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_cliente.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_cliente.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_cliente.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                            }
                        }

                        if (data_cliente.listCliente.Count > CEN_Constantes.g_const_0)
                        {

                            data_cliente.DescripcionResp = CEN_Constantes.g_const_vacio;
                            data_cliente.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_cliente.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_cliente.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_vacio;
                        }
                    }
                    dr.Close();
                }
                return data_cliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }
    }
}
