﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CAD_Descuentos
    {
        private SqlConnection Connection;
        public CEN_Data_Descuento obtenerDescuentos(DateTime fecha, int estado, int flag)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Descuento data_descuento = new CEN_Data_Descuento(); //Lista de rutas
            List<CEN_Descuento> lista_descuento_aux = new List<CEN_Descuento>(); //Lista de descuento
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_descuentos_fecha", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_fech", SqlDbType.DateTime).Value = fecha;
                        Command.Parameters.Add("@p_estado", SqlDbType.Int).Value = estado;
                        Command.Parameters.Add("@p_flag", SqlDbType.Int).Value = flag;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Descuento descuento = new CEN_Descuento();
                                if (dr["ntraDescuento"] != DBNull.Value)
                                    descuento.ntraDescuento = Convert.ToInt32(dr["ntraDescuento"].ToString());
                                if (dr["flag"] != DBNull.Value)
                                    descuento.flag = Convert.ToInt32(dr["flag"].ToString());
                                if (dr["ntra_flag"] != DBNull.Value)
                                    descuento.ntra_flag = dr["ntra_flag"].ToString();
                                if (dr["desc_desc"] != DBNull.Value)
                                    descuento.desc_desc = dr["desc_desc"].ToString();
                                if (dr["horaInicial"] != DBNull.Value)
                                    descuento.horaInicial = dr["horaInicial"].ToString();
                                if (dr["horaFin"] != DBNull.Value)
                                    descuento.horaFin = dr["horaFin"].ToString();
                                if (dr["desc_det"] != DBNull.Value)
                                    descuento.desc_det = dr["desc_det"].ToString();
                                if (dr["valorInicial"] != DBNull.Value)
                                    descuento.valorInicial = dr["valorInicial"].ToString();
                                if (dr["valorFinal"] != DBNull.Value)
                                    descuento.valorFinal = dr["valorFinal"].ToString();
                                if (dr["detalle"] != DBNull.Value)
                                    descuento.detalle = Convert.ToInt32(dr["detalle"].ToString());
                                if (dr["estado"] != DBNull.Value)
                                    descuento.estado = Convert.ToInt32(dr["estado"].ToString());
                                if (dr["tipoDescuento"] != DBNull.Value)
                                    descuento.tipoDescuento = Convert.ToInt32(dr["tipoDescuento"].ToString());

                                descuento.listDetalleDescuento = obtenerDetalleDescuentos(descuento.ntraDescuento, descuento.flag);
                                data_descuento.listDescuento.Add(descuento);
                                descuento = new CEN_Descuento();
                            }
                            else
                            {
                                data_descuento.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_descuento.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_descuento.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_descuento.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_descuento;
                            }
                        }

                        if (data_descuento.listDescuento.Count > CEN_Constantes.g_const_0)
                        {
                            var distinctDescuentos = data_descuento.listDescuento
                                                    .Select(m => new { m.ntraDescuento, m.desc_desc, m.horaInicial, m.horaFin })
                                                    .Distinct()
                                                    .ToList();
                            foreach (var item in distinctDescuentos)
                            {
                                CEN_Descuento aux_desc = new CEN_Descuento();
                                aux_desc.ntraDescuento = item.ntraDescuento;
                                aux_desc.desc_desc = item.desc_desc;
                                aux_desc.horaInicial = item.horaInicial;
                                aux_desc.horaFin = item.horaFin;
                                lista_descuento_aux.Add(aux_desc);
                            }
                            for (int i = CEN_Constantes.g_const_0; i < lista_descuento_aux.Count; i++)
                            {
                                CEN_Descuento aux_promo = new CEN_Descuento();
                                //REGISTRO PARA DESCRIPCION DE PROMOCION (100)
                                aux_promo.ntraDescuento = lista_descuento_aux[i].ntraDescuento;
                                aux_promo.flag = CEN_Constantes.g_const_100;
                                aux_promo.ntra_flag = aux_promo.ntraDescuento + CEN_Constantes.g_const_vacio + aux_promo.flag;
                                aux_promo.desc_desc = lista_descuento_aux[i].desc_desc;
                                aux_promo.horaInicial = CEN_Constantes.g_const_vacio;
                                aux_promo.horaFin = CEN_Constantes.g_const_vacio;
                                aux_promo.detalle = CEN_Constantes.g_const_0;
                                aux_promo.estado = CEN_Constantes.g_const_0;

                                data_descuento.listDescuento.Add(aux_promo);
                                //REGISTRO PARA HORAS DE PROMOCION (101)
                                aux_promo = new CEN_Descuento();
                                aux_promo.ntraDescuento = lista_descuento_aux[i].ntraDescuento;
                                aux_promo.flag = CEN_Constantes.g_const_101;
                                aux_promo.ntra_flag = aux_promo.ntraDescuento + CEN_Constantes.g_const_vacio + aux_promo.flag;
                                aux_promo.desc_desc = CEN_Constantes.g_const_vacio;
                                aux_promo.horaInicial = lista_descuento_aux[i].horaInicial;
                                aux_promo.horaFin = lista_descuento_aux[i].horaFin;
                                aux_promo.valorInicial = lista_descuento_aux[i].horaInicial;
                                aux_promo.valorFinal = lista_descuento_aux[i].horaFin;
                                aux_promo.detalle = CEN_Constantes.g_const_0;
                                aux_promo.estado = CEN_Constantes.g_const_0;

                                data_descuento.listDescuento.Add(aux_promo);
                            }

                                

                        }


                        data_descuento.DescripcionResp = CEN_Constantes.g_const_vacio;
                        data_descuento.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        data_descuento.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                        data_descuento.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_vacio;
                    }
                    dr.Close();
                }
                return data_descuento;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        private List<CEN_Detalle_Descuento> obtenerDetalleDescuentos(int p_ntra, int p_flag)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            List<CEN_Detalle_Descuento> list_detalle_descuento = new List<CEN_Detalle_Descuento>(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_detalle_flag_descuento", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_ntra", SqlDbType.Int).Value = p_ntra;
                        Command.Parameters.Add("@p_flag", SqlDbType.Int).Value = p_flag;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Detalle_Descuento det_descuento = new CEN_Detalle_Descuento();
                                if (dr["ntra_flag"] != DBNull.Value)
                                    det_descuento.ntra_flag = dr["ntra_flag"].ToString();
                                if (dr["flag"] != DBNull.Value)
                                    det_descuento.flag = Convert.ToInt32(dr["flag"].ToString());
                                if (dr["descripcion"] != DBNull.Value)
                                    det_descuento.descripcion = dr["descripcion"].ToString();
                                if (dr["valorEntero1"] != DBNull.Value)
                                    det_descuento.valorEntero1 = Convert.ToInt32(dr["valorEntero1"].ToString());
                                if (dr["valorEntero2"] != DBNull.Value)
                                    det_descuento.valorEntero2 = Convert.ToInt32(dr["valorEntero2"].ToString());
                                if (dr["valorMoneda1"] != DBNull.Value)
                                    det_descuento.valorMoneda1 = Convert.ToInt64(dr["valorMoneda1"].ToString());
                                if (dr["valorMoneda2"] != DBNull.Value)
                                    det_descuento.valorMoneda2 = Convert.ToInt64(dr["valorMoneda2"].ToString());
                                if (dr["valorCadena1"] != DBNull.Value)
                                    det_descuento.valorCadena1 = dr["valorCadena1"].ToString();
                                if (dr["valorCadena2"] != DBNull.Value)
                                    det_descuento.valorCadena2 = dr["valorCadena2"].ToString();
                                if (dr["valorFecha1"] != DBNull.Value)
                                    det_descuento.valorFecha1 = Convert.ToDateTime(dr["valorFecha1"].ToString());
                                if (dr["valorFecha2"] != DBNull.Value)
                                    det_descuento.valorFecha2 = Convert.ToDateTime(dr["valorFecha2"].ToString());
                                if (dr["estado"] != DBNull.Value)
                                    det_descuento.estado = Convert.ToInt32(dr["estado"].ToString());

                                list_detalle_descuento.Add(det_descuento);
                                det_descuento = new CEN_Detalle_Descuento();
                            }
                            else
                            {
                                return list_detalle_descuento;
                            }
                        }
                    }
                    dr.Close();
                }
                return list_detalle_descuento;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }
    }
}
