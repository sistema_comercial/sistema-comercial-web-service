﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CEN;

namespace CAD
{
    public class CAD_Preventa
    {
        private SqlConnection Connection;
        public CEN_Data_Preventa listaPreventasVendedor(int codVendedor,DateTime fecha)
        {
            
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Preventa data_preventa = new CEN_Data_Preventa(); //Lista de preventa
            CEN_Data_Detalle_Preventa det_preventa = new CEN_Data_Detalle_Preventa();
            CEN_Data_Preventa_Promocion det_prev_prom = new CEN_Data_Preventa_Promocion();
            CEN_Data_Preventa_Descuento det_prev_desc = new CEN_Data_Preventa_Descuento();
            CAD_Consulta cad_consulta = new CAD_Consulta();
            DateTime fecha_prev = DateTime.Now;
            try
            {
                int dia = (int)System.DateTime.Now.DayOfWeek;
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_preventas_vendedor", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("p_codUsu", SqlDbType.Int).Value = codVendedor;
                        Command.Parameters.Add("p_fech", SqlDbType.Date).Value = fecha;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Preventa preventa = new CEN_Preventa();
                                if (dr["ntraPreventa"] != DBNull.Value)
                                    preventa.ntraPreventa = Convert.ToInt32(dr["ntraPreventa"].ToString());
                                if (dr["codCliente"] != DBNull.Value)
                                    preventa.codCliente = Convert.ToInt32(dr["codCliente"].ToString());
                                if (dr["codUsuario"] != DBNull.Value)
                                    preventa.codUsuario = Convert.ToInt32(dr["codUsuario"].ToString());
                                if (dr["codPuntoEntrega"] != DBNull.Value)
                                    preventa.codPuntoEntrega = Convert.ToInt32(dr["codPuntoEntrega"].ToString());
                                if (dr["tipoMoneda"] != DBNull.Value)
                                    preventa.tipoMoneda = Convert.ToInt16(dr["tipoMoneda"].ToString());
                                if (dr["tipoVenta"] != DBNull.Value)
                                    preventa.tipoVenta = Convert.ToInt16(dr["tipoVenta"].ToString());
                                if (dr["tipoDocumentoVenta"] != DBNull.Value)
                                    preventa.tipoDocumentoVenta = Convert.ToInt16(dr["tipoDocumentoVenta"].ToString());
                                if (dr["fecha"] != DBNull.Value)
                                {
                                    fecha_prev = Convert.ToDateTime(dr["fecha"].ToString());
                                    preventa.fecha = cad_consulta.ConvertFechaDateToString(fecha_prev);

                                }

                                if (dr["fechaEntrega"] != DBNull.Value)
                                {
                                    fecha_prev = Convert.ToDateTime(dr["fechaEntrega"].ToString());
                                    preventa.fechaEntrega = cad_consulta.ConvertFechaDateToString(fecha_prev);
                                    //preventa.fechaEntrega = Convert.ToDateTime(dr["fechaEntrega"].ToString());
                                }

                                if (dr["fechaPago"] != DBNull.Value)
                                {
                                    fecha_prev = Convert.ToDateTime(dr["fechaPago"].ToString());
                                    preventa.fechaPago = cad_consulta.ConvertFechaDateToString(fecha_prev);
                                    //preventa.fechaPago = Convert.ToDateTime(dr["fechaPago"].ToString());
                                }
                                if (dr["recargo"] != DBNull.Value)
                                    preventa.recargo = Convert.ToDouble(dr["recargo"].ToString());
                                if (dr["igv"] != DBNull.Value)
                                    preventa.igv = Convert.ToDouble(dr["igv"].ToString());
                                if (dr["isc"] != DBNull.Value)
                                    preventa.isc = Convert.ToDouble(dr["isc"].ToString());
                                if (dr["estado"] != DBNull.Value)
                                    preventa.estado = Convert.ToInt16(dr["estado"].ToString());
                                if (dr["total"] != DBNull.Value)
                                    preventa.total = Convert.ToDouble(dr["total"].ToString());
                                if (dr["tipoDocumento"] != DBNull.Value)
                                    preventa.tipoDocumento = Convert.ToInt32(dr["tipoDocumento"].ToString());
                                if (dr["numeroDocumento"] != DBNull.Value)
                                    preventa.numeroDocumento = dr["numeroDocumento"].ToString();
                                if (dr["origenVenta"] != DBNull.Value)
                                    preventa.origenVenta = Convert.ToInt16(dr["origenVenta"].ToString());
                                if (dr["horaEntrega"] != DBNull.Value)
                                    preventa.horaEntrega = dr["horaEntrega"].ToString();
                                if (dr["codSucursal"] != DBNull.Value)
                                    preventa.codSucursal = Convert.ToInt32(dr["codSucursal"].ToString());
                                if (dr["flagRecargo"] != DBNull.Value)
                                    preventa.flagRecargo = Convert.ToInt16(dr["flagRecargo"].ToString());


                                det_preventa = listaDetallePreventa(preventa.ntraPreventa);
                                if(det_preventa.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                                {
                                    preventa.listDetPreventa = det_preventa.listdet_Preventa;
                                }
                                det_preventa = new CEN_Data_Detalle_Preventa();

                                det_prev_prom = ListaPreventaPromociones(preventa.ntraPreventa);
                                if (det_prev_prom.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                                {
                                    preventa.listPrevPromocion = det_prev_prom.list_Preventa_prom;
                                }
                                det_prev_prom = new CEN_Data_Preventa_Promocion();

                                det_prev_desc = ListaPreventaDescuentos(preventa.ntraPreventa);
                                if (det_prev_desc.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                                {
                                    preventa.listPrevDescuento = det_prev_desc.list_Preventa_desc;
                                }
                                det_prev_desc = new CEN_Data_Preventa_Descuento();


                                data_preventa.listPreventa.Add(preventa);
                                preventa = new CEN_Preventa();
                            }
                            else
                            {
                                data_preventa.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_preventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_preventa.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_preventa.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_preventa;
                            }
                        }

                        if (data_preventa.listPreventa.Count > CEN_Constantes.g_const_0)
                        {

                            data_preventa.DescripcionResp = "";
                            data_preventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_preventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_preventa.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_preventa.DescripcionResp = "NO HAY PREVENTAS REGISTRADOS";
                            data_preventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_preventa.ErrorWebSer.CodigoErr = 1000;
                            data_preventa.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_preventa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        public CEN_Data_Detalle_Preventa listaDetallePreventa(int p_codPrev)
        {

            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Detalle_Preventa data_det_preventa = new CEN_Data_Detalle_Preventa(); //Lista de preventa
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_detalle_preventas", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("p_codPrev", SqlDbType.Int).Value = p_codPrev;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Detalle_Preventa det_preventa = new CEN_Detalle_Preventa();
                                if (dr["codPreventa"] != DBNull.Value)
                                    det_preventa.codPreventa = Convert.ToInt32(dr["codPreventa"].ToString());
                                if (dr["itemPreventa"] != DBNull.Value)
                                    det_preventa.itemPreventa = Convert.ToInt16(dr["itemPreventa"].ToString());
                                if (dr["codPresentacion"] != DBNull.Value)
                                    det_preventa.codPresentacion = Convert.ToInt32(dr["codPresentacion"].ToString());
                                if (dr["codProducto"] != DBNull.Value)
                                    det_preventa.codProducto = dr["codProducto"].ToString();
                                if (dr["codAlmacen"] != DBNull.Value)
                                    det_preventa.codAlmacen = Convert.ToInt32(dr["codAlmacen"].ToString());
                                if (dr["cantidadPresentacion"] != DBNull.Value)
                                    det_preventa.cantidadPresentacion = Convert.ToInt32(dr["cantidadPresentacion"].ToString());
                                if (dr["cantidadUnidadBase"] != DBNull.Value)
                                    det_preventa.cantidadUnidadBase = Convert.ToInt32(dr["cantidadUnidadBase"].ToString());
                                if (dr["precioVenta"] != DBNull.Value)
                                    det_preventa.precioVenta = Convert.ToDouble(dr["precioVenta"].ToString());
                                if (dr["TipoProducto"] != DBNull.Value)
                                    det_preventa.TipoProducto = Convert.ToInt16(dr["TipoProducto"].ToString());
                                data_det_preventa.listdet_Preventa.Add(det_preventa);
                                det_preventa = new CEN_Detalle_Preventa();
                            }
                            else
                            {
                                data_det_preventa.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_det_preventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_det_preventa.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_det_preventa.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_det_preventa;
                            }
                        }

                        if (data_det_preventa.listdet_Preventa.Count > CEN_Constantes.g_const_0)
                        {

                            data_det_preventa.DescripcionResp = "";
                            data_det_preventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_det_preventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_det_preventa.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_det_preventa.DescripcionResp = "NO HAY PREVENTAS REGISTRADOS";
                            data_det_preventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_det_preventa.ErrorWebSer.CodigoErr = 1000;
                            data_det_preventa.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_det_preventa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Preventa_Promocion ListaPreventaPromociones(int p_codPrev)
        {

            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Preventa_Promocion data_preventa_prom = new CEN_Data_Preventa_Promocion(); //Lista de preventa
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_preventa_promociones", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("p_codPrev", SqlDbType.Int).Value = p_codPrev;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Preventa_Promocion det_preventa = new CEN_Preventa_Promocion();
                                if (dr["codPreventa"] != DBNull.Value)
                                    det_preventa.codPreventa = Convert.ToInt32(dr["codPreventa"].ToString());
                                if (dr["codPromocion"] != DBNull.Value)
                                    det_preventa.codPromocion = Convert.ToInt32(dr["codPromocion"].ToString());
                                if (dr["itemPreventa"] != DBNull.Value)
                                    det_preventa.itemPreventa = Convert.ToInt16(dr["itemPreventa"].ToString());
                                if (dr["itemPromocionado"] != DBNull.Value)
                                    det_preventa.itemPromocionado = Convert.ToInt16(dr["itemPromocionado"].ToString());

                                data_preventa_prom.list_Preventa_prom.Add(det_preventa);
                                det_preventa = new CEN_Preventa_Promocion();
                            }
                            else
                            {
                                data_preventa_prom.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_preventa_prom.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_preventa_prom.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_preventa_prom.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_preventa_prom;
                            }
                        }

                        if (data_preventa_prom.list_Preventa_prom.Count > CEN_Constantes.g_const_0)
                        {

                            data_preventa_prom.DescripcionResp = "";
                            data_preventa_prom.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_preventa_prom.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_preventa_prom.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_preventa_prom.DescripcionResp = "NO HAY PREVENTAS REGISTRADOS";
                            data_preventa_prom.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_preventa_prom.ErrorWebSer.CodigoErr = 1000;
                            data_preventa_prom.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_preventa_prom;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Preventa_Descuento ListaPreventaDescuentos(int p_codPrev)
        {

            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Preventa_Descuento data_preventa_desc = new CEN_Data_Preventa_Descuento(); //Lista 
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_preventa_descuentos", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("p_codPrev", SqlDbType.Int).Value = p_codPrev;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Preventa_Descuento det_preventa = new CEN_Preventa_Descuento();
                                if (dr["codPreventa"] != DBNull.Value)
                                    det_preventa.codPreventa = Convert.ToInt32(dr["codPreventa"].ToString());
                                if (dr["codDescuento"] != DBNull.Value)
                                    det_preventa.codDescuento = Convert.ToInt32(dr["codDescuento"].ToString());
                                if (dr["itemPreventa"] != DBNull.Value)
                                    det_preventa.itemPreventa = Convert.ToInt16(dr["itemPreventa"].ToString());
                                if (dr["importe"] != DBNull.Value)
                                    det_preventa.importe = Convert.ToDouble(dr["importe"].ToString());

                                data_preventa_desc.list_Preventa_desc.Add(det_preventa);
                                det_preventa = new CEN_Preventa_Descuento();
                            }
                            else
                            {
                                data_preventa_desc.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_preventa_desc.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_preventa_desc.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_preventa_desc.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_preventa_desc;
                            }
                        }

                        if (data_preventa_desc.list_Preventa_desc.Count > CEN_Constantes.g_const_0)
                        {

                            data_preventa_desc.DescripcionResp = "";
                            data_preventa_desc.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_preventa_desc.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_preventa_desc.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_preventa_desc.DescripcionResp = "NO HAY PREVENTAS REGISTRADOS";
                            data_preventa_desc.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_preventa_desc.ErrorWebSer.CodigoErr = 1000;
                            data_preventa_desc.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_preventa_desc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        public CEN_RespuestaWSRegPreventa registro_modificacion_preventa(CEN_RequestPreventa Preventa)
        {
            //DESCRIPCION: REGISTRAR PREVENTA
            CAD_Conector CadCx = new CAD_Conector();            //conexion
            SqlDataReader dr;                                   //data reader
            CEN_RespuestaWSRegPreventa Respuesta = new CEN_RespuestaWSRegPreventa();  //clase de registro de respuesta
            RptaRegistroPreventa rp = null;                      //clase respuesta
            CAD_Consulta consulta = new CAD_Consulta();
            DateTime fecha2 = DateTime.Now;
            TimeSpan hora = new TimeSpan();
            string xmlListDetalle = ObjectToXMLGeneric<List<CEN_Detalle_Preventa>>(Preventa.listDetPreventa); //XML de lista de detalles
            string xmlListPreventaProm = ObjectToXMLGeneric<List<CEN_Preventa_Promocion>>(Preventa.listPrevPromocion); //XML de lista de preventa promocion
            string xmlListPreventaDesc = ObjectToXMLGeneric<List<CEN_Preventa_Descuento>>(Preventa.listPrevDescuento); //XML de lista de preventa descuento
            try
            {
                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    using (SqlCommand Command = new SqlCommand("pa_registrar_modificar_preventa", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_proceso", SqlDbType.TinyInt).Value = Preventa.proceso;
                        Command.Parameters.Add("@p_ntraPreventa", SqlDbType.Int).Value = Preventa.ntraPreventa;
                        Command.Parameters.Add("@p_codCliente", SqlDbType.Int).Value = Preventa.codCliente;
                        Command.Parameters.Add("@p_codUsuario", SqlDbType.Int).Value = Preventa.codUsuario;
                        Command.Parameters.Add("@p_codPuntoEntrega", SqlDbType.Int).Value = Preventa.codPuntoEntrega;
                        Command.Parameters.Add("@p_tipoMoneda", SqlDbType.TinyInt).Value = Preventa.tipoMoneda;
                        Command.Parameters.Add("@p_tipoVenta", SqlDbType.TinyInt).Value = Preventa.tipoVenta;
                        Command.Parameters.Add("@p_tipoDocumentoVenta", SqlDbType.TinyInt).Value = Preventa.tipoDocumentoVenta;

                        if (Preventa.fecha == null)
                            Command.Parameters.Add("@p_fecha", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = CEN_Constantes.g_const_vacio;
                        else
                        {
                            fecha2 = consulta.ConvertFechaStringToDate(Preventa.fecha);
                            Command.Parameters.Add("@p_fecha", SqlDbType.Date).Value = fecha2;
                        }


                        if (Preventa.fechaEntrega == null)
                            Command.Parameters.Add("@p_fechaEntrega", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = CEN_Constantes.g_const_vacio;
                        else
                        {
                            fecha2 = consulta.ConvertFechaStringToDate(Preventa.fechaEntrega);
                            Command.Parameters.Add("@p_fechaEntrega", SqlDbType.Date).Value = fecha2;
                        }

                        if (Preventa.fechaPago == null)
                            Command.Parameters.Add("@p_fechaPago", SqlDbType.VarChar, CEN_Constantes.g_const_15).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_fechaPago", SqlDbType.Date).Value = Preventa.fechaPago;

                        Command.Parameters.Add("@p_flagRecargo", SqlDbType.TinyInt).Value = Preventa.flagRecargo;
                        Command.Parameters.Add("@p_recargo", SqlDbType.Money).Value = Preventa.recargo;
                        Command.Parameters.Add("@p_igv", SqlDbType.Money).Value = Preventa.igv;
                        Command.Parameters.Add("@p_isc", SqlDbType.Money).Value = Preventa.isc;
                        Command.Parameters.Add("@p_total", SqlDbType.Money).Value = Preventa.total;
                        Command.Parameters.Add("@p_estado", SqlDbType.TinyInt).Value = Preventa.estado;
                        Command.Parameters.Add("@p_origenVenta", SqlDbType.TinyInt).Value = Preventa.origenVenta;
                        Command.Parameters.Add("@p_listaDetalles", SqlDbType.Xml).Value = xmlListDetalle;
                        Command.Parameters.Add("@p_listaPreventaPromocion", SqlDbType.Xml).Value = xmlListPreventaProm;
                        Command.Parameters.Add("@p_listaPreventaDescuento", SqlDbType.Xml).Value = xmlListPreventaDesc;

                        if (Preventa.usuario == null)
                            Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = Preventa.usuario.Trim();

                        if (Preventa.ip == null)
                            Command.Parameters.Add("@p_ip", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_ip", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = Preventa.ip.Trim();

                        if (Preventa.mac == null)
                            Command.Parameters.Add("@p_mac", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_mac", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = Preventa.mac.Trim();

                        if (Preventa.horaEntrega == null)
                            Command.Parameters.Add("@p_horaEntrega", SqlDbType.VarChar, CEN_Constantes.g_const_10).Value = CEN_Constantes.g_const_vacio;
                        else
                        {
                            hora = TimeSpan.Parse(Preventa.horaEntrega);
                            Command.Parameters.Add("@p_horaEntrega", SqlDbType.Time).Value = hora;
                        }

                        Command.Parameters.Add("@p_codSucursal", SqlDbType.Int).Value = Preventa.codSucursal;

                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["flag"] != DBNull.Value && Convert.ToInt32(dr["flag"].ToString()) == CEN_Constantes.g_const_0)
                            {
                                rp = new RptaRegistroPreventa();
                                rp.ntraPreventa = int.Parse(dr["ntraPreventa"].ToString());
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                                Respuesta.ResultadoRegPreventa = new { rp };

                            }
                            else
                            {
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                                return Respuesta;
                            }
                        }

                    }
                    dr.Close();
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

        public static String ObjectToXMLGeneric<T>(T filter)
        {
            //DESCRIPCION: CONVERTIR CLASS LIST EN CADENA XML
            string xml = null; // XML
            using (StringWriter sw = new StringWriter())
            {

                XmlSerializer xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, filter);
                try
                {
                    xml = sw.ToString();

                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            return xml;
        }

        public CEN_RespuestaWS anular_preventa(CEN_RequestAnularPreventa preventa)
        {
            //DESCRIPCION: ANULAR PREVENTA
            CAD_Conector CadCx = new CAD_Conector();            //conexion
            SqlDataReader dr;                                   //data reader
            CEN_RespuestaWS Respuesta = new CEN_RespuestaWS();  //clase de registro de respuesta
            try
            {
                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    using (SqlCommand Command = new SqlCommand("pa_anular_preventa", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_ntraPreventa", SqlDbType.Int).Value = preventa.ntraPreventa;

                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["flag"] != DBNull.Value && Convert.ToInt32(dr["flag"].ToString()) == CEN_Constantes.g_const_0)
                            {
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                            }
                            else
                            {
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                                return Respuesta;
                            }
                        }

                    }
                    dr.Close();
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }

    }
}
