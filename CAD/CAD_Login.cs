﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
    public class CAD_Login
    {
        private SqlConnection Connection;
        public CEN_RespuestaWSLogin ValidarLogin(CEN_Request_Login request, int ntraUsuario)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_RespuestaWSLogin respuestaWS = new CEN_RespuestaWSLogin();
            try
            {
                string keyLogin = ConfigurationManager.ConnectionStrings[CEN_Constantes.g_const_keyLogin].ConnectionString.ToString();
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_validar_sesion_usuario", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = request.usuario;
                        Command.Parameters.Add("@p_contra", SqlDbType.VarChar, CEN_Constantes.g_const_30).Value = request.contra;
                        Command.Parameters.Add("@p_sucursal", SqlDbType.Int).Value = request.sucursal;
                        Command.Parameters.Add("@p_keyLogin", SqlDbType.VarChar, CEN_Constantes.g_const_30).Value = keyLogin;
                        Command.Parameters.Add("@p_tipo", SqlDbType.SmallInt).Value = request.tipo;
                        Command.Parameters.Add("@p_ntraUsuario", SqlDbType.Int).Value = ntraUsuario;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuestaWS.respuesta = Convert.ToInt32(dr["estado"].ToString());
                            respuestaWS.ntraUsuario = Convert.ToInt32(dr["ntraUsuario"].ToString());
                            respuestaWS.DescripcionResp = dr["mensaje"].ToString();

                            respuestaWS.ErrorWebSer.CodigoErr = Convert.ToInt16(dr["codigo"].ToString());
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            }
                            else
                            {
                                respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            }
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_3000)
                            {
                                respuestaWS.ErrorWebSer.TipoErr = Convert.ToInt16(dr["estado"].ToString());
                            }
                            respuestaWS.ErrorWebSer.DescripcionErr = dr["mensaje"].ToString();

                        }
                    }
                    dr.Close();
                }
                return respuestaWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_DetalleSesionUsuario RegistroSesionUsu(int codUsuario, short tipoLogueo, DateTime fechaInicio)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_DetalleSesionUsuario respuestaWS = new CEN_DetalleSesionUsuario();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_registrar_sesion_usuario", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsu", SqlDbType.Int).Value = codUsuario;
                        Command.Parameters.Add("@p_tipoLog", SqlDbType.SmallInt).Value = tipoLogueo;
                        Command.Parameters.Add("@p_fecha_ini", SqlDbType.DateTime).Value = fechaInicio;
                        Command.Parameters.Add("@p_fecha_fin", SqlDbType.DateTime).Value = null;
                        Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = codUsuario;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuestaWS.ntraLogueo = long.Parse(dr["ntra"].ToString());
                            respuestaWS.codigo = Convert.ToInt32(dr["codigo"].ToString());
                            respuestaWS.mensaje = dr["mensaje"].ToString();

                        }
                    }
                    dr.Close();
                }
                return respuestaWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        public CEN_DetalleSesionUsuario CerrarSesionUsu(int codUsuario, DateTime fecha)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_DetalleSesionUsuario respuestaWS = new CEN_DetalleSesionUsuario();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_cerrar_sesion_usuario", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsu", SqlDbType.Int).Value = codUsuario;
                        Command.Parameters.Add("@p_fecha_fin", SqlDbType.DateTime).Value = fecha;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuestaWS.codigo = Convert.ToInt32(dr["codigo"].ToString());
                            respuestaWS.mensaje = dr["mensaje"].ToString();

                        }
                    }
                    dr.Close();
                }
                return respuestaWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        public CEN_DetalleSesionUsuario ValidarSesionAbiertaUsu(int codUsuario)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_DetalleSesionUsuario respuestaWS = new CEN_DetalleSesionUsuario();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_validar_sesion_abierta_usuario", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsu", SqlDbType.Int).Value = codUsuario;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuestaWS.contador = Convert.ToInt32(dr["contador"].ToString());
                            respuestaWS.mensaje = dr["mensaje"].ToString();

                        }
                    }
                    dr.Close();
                }
                return respuestaWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_DataSalidaLogin RegistrarLoginFallido(CEN_LoginFallido data)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_DataSalidaLogin respuesta = new CEN_DataSalidaLogin();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_registrar_login_fallido", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsu", SqlDbType.Int).Value = data.codUsu;
                        Command.Parameters.Add("@p_cantFall", SqlDbType.SmallInt).Value = data.cantFall;
                        Command.Parameters.Add("@p_fecha", SqlDbType.DateTime).Value = data.fecha;
                        Command.Parameters.Add("@p_ip", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = data.ip;
                        Command.Parameters.Add("@p_mac", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = data.mac;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuesta.codigo = Convert.ToInt32(dr["codigo"].ToString());
                            respuesta.mensaje = dr["mensaje"].ToString();

                        }
                    }
                    dr.Close();
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        public CEN_LoginFallido ListarLoginFallidoUsu(int codUsu)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_LoginFallido respuesta = new CEN_LoginFallido();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_login_fallido_usuario", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsu", SqlDbType.Int).Value = codUsu;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuesta.codUsu = Convert.ToInt32(dr["codUsuario"].ToString());
                            respuesta.cantFall = Convert.ToInt16(dr["cantFallido"].ToString());
                            respuesta.fecha = Convert.ToDateTime(dr["FechaRegistro"].ToString());

                        }
                    }
                    dr.Close();
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_DataSalidaLogin BloquearUsuario(int codUsu)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_DataSalidaLogin respuesta = new CEN_DataSalidaLogin();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_bloquear_usuario", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsu", SqlDbType.Int).Value = codUsu;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            respuesta.codigo = Convert.ToInt32(dr["codigo"].ToString());
                            respuesta.mensaje = dr["mensaje"].ToString();

                        }
                    }
                    dr.Close();
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }



    }
}
