﻿using System;
using System.Data;
using System.Data.SqlClient;
using CEN;

namespace CAD
{
    public class CAD_Usuario
    {
        private SqlConnection Connection;
        public int ValidarUsuarioActivo(int codUsu)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            int respuesta = CEN_Constantes.g_const_0; 
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_validar_usuario_activo", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsu", SqlDbType.Int).Value = codUsu;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["contador"] != DBNull.Value)
                                respuesta = Convert.ToInt32(dr["contador"].ToString());

                        }
                    }
                    dr.Close();
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }


        }

    }
}
