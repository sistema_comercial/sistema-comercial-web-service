﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace CAD
{
    public class CAD_BitacorasRutas
    {
        private SqlConnection Connection;
        public CEN_RespuestaBitacoraWS RegistrarRutasBitacoras(CEN_RequestBitacorasRutas ObjBitRu)
        {

            SqlConnection con = null;
            SqlCommand cmd = null;
            CEN_RespuestaRutasBitacoras respBiRu = null;
            CEN_RespuestaBitacoraWS RespuestaBi = new CEN_RespuestaBitacoraWS();
            CAD_Conector CadCx = new CAD_Conector();
            int result = 0;
            string mensaje = "";
            int codRutaResputa = 0;
            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_insertar_rutas_bitacoras", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@codRuta", ObjBitRu.codRuta);
                cmd.Parameters.AddWithValue("@codCliente", ObjBitRu.codCliente);
                cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(ObjBitRu.fecha));
                cmd.Parameters.AddWithValue("@visita", ObjBitRu.visita);
                cmd.Parameters.AddWithValue("@motivo", ObjBitRu.motivo);
                cmd.Parameters.AddWithValue("@usuario", ObjBitRu.usuario);
                cmd.Parameters.AddWithValue("@ip", ObjBitRu.ip);
                cmd.Parameters.AddWithValue("@mac", ObjBitRu.mac);
                cmd.Parameters.AddWithValue("@cordenadaX", ObjBitRu.cordenadaX);
                cmd.Parameters.AddWithValue("@cordenadaY", ObjBitRu.cordenadaY);
                cmd.Parameters.AddWithValue("@estado", ObjBitRu.estado);
                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@msje", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@codRutaResputa", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@resultado"].Value);
                mensaje = Convert.ToString(cmd.Parameters["@msje"].Value);
                codRutaResputa = Convert.ToInt32(cmd.Parameters["@codRutaResputa"].Value);

                if (cmd.Parameters["@resultado"].Value != DBNull.Value && result == 0)
                {
                    respBiRu = new CEN_RespuestaRutasBitacoras();
                    respBiRu.codRuta = codRutaResputa;
                    RespuestaBi.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                    RespuestaBi.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    RespuestaBi.ErrorWebSer.DescripcionErr = mensaje;
                    RespuestaBi.ResultadoBitacora = new { respBiRu };
                }
                else
                {

                    RespuestaBi.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                    RespuestaBi.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                    RespuestaBi.ErrorWebSer.DescripcionErr = mensaje;
                    return RespuestaBi;
                }

                

                return RespuestaBi;
            }
            catch (Exception ex)
                {
                throw ex;
            }
            finally
            {
                con.Close();
            }
           
        }


        public CEN_Data_Localizacion LocalizacionxClienteSinc(List<CEN_Cliente> listCliente)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Localizacion data_ruta = new CEN_Data_Localizacion(); //Lista de localizaciones
            CEN_Localizacion ruta = new CEN_Localizacion();
            try
            {
                
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    

                    for(int i = CEN_Constantes.g_const_0; i<listCliente.Count; i++)
                    {
                        conexion.AbrirConexion(Connection);
                        using (SqlCommand Command = new SqlCommand("pa_listar_localizacion_cliente", Connection))
                        {
                            Command.CommandType = CommandType.StoredProcedure;
                            Command.Parameters.Add("@p_codPer", SqlDbType.Int).Value = listCliente[i].codPersona;
                            Command.CommandTimeout = CEN_Constantes.g_const_0;
                            dr = Command.ExecuteReader();

                            while (dr.Read())
                            {

                                ruta = new CEN_Localizacion();
                                if (dr["codPersona"] != DBNull.Value)
                                    ruta.codPersona = Convert.ToInt32(dr["codPersona"].ToString());
                                if (dr["coordenadaX"] != DBNull.Value)
                                    ruta.coordenadaX = dr["coordenadaX"].ToString();
                                if (dr["coordenadaY"] != DBNull.Value)
                                    ruta.coordenadaY = dr["coordenadaY"].ToString();
                                //Completando datos adicionales
                                if(listCliente[i].tipoPersona == CEN_Constantes.g_const_1)
                                {
                                    //Persona natura - numero documento
                                    ruta.numDocumento = listCliente[i].numeroDocumento;
                                    ruta.nombreCompleto = listCliente[i].apellidoPaterno + CEN_Constantes.g_const_espacio +
                                                            listCliente[i].apellidoMaterno + CEN_Constantes.g_const_espacio +
                                                            listCliente[i].nombres;
                                }
                                else if(listCliente[i].tipoPersona == CEN_Constantes.g_const_2)
                                {
                                    ruta.numDocumento = listCliente[i].ruc;
                                    ruta.nombreCompleto = listCliente[i].razonSocial;
                                }

                                data_ruta.listLocalizacion.Add(ruta);
                            }
                            
                        }
                        dr.Close();
                    }

                    data_ruta.DescripcionResp = CEN_Constantes.g_const_vacio;
                    data_ruta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    data_ruta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                    data_ruta.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_vacio;

                }
                return data_ruta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }



    }
}
