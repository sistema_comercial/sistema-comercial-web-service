﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CAD_Consulta
    {
        private SqlConnection Connection;
        public string ConvertFechaDateToString(DateTime fecha)
        {
            try
            {
                CultureInfo MyCultureInfo = new CultureInfo(CEN_Constantes.g_const_es_PE);
                return fecha.ToString(CEN_Constantes.g_const_formfech, MyCultureInfo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DateTime ConvertFechaStringToDate(string fecha)
        {
            CultureInfo MyCultureInfo = new CultureInfo(CEN_Constantes.g_const_es_PE);
            DateTime myDate = DateTime.ParseExact(fecha, CEN_Constantes.g_const_formfech, MyCultureInfo);
            return myDate;

        }

        public int countPorFlag(int p_flag, string p_codigo, string p_valor)
        {
            //DESCRIPCION: CONTEO DE TABLAS SEGUN FLAG
            CAD_Conector CadCx = new CAD_Conector();    // Conexión
            int cont = 0;                               //contador
            string l_sql = null;                        //cadena de consulta
            try
            {
                switch (p_flag)
                {
                    case CEN_Constantes.g_const_1:
                        l_sql = "SELECT COUNT (codPersona) FROM tblPuntoEntrega WHERE ntraPuntoEntrega = '" + p_codigo.Trim() + "' AND codPersona = '" + p_valor.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                    case CEN_Constantes.g_const_2: //existencia de preventa
                        l_sql = "SELECT COUNT (ntraPreventa) FROM tblPreventa WHERE ntraPreventa = '" + p_codigo.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                    case CEN_Constantes.g_const_3: //existencia de cliente
                        l_sql = "SELECT COUNT (codPersona) FROM tblPersona WHERE codPersona = '" + p_codigo.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                    case CEN_Constantes.g_const_4: //preventa pertenece a cliente
                        l_sql = "SELECT COUNT (codCliente) FROM tblPreventa WHERE ntraPreventa = '" + p_codigo.Trim() + "' AND codCliente = '" + p_valor.Trim() + "' AND marcaBaja = " + CEN_Constantes.g_const_0;
                        break;
                }

                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    SqlCommand cmd = new SqlCommand(l_sql, Connection);
                    cmd.CommandTimeout = CEN_Constantes.g_const_0;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        cont = Convert.ToInt32(dr[CEN_Constantes.g_const_0]);
                    }

                    dr.Close();
                }

                return cont;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }
    }
}
