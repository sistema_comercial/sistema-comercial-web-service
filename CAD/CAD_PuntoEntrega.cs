﻿using CEN;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CAD_PuntoEntrega
    {
        private SqlConnection Connection;
        public CEN_RespuestaWS registro_modificacion_punto_entrega(CEN_RequestPuntoEntrega entrega)
        {
            //DESCRIPCION: REGISTRAR PREVENTA
            CAD_Conector CadCx = new CAD_Conector();            //conexion
            SqlDataReader dr;                                   //data reader
            CEN_RespuestaWS Respuesta = new CEN_RespuestaWS();  //clase de registro de respuesta
            RptaRegistroPuntoEntrega re = null;                     //clase respuesta
            try
            {
                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    using (SqlCommand Command = new SqlCommand("pa_registrar_modificar_punto_entrega_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_proceso", SqlDbType.TinyInt).Value = entrega.proceso;

                        if (entrega.coordenadaX == null)
                            Command.Parameters.Add("@p_coordenadaX", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_coordenadaX", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = entrega.coordenadaX.Trim();

                        if (entrega.coordenadaY == null)
                            Command.Parameters.Add("@p_coordenadaY", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_coordenadaY", SqlDbType.VarChar, CEN_Constantes.g_const_100).Value = entrega.coordenadaY.Trim();

                        if (entrega.codUbigeo == null)
                            Command.Parameters.Add("@p_codUbigeo", SqlDbType.Char, CEN_Constantes.g_const_100).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_codUbigeo", SqlDbType.Char, CEN_Constantes.g_const_100).Value = entrega.codUbigeo.Trim();

                        if (entrega.direccion == null)
                            Command.Parameters.Add("@p_direccion", SqlDbType.VarChar, CEN_Constantes.g_const_200).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_direccion", SqlDbType.VarChar, CEN_Constantes.g_const_200).Value = entrega.direccion.Trim();

                        if (entrega.referencia == null)
                            Command.Parameters.Add("@p_referencia", SqlDbType.VarChar, CEN_Constantes.g_const_200).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_referencia", SqlDbType.VarChar, CEN_Constantes.g_const_200).Value = entrega.referencia.Trim();


                        Command.Parameters.Add("@p_ordenEntrega", SqlDbType.SmallInt).Value = entrega.ordenEntrega;
                        Command.Parameters.Add("@p_codPersona", SqlDbType.Int).Value = entrega.codPersona;

                        

                        if (entrega.usuario == null)
                            Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_usuario", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = entrega.usuario.Trim();

                        if (entrega.ip == null)
                            Command.Parameters.Add("@p_ip", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_ip", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = entrega.ip.Trim();

                        if (entrega.mac == null)
                            Command.Parameters.Add("@p_mac", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = CEN_Constantes.g_const_vacio;
                        else
                            Command.Parameters.Add("@p_mac", SqlDbType.VarChar, CEN_Constantes.g_const_20).Value = entrega.mac.Trim();

                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["flag"] != DBNull.Value && Convert.ToInt32(dr["flag"].ToString()) == CEN_Constantes.g_const_0)
                            {
                                re = new RptaRegistroPuntoEntrega();
                                re.ntraPuntoEntrega = int.Parse(dr["ntraPuntoEntrega"].ToString());
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                                string jsonData = JsonConvert.SerializeObject(re);
                                //Respuesta.Resultado = new { jsonData };
                                Respuesta.Resultado = new { result = re };

                            }
                            else
                            {
                                Respuesta.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                                Respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                Respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["msje"].ToString());
                                return Respuesta;
                            }
                        }

                    }
                    dr.Close();
                }
                return Respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }
    }
}
