﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
    public class CAD_Vendedor
    {
        private SqlConnection Connection;
        public CEN_Data_Vendedor obtenerVendedor(CEN_Request_Sincronizacion datos)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Vendedor respuesta = new CEN_Data_Vendedor(); //Clase de respuesta de consentimiento digital
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_buscar_vendedor_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codUsuario", SqlDbType.Int).Value = datos.codVendedor;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            //gbcon = Obtener_desc_gbcon(p_gbconpfij, Convert.ToInt32(dr["codigo"].ToString()));
                            CEN_Vendedor vendedor = new CEN_Vendedor();
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                if (dr["ntraUsuario"] != DBNull.Value)
                                    respuesta.vendedor.ntraUsuario = Convert.ToInt32(dr["ntraUsuario"].ToString());
                                if (dr["codPerfil"] != DBNull.Value)
                                    respuesta.vendedor.codPerfil = Convert.ToInt32(dr["codPerfil"].ToString());
                                if (dr["tipoDocumento"] != DBNull.Value)
                                    respuesta.vendedor.tipoDocumento = Convert.ToInt16(dr["tipoDocumento"].ToString());
                                if (dr["numeroDocumento"] != DBNull.Value)
                                    respuesta.vendedor.numeroDocumento = dr["numeroDocumento"].ToString();
                                if (dr["apellidoPaterno"] != DBNull.Value)
                                    respuesta.vendedor.apellidoPaterno = dr["apellidoPaterno"].ToString();
                                if (dr["apellidoMaterno"] != DBNull.Value)
                                    respuesta.vendedor.apellidoMaterno = dr["apellidoMaterno"].ToString();
                                if (dr["nombres"] != DBNull.Value)
                                    respuesta.vendedor.nombres = dr["nombres"].ToString();
                                if (dr["nombreCompleto"] != DBNull.Value)
                                    respuesta.vendedor.nombreCompleto = dr["nombreCompleto"].ToString();
                                if (dr["direccion"] != DBNull.Value)
                                    respuesta.vendedor.direccion = dr["direccion"].ToString();
                                if (dr["correo"] != DBNull.Value)
                                    respuesta.vendedor.correo = dr["correo"].ToString();
                                if (dr["telefono"] != DBNull.Value)
                                    respuesta.vendedor.telefono = dr["telefono"].ToString();
                                if (dr["celular"] != DBNull.Value)
                                    respuesta.vendedor.celular = dr["celular"].ToString();

                                respuesta.DescripcionResp = "";
                                respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                                respuesta.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);


                            }
                            else
                            {
                                respuesta.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                respuesta.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                respuesta.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                respuesta.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                            }

                        }
                    }
                    dr.Close();
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }


        }
    }
}
