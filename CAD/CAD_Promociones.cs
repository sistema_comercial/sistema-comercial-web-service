﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
    public class CAD_Promociones
    {
        private SqlConnection Connection;
        public CEN_Data_Promocion obtenerPromociones(DateTime fecha, int estado, int flag)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Promocion data_promocion = new CEN_Data_Promocion(); //Lista de promociones
            List<CEN_Promocion> lista_promocion_aux = new List<CEN_Promocion>(); //Lista de promociones
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_promociones_fecha", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_fech", SqlDbType.DateTime).Value = fecha;
                        Command.Parameters.Add("@p_estado", SqlDbType.Int).Value = estado;
                        Command.Parameters.Add("@p_flag", SqlDbType.Int).Value = flag;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Promocion promocion = new CEN_Promocion();
                                if (dr["ntraPromocion"] != DBNull.Value)
                                    promocion.ntraPromocion = Convert.ToInt32(dr["ntraPromocion"].ToString());
                                if (dr["flag"] != DBNull.Value)
                                    promocion.flag = Convert.ToInt32(dr["flag"].ToString());
                                if (dr["ntra_flag"] != DBNull.Value)
                                    promocion.ntra_flag = dr["ntra_flag"].ToString();
                                if (dr["desc_promo"] != DBNull.Value)
                                    promocion.desc_promo = dr["desc_promo"].ToString();
                                if (dr["horaInicial"] != DBNull.Value)
                                    promocion.horaInicial = dr["horaInicial"].ToString();
                                if (dr["horaFin"] != DBNull.Value)
                                    promocion.horaFin = dr["horaFin"].ToString();
                                if (dr["desc_det"] != DBNull.Value)
                                    promocion.desc_det = dr["desc_det"].ToString();
                                if (dr["valorInicial"] != DBNull.Value)
                                    promocion.valorInicial = dr["valorInicial"].ToString();
                                if (dr["valorFinal"] != DBNull.Value)
                                    promocion.valorFinal = dr["valorFinal"].ToString();
                                if (dr["detalle"] != DBNull.Value)
                                    promocion.detalle = Convert.ToInt32(dr["detalle"].ToString());
                                if (dr["estado"] != DBNull.Value)
                                    promocion.estado = Convert.ToInt32(dr["estado"].ToString());
                                promocion.listDetallePromocion = obtenerDetallePromociones(promocion.ntraPromocion, promocion.flag);
                                data_promocion.listPromocion.Add(promocion);
                                promocion = new CEN_Promocion();
                            }
                            else
                            {
                                data_promocion.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_promocion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_promocion.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_promocion.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_promocion;
                            }
                        }

                        if (data_promocion.listPromocion.Count > CEN_Constantes.g_const_0)
                        {

                            //List<int> lista_promocion_aux2 = data_promocion.listPromocion.Select(x =>  x.ntraPromocion).Distinct().ToList();

                            var distinctPromociones = data_promocion.listPromocion
                                                    .Select(m => new { m.ntraPromocion, m.desc_promo, m.horaInicial, m.horaFin })
                                                    .Distinct()
                                                    .ToList();
                            foreach (var item in distinctPromociones)
                            {
                                CEN_Promocion aux_promo3 = new CEN_Promocion();
                                aux_promo3.ntraPromocion = item.ntraPromocion;
                                aux_promo3.desc_promo = item.desc_promo;
                                aux_promo3.horaInicial = item.horaInicial;
                                aux_promo3.horaFin = item.horaFin;
                                lista_promocion_aux.Add(aux_promo3);
                            }
                            for (int i = CEN_Constantes.g_const_0; i < lista_promocion_aux.Count; i++)
                            {
                                CEN_Promocion aux_promo = new CEN_Promocion();
                                //REGISTRO PARA DESCRIPCION DE PROMOCION (100)
                                aux_promo.ntraPromocion = lista_promocion_aux[i].ntraPromocion;
                                aux_promo.flag = CEN_Constantes.g_const_100;
                                aux_promo.ntra_flag = aux_promo.ntraPromocion + CEN_Constantes.g_const_vacio + aux_promo.flag;
                                aux_promo.desc_promo = lista_promocion_aux[i].desc_promo;
                                aux_promo.horaInicial = CEN_Constantes.g_const_vacio;
                                aux_promo.horaFin = CEN_Constantes.g_const_vacio;
                                aux_promo.detalle = CEN_Constantes.g_const_0;
                                aux_promo.estado = CEN_Constantes.g_const_0;

                                data_promocion.listPromocion.Add(aux_promo);
                                //REGISTRO PARA HORAS DE PROMOCION (101)
                                aux_promo = new CEN_Promocion();
                                aux_promo.ntraPromocion = lista_promocion_aux[i].ntraPromocion;
                                aux_promo.flag = CEN_Constantes.g_const_101;
                                aux_promo.ntra_flag = aux_promo.ntraPromocion + CEN_Constantes.g_const_vacio + aux_promo.flag;
                                aux_promo.desc_promo = CEN_Constantes.g_const_vacio;
                                aux_promo.horaInicial = lista_promocion_aux[i].horaInicial;
                                aux_promo.horaFin = lista_promocion_aux[i].horaFin;
                                aux_promo.valorInicial = lista_promocion_aux[i].horaInicial;
                                aux_promo.valorFinal = lista_promocion_aux[i].horaFin;
                                aux_promo.detalle = CEN_Constantes.g_const_0;
                                aux_promo.estado = CEN_Constantes.g_const_0;



                                data_promocion.listPromocion.Add(aux_promo);
                            }


                        }


                        data_promocion.DescripcionResp = CEN_Constantes.g_const_vacio;
                        data_promocion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        data_promocion.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                        data_promocion.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_vacio;
                    }
                    dr.Close();
                }
                return data_promocion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Promocion obtenerPromocionesDescripcion()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Promocion data_promocion = new CEN_Data_Promocion(); //Lista de promociones
            List<CEN_Promocion> lista_promocion_aux = new List<CEN_Promocion>(); //Lista de promociones
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_promociones_desc_sinc", Connection))
                    {
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();
                        CEN_Promocion promocion = new CEN_Promocion();
                        while (dr.Read())
                        {

                            promocion = new CEN_Promocion();
                            if (dr["ntraPromocion"] != DBNull.Value)
                                promocion.ntraPromocion = Convert.ToInt32(dr["ntraPromocion"].ToString());
                            if (dr["desc_promo"] != DBNull.Value)
                                promocion.desc_promo = dr["desc_promo"].ToString();
                            data_promocion.listPromocion.Add(promocion);
                        }


                        data_promocion.DescripcionResp = CEN_Constantes.g_const_vacio;
                        data_promocion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        data_promocion.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                        data_promocion.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_vacio;
                    }
                    dr.Close();
                }
                return data_promocion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        private List<CEN_Detalle_Promocion> obtenerDetallePromociones(int p_ntra, int p_flag)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            List<CEN_Detalle_Promocion> list_detalle_promocion = new List<CEN_Detalle_Promocion>(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_detalle_flag_promocion", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_ntra", SqlDbType.Int).Value = p_ntra;
                        Command.Parameters.Add("@p_flag", SqlDbType.Int).Value = p_flag;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Detalle_Promocion det_promocion = new CEN_Detalle_Promocion();
                                if (dr["ntra_flag"] != DBNull.Value)
                                    det_promocion.ntra_flag = dr["ntra_flag"].ToString();
                                if (dr["flag"] != DBNull.Value)
                                    det_promocion.flag = Convert.ToInt32(dr["flag"].ToString());
                                if (dr["descripcion"] != DBNull.Value)
                                    det_promocion.descripcion = dr["descripcion"].ToString();
                                if (dr["valorEntero1"] != DBNull.Value)
                                    det_promocion.valorEntero1 = Convert.ToInt32(dr["valorEntero1"].ToString());
                                if (dr["valorEntero2"] != DBNull.Value)
                                    det_promocion.valorEntero2 = Convert.ToInt32(dr["valorEntero2"].ToString());
                                if (dr["valorMoneda1"] != DBNull.Value)
                                    det_promocion.valorMoneda1 = Convert.ToDouble(dr["valorMoneda1"].ToString());
                                if (dr["valorMoneda2"] != DBNull.Value)
                                    det_promocion.valorMoneda2 = Convert.ToDouble(dr["valorMoneda2"].ToString());
                                if (dr["valorCadena1"] != DBNull.Value)
                                    det_promocion.valorCadena1 = dr["valorCadena1"].ToString();
                                if (dr["valorCadena2"] != DBNull.Value)
                                    det_promocion.valorCadena2 = dr["valorCadena2"].ToString();
                                if (dr["valorFecha1"] != DBNull.Value)
                                    det_promocion.valorFecha1 = Convert.ToDateTime(dr["valorFecha1"].ToString());
                                if (dr["valorFecha2"] != DBNull.Value)
                                    det_promocion.valorFecha2 = Convert.ToDateTime(dr["valorFecha2"].ToString());
                                if (dr["estado"] != DBNull.Value)
                                    det_promocion.estado = Convert.ToInt32(dr["estado"].ToString());

                                list_detalle_promocion.Add(det_promocion);
                                det_promocion = new CEN_Detalle_Promocion();
                            }
                            else
                            {
                                return list_detalle_promocion;
                            }
                        }
                    }
                    dr.Close();
                }
                return list_detalle_promocion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }
    }
}
