﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
    public class CAD_Sunat
    {
        private SqlConnection Connection;
        public CEN_Sunat ObtenerDataSunatBD(string rUC)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            long p_ruc = CEN_Constantes.g_const_0;
            CEN_Sunat data = new CEN_Sunat();

            CAD_Ubigeo cad_ubigeo = new CAD_Ubigeo();
            List<CEN_Departamento> listdep = new List<CEN_Departamento>();
            List<CEN_Provincia> listprov = new List<CEN_Provincia>();
            List<CEN_Distrito> listDist = new List<CEN_Distrito>();
            string codDep = CEN_Constantes.g_const_vacio;
            string codProv = CEN_Constantes.g_const_vacio;
            string codDis = CEN_Constantes.g_const_vacio;

            try
            {
                p_ruc = long.Parse(rUC);
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_cliente_sunat", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_ruc", SqlDbType.BigInt).Value = p_ruc;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            data.RUC = dr["RUC"].ToString();
                            data.razonSocial = dr["razonSocial"].ToString();
                            data.estCont = dr["estCont"].ToString();
                            data.condDom = dr["condDom"].ToString();
                            data.ubigeo = dr["ubigeo"].ToString();
                            data.tipVia = dr["tipVia"].ToString();
                            data.nomVia = dr["nomVia"].ToString();
                            data.codZona = dr["codZona"].ToString();
                            data.nomZona = dr["nomZona"].ToString();
                            data.numero = dr["numero"].ToString();
                            data.interior = dr["interior"].ToString();
                            data.lote = dr["lote"].ToString();
                            data.departamento = dr["departamento"].ToString();
                            data.manzana = dr["manzana"].ToString();
                            data.kilometro = dr["kilometro"].ToString();

                            //"CAL.LORETO NRO. 165 URB. PATAZCA (4TO PISO , POR GRUPO LA REPUBLICA) LAMBAYEQUE - CHICLAYO - CHICLAYO"
                            if (!(data.nomVia.Trim().Equals("-")))
                            {
                                data.direccionFizcal = data.direccionFizcal + data.tipVia + " " + data.nomVia + " ";
                            }

                            if (!(data.numero.Trim().Equals("-")))
                            {
                                data.direccionFizcal = data.direccionFizcal + "NRO. " + data.numero + " ";
                            }
                            

                            if (!(data.nomZona.Trim().Equals("-")))
                            {
                                data.direccionFizcal = data.direccionFizcal + data.codZona + " " + data.nomZona + " ";
                            }

                            if (!(data.interior.Trim().Equals("-")))
                            {
                                data.direccionFizcal = data.direccionFizcal + "interior. " + data.interior + " ";
                            }

                            if (!(data.lote.Trim().Equals("-")))
                            {
                                data.direccionFizcal = data.direccionFizcal + "lote. " + data.lote + " ";
                            }
                            
                            if(data.ubigeo.Trim().Length == CEN_Constantes.g_const_6)
                            {
                                codDep = data.ubigeo.Substring(CEN_Constantes.g_const_0, CEN_Constantes.g_const_2);
                                codProv = data.ubigeo.Substring(CEN_Constantes.g_const_2, CEN_Constantes.g_const_2);
                                codDis = data.ubigeo.Substring(CEN_Constantes.g_const_4, CEN_Constantes.g_const_2);

                                listdep = cad_ubigeo.ObtenerDepartamentos(codDep);
                                listprov = cad_ubigeo.ObtenerProvincias(codDep,codProv);
                                listDist = cad_ubigeo.ObtenerDistritos(codDep, codProv, codDis);

                                if(listdep.Count > CEN_Constantes.g_const_0)
                                {
                                    data.direccionFizcal = data.direccionFizcal + listdep[CEN_Constantes.g_const_0].nombre + " - ";

                                }
                                if (listprov.Count > CEN_Constantes.g_const_0)
                                {
                                    data.direccionFizcal = data.direccionFizcal + listprov[CEN_Constantes.g_const_0].nombre + " - ";

                                }
                                if (listDist.Count > CEN_Constantes.g_const_0)
                                {
                                    data.direccionFizcal = data.direccionFizcal + listDist[CEN_Constantes.g_const_0].nombre ;

                                }

                            }
                        }
                    }
                    dr.Close();
                }
                
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }
    }
}
