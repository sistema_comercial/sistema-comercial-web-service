﻿using CEN;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CAD
{
    class CAD_Conector
    {
        public String CxSQL()
        {
            //DESCRIPCION : CONEXION CON SQL SERVER
            try { return ConfigurationManager.ConnectionStrings[CEN_Constantes.ConexionSQL].ConnectionString.ToString(); }
            catch (Exception ex) { throw ex; }
        }

        
        public void AbrirConexion(SqlConnection Connection)
        {
            //DESCRIPCION: INICIA CONEXION CON DB SQL
            try
            {
                if (Connection.State == ConnectionState.Closed) Connection.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CerrarConexion(SqlConnection Connection)
        {
            //DESCRIPCION: CERRAR CONEXION CON DB SQL
            try
            {
                if (Connection.State == ConnectionState.Open) Connection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
