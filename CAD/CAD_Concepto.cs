﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
   
    public class CAD_Concepto
    {
        private SqlConnection Connection;

        public string obtener_descripcion_concepto_general(CEN_Concepto objConcepto)
        {
            //DESCRIPCION: OBTENER DESCRIPCION DE CONCEPTO
            CAD_Conector CadCx = new CAD_Conector();    //conexión
            string descripcion = null;                  //descripcion
            try
            {
                string l_sql = "SELECT descripcion FROM tblConcepto WHERE codConcepto = " + Convert.ToInt32(objConcepto.codConcepto) + " AND correlativo = " + Convert.ToInt32(objConcepto.correlativo) + " AND marcaBaja = " + CEN_Constantes.g_const_0;
                using (Connection = new SqlConnection(CadCx.CxSQL()))
                {
                    Connection.Open();
                    SqlCommand cmd = new SqlCommand(l_sql, Connection);
                    cmd.CommandTimeout = CEN_Constantes.g_const_0;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        descripcion = Convert.ToString(dr["descripcion"]);
                    }

                    dr.Close();
                }

                return descripcion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Connection.Close();
            }
        }
    }
}
