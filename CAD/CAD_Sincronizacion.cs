﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CAD_Sincronizacion
    {
        private SqlConnection Connection;

        public CEN_Data_Categoria obtenerCategorias()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Categoria data_categoria = new CEN_Data_Categoria(); //Lista de rutas
            try
            {
                int dia = (int)System.DateTime.Now.DayOfWeek;
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_categorias", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            CEN_Categoria categoria = new CEN_Categoria();
                            if (dr["ntraCategoria"] != DBNull.Value)
                                categoria.ntraCategoria = Convert.ToInt32(dr["ntraCategoria"].ToString());
                            if (dr["descripcion"] != DBNull.Value)
                                categoria.descripcion = dr["descripcion"].ToString();

                            data_categoria.listCategoria.Add(categoria);
                            categoria = new CEN_Categoria();
                        }

                        if (data_categoria.listCategoria.Count > CEN_Constantes.g_const_0)
                        {

                            data_categoria.DescripcionResp = "";
                            data_categoria.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_categoria.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_categoria.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_categoria.DescripcionResp = "NO HAY CATEGORIAS REGISTRADAS";
                            data_categoria.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_categoria.ErrorWebSer.CodigoErr = 1000;
                            data_categoria.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_categoria;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        public CEN_Data_Almacen obtenerAlmacenes()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Almacen data_almacen = new CEN_Data_Almacen(); //Lista de almacenes
            try
            {
                int dia = (int)System.DateTime.Now.DayOfWeek;
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_almacen_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Almacen almacen = new CEN_Almacen();
                                if (dr["ntraAlmacen"] != DBNull.Value)
                                    almacen.ntraAlmacen = Convert.ToInt32(dr["ntraAlmacen"].ToString());
                                if (dr["abreviatura"] != DBNull.Value)
                                    almacen.abreviatura = dr["abreviatura"].ToString();
                                if (dr["descripcion"] != DBNull.Value)
                                    almacen.descripcion = dr["descripcion"].ToString();

                                data_almacen.listAlmacen.Add(almacen);
                                almacen = new CEN_Almacen();
                            }
                            else
                            {
                                data_almacen.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_almacen.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_almacen.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_almacen.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_almacen;
                            }
                        }

                        if (data_almacen.listAlmacen.Count > CEN_Constantes.g_const_0)
                        {

                            data_almacen.DescripcionResp = "";
                            data_almacen.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_almacen.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_almacen.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_almacen.DescripcionResp = "NO HAY ALMACENES REGISTRADAS";
                            data_almacen.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_almacen.ErrorWebSer.CodigoErr = 1000;
                            data_almacen.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_almacen;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Inventario obtenerInventarios()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Inventario data_inventario = new CEN_Data_Inventario(); //Lista de inventarios
            try
            {
                int dia = (int)System.DateTime.Now.DayOfWeek;
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_inventario_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Inventario inventario = new CEN_Inventario();
                                if (dr["ntraInventario"] != DBNull.Value)
                                    inventario.ntraInventario = Convert.ToInt32(dr["ntraInventario"].ToString());
                                if (dr["codAlmacen"] != DBNull.Value)
                                    inventario.codAlmacen = Convert.ToInt32(dr["codAlmacen"].ToString());
                                if (dr["stock"] != DBNull.Value)
                                    inventario.stock = Convert.ToInt32(dr["stock"].ToString());
                                if (dr["codProducto"] != DBNull.Value)
                                    inventario.codProducto = dr["codProducto"].ToString();

                                data_inventario.listInventario.Add(inventario);
                                inventario = new CEN_Inventario();
                            }
                            else
                            {
                                data_inventario.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_inventario.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_inventario.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_inventario.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_inventario;
                            }
                        }

                        if (data_inventario.listInventario.Count > CEN_Constantes.g_const_0)
                        {

                            data_inventario.DescripcionResp = "";
                            data_inventario.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_inventario.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_inventario.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_inventario.DescripcionResp = "NO HAY INVENTARIOS REGISTRADOS";
                            data_inventario.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_inventario.ErrorWebSer.CodigoErr = 1000;
                            data_inventario.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_inventario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_PuntosEntrega obtenerlistaPuntosEntrega()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_PuntosEntrega data_puntoentrega = new CEN_Data_PuntosEntrega(); //Lista de puntos de entrega
            try
            {
                int dia = (int)System.DateTime.Now.DayOfWeek;
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_puntos_entregas", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_PuntosEntrega punto_entrega = new CEN_PuntosEntrega();
                                if (dr["ntraPuntoEntrega"] != DBNull.Value)
                                    punto_entrega.ntraPuntoEntrega = Convert.ToInt32(dr["ntraPuntoEntrega"].ToString());
                                if (dr["coordenadaX"] != DBNull.Value)
                                    punto_entrega.coordenadaX = dr["coordenadaX"].ToString();
                                if (dr["coordenadaY"] != DBNull.Value)
                                    punto_entrega.coordenadaY = dr["coordenadaY"].ToString();
                                if (dr["direccion"] != DBNull.Value)
                                    punto_entrega.direccion = dr["direccion"].ToString();
                                if (dr["referencia"] != DBNull.Value)
                                    punto_entrega.referencia = dr["referencia"].ToString();
                                if (dr["ordenEntrega"] != DBNull.Value)
                                    punto_entrega.ordenEntrega = Convert.ToInt16(dr["ordenEntrega"].ToString());
                                if (dr["codPersona"] != DBNull.Value)
                                    punto_entrega.codPersona = Convert.ToInt32(dr["codPersona"].ToString());
                                if (dr["numeroDocumento"] != DBNull.Value)
                                    punto_entrega.numeroDocumento = dr["numeroDocumento"].ToString();
                                if (dr["tipoDocumento"] != DBNull.Value)
                                    punto_entrega.tipoDocumento = Convert.ToInt32(dr["tipoDocumento"].ToString());

                                data_puntoentrega.listEntrega.Add(punto_entrega);
                                punto_entrega = new CEN_PuntosEntrega();
                            }
                            else
                            {
                                data_puntoentrega.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_puntoentrega.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_puntoentrega.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_puntoentrega.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data_puntoentrega;
                            }
                        }

                        if (data_puntoentrega.listEntrega.Count > CEN_Constantes.g_const_0)
                        {

                            data_puntoentrega.DescripcionResp = "";
                            data_puntoentrega.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_puntoentrega.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_puntoentrega.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_puntoentrega.DescripcionResp = "NO HAY INVENTARIOS REGISTRADOS";
                            data_puntoentrega.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_puntoentrega.ErrorWebSer.CodigoErr = 1000;
                            data_puntoentrega.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_puntoentrega;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Concepto obtenerlistaConceptos()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Concepto data = new CEN_Data_Concepto(); //Lista de conceptos
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_conceptos_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codConcepto"].ToString()) > CEN_Constantes.g_const_0)
                            {
                                CEN_Concepto concepto = new CEN_Concepto();
                                if (dr["codConcepto"] != DBNull.Value)
                                    concepto.codConcepto = Convert.ToInt32(dr["codConcepto"].ToString());
                                if (dr["correlativo"] != DBNull.Value)
                                    concepto.correlativo = Convert.ToInt32(dr["correlativo"].ToString());
                                if (dr["descripcion"] != DBNull.Value)
                                    concepto.descripcion = dr["descripcion"].ToString();


                                data.listConcepto.Add(concepto);
                                concepto = new CEN_Concepto();
                            }
                            else
                            {
                                data.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codConcepto"].ToString());
                                data.ErrorWebSer.DescripcionErr = Convert.ToString(dr["descripcion"]);
                                return data;
                            }
                        }

                        if (data.listConcepto.Count > CEN_Constantes.g_const_0)
                        {

                            data.DescripcionResp = "";
                            data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data.DescripcionResp = "NO HAY REGISTROS";
                            data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data.ErrorWebSer.CodigoErr = 1000;
                            data.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Precio obtenerlistaPrecios()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Precio data = new CEN_Data_Precio(); //Lista de conceptos
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_precios_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Precio precio = new CEN_Precio();
                                if (dr["ntraPrecio"] != DBNull.Value)
                                    precio.ntraPrecio = Convert.ToInt32(dr["ntraPrecio"].ToString());
                                if (dr["tipoListaPrecio"] != DBNull.Value)
                                    precio.tipoListaPrecio = Convert.ToInt16(dr["tipoListaPrecio"].ToString());
                                if (dr["codProducto"] != DBNull.Value)
                                    precio.codProducto = dr["codProducto"].ToString();
                                if (dr["precioVenta"] != DBNull.Value)
                                    precio.precioVenta = Convert.ToDouble(dr["precioVenta"].ToString());
                                if (dr["descripcion"] != DBNull.Value)
                                    precio.descripcion = dr["descripcion"].ToString();


                                data.listPrecio.Add(precio);
                                precio = new CEN_Precio();
                            }
                            else
                            {
                                data.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                                return data;
                            }
                        }

                        if (data.listPrecio.Count > CEN_Constantes.g_const_0)
                        {

                            data.DescripcionResp = "";
                            data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data.DescripcionResp = "NO HAY REGISTROS";
                            data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data.ErrorWebSer.CodigoErr = 1000;
                            data.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


        public CEN_Data_Parametro obtenerParametros()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Parametro data_parametros = new CEN_Data_Parametro(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_parametro_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            CEN_Parametro parametro = new CEN_Parametro();
                            if (dr["codParametro"] != DBNull.Value)
                                parametro.codParametro = Convert.ToInt32(dr["codParametro"].ToString());
                            if (dr["tipo"] != DBNull.Value)
                                parametro.tipo = Convert.ToInt32(dr["tipo"].ToString());
                            if (dr["valorEntero1"] != DBNull.Value)
                                parametro.valorEntero1 = Convert.ToInt32(dr["valorEntero1"].ToString());
                            if (dr["valorEntero2"] != DBNull.Value)
                                parametro.valorEntero2 = Convert.ToInt32(dr["valorEntero2"].ToString());
                            if (dr["valorCaneda1"] != DBNull.Value)
                                parametro.valorCaneda1 = dr["valorCaneda1"].ToString();
                            if (dr["valorCaneda2"] != DBNull.Value)
                                parametro.valorCaneda2 = dr["valorCaneda2"].ToString();
                            if (dr["valorMoneda1"] != DBNull.Value)
                                parametro.valorMoneda1 = Convert.ToDouble(dr["valorMoneda1"].ToString());
                            if (dr["valorMoneda2"] != DBNull.Value)
                                parametro.valorMoneda2 = Convert.ToDouble(dr["valorMoneda2"].ToString());
                            if (dr["valorFloat1"] != DBNull.Value)
                                parametro.valorFloat1 = Convert.ToInt64(dr["valorFloat1"].ToString());
                            if (dr["valorFloat2"] != DBNull.Value)
                                parametro.valorFloat2 = Convert.ToInt64(dr["valorFloat2"].ToString());
                            if (dr["valorFecha1"] != DBNull.Value)
                                parametro.valorFecha1 = Convert.ToDateTime(dr["valorFecha1"].ToString());
                            if (dr["valorFecha2"] != DBNull.Value)
                                parametro.valorFecha2 = Convert.ToDateTime(dr["valorFecha2"].ToString());

                            data_parametros.listParametro.Add(parametro);
                            parametro = new CEN_Parametro();
                        }

                        if (data_parametros.listParametro.Count > CEN_Constantes.g_const_0)
                        {

                            data_parametros.DescripcionResp = "";
                            data_parametros.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_parametros.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_parametros.ErrorWebSer.DescripcionErr = "";
                        }
                        else
                        {
                            data_parametros.DescripcionResp = "NO HAY PARAMETROS REGISTRADOS";
                            data_parametros.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                            data_parametros.ErrorWebSer.CodigoErr = 1000;
                            data_parametros.ErrorWebSer.DescripcionErr = "SIN REGISTROS";
                        }
                    }
                    dr.Close();
                }
                return data_parametros;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }



        public CEN_Parametro obtenerParametroIndividual(int codParametro)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Parametro parametro = new CEN_Parametro(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_buscar_det_parametro", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_cod", SqlDbType.Int).Value = codParametro;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["codParametro"] != DBNull.Value)
                                parametro.codParametro = Convert.ToInt32(dr["codParametro"].ToString());
                            if (dr["tipo"] != DBNull.Value)
                                parametro.tipo = Convert.ToInt32(dr["tipo"].ToString());
                            if (dr["valorEntero1"] != DBNull.Value)
                                parametro.valorEntero1 = Convert.ToInt32(dr["valorEntero1"].ToString());
                            if (dr["valorEntero2"] != DBNull.Value)
                                parametro.valorEntero2 = Convert.ToInt32(dr["valorEntero2"].ToString());
                            if (dr["valorCaneda1"] != DBNull.Value)
                                parametro.valorCaneda1 = dr["valorCaneda1"].ToString();
                            if (dr["valorCaneda2"] != DBNull.Value)
                                parametro.valorCaneda2 = dr["valorCaneda2"].ToString();
                            if (dr["valorMoneda1"] != DBNull.Value)
                                parametro.valorMoneda1 = Convert.ToDouble(dr["valorMoneda1"].ToString());
                            if (dr["valorMoneda2"] != DBNull.Value)
                                parametro.valorMoneda2 = Convert.ToDouble(dr["valorMoneda2"].ToString());
                            if (dr["valorFloat1"] != DBNull.Value)
                                parametro.valorFloat1 = Convert.ToInt64(dr["valorFloat1"].ToString());
                            if (dr["valorFloat2"] != DBNull.Value)
                                parametro.valorFloat2 = Convert.ToInt64(dr["valorFloat2"].ToString());
                            if (dr["valorFecha1"] != DBNull.Value)
                                parametro.valorFecha1 = Convert.ToDateTime(dr["valorFecha1"].ToString());
                            if (dr["valorFecha2"] != DBNull.Value)
                                parametro.valorFecha2 = Convert.ToDateTime(dr["valorFecha2"].ToString());

                        }

                    }
                    dr.Close();
                }
                return parametro;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }



    }
}
