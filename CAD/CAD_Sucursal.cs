﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
    public class CAD_Sucursal
    {
        private SqlConnection Connection;
        public CEN_RespuestaWSSucursal ObtenerSucursales()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_RespuestaWSSucursal respuestaWS = new CEN_RespuestaWSSucursal();
            CEN_Response_Sucursal respuesta = new CEN_Response_Sucursal(); //Clase de respuesta 
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_sucursales", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            //gbcon = Obtener_desc_gbcon(p_gbconpfij, Convert.ToInt32(dr["codigo"].ToString()));
                            CEN_Sucursal sucursal = new CEN_Sucursal();
                            if (dr["ntraSucursal"] != DBNull.Value)
                                sucursal.ntraSucursal = Convert.ToInt32(dr["ntraSucursal"].ToString());
                            if (dr["descripcion"] != DBNull.Value)
                                sucursal.descripcion = dr["descripcion"].ToString();
                            if (dr["codUbigeo"] != DBNull.Value)
                                sucursal.codUbigeo = dr["codUbigeo"].ToString();
                            if (dr["factor"] != DBNull.Value)
                                sucursal.factor = Convert.ToDouble(dr["factor"].ToString());

                            respuesta.listSucursales.Add(sucursal);

                        }
                        respuesta.DescripcionResp = CEN_Constantes.g_const_consultaExitosa;
                        respuestaWS.ResultadoSucursal = new { respuesta };
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                        respuestaWS.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_consultaExitosa;
                    }
                    dr.Close();
                }
                return respuestaWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }
    }
}
