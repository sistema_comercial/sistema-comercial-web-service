﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CEN;

namespace CAD
{
    public class CAD_Productos
    {
        private SqlConnection Connection;

        public CEN_Data_Producto obtenerListaProductos()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Producto data_producto = new CEN_Data_Producto(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_productos_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Producto producto = new CEN_Producto();
                                if (dr["codProducto"] != DBNull.Value)
                                    producto.codProducto = dr["codProducto"].ToString();
                                if (dr["descripcion"] != DBNull.Value)
                                    producto.descripcion = dr["descripcion"].ToString();
                                if (dr["cod_des"] != DBNull.Value)
                                    producto.cod_des = dr["cod_des"].ToString();
                                if (dr["codCategoria"] != DBNull.Value)
                                    producto.codCategoria = Convert.ToInt32(dr["codCategoria"].ToString());
                                if (dr["codUnidadBaseventa"] != DBNull.Value)
                                    producto.codUnidadBaseventa = Convert.ToInt32(dr["codUnidadBaseventa"].ToString());
                                if (dr["tipoProducto"] != DBNull.Value)
                                    producto.tipoProducto = Convert.ToInt32(dr["tipoProducto"].ToString());

                                data_producto.listProducto.Add(producto);
                                producto = new CEN_Producto();
                            }
                            else
                            {
                                data_producto.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_producto.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_producto.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_producto.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                            }
                        }

                        if (data_producto.listProducto.Count > CEN_Constantes.g_const_0)
                        {

                            data_producto.DescripcionResp = CEN_Constantes.g_const_vacio;
                            data_producto.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_producto.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_producto.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_vacio;
                        }
                    }
                    dr.Close();
                }
                return data_producto;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public CEN_Data_Presentacion obtenerListaPresentaciones()
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader
            CEN_Data_Presentacion data_Presentacion = new CEN_Data_Presentacion(); //Lista de rutas
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_presentacion_productos_sinc", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            if (Convert.ToInt32(dr["codigo"].ToString()) == CEN_Constantes.g_const_2000)
                            {
                                CEN_Presentacion presentacion = new CEN_Presentacion();
                                if (dr["codProducto"] != DBNull.Value)
                                    presentacion.codProducto = dr["codProducto"].ToString();
                                if (dr["codPresentancion"] != DBNull.Value)
                                    presentacion.codPresentancion = Convert.ToInt32(dr["codPresentancion"].ToString());
                                if (dr["cantidadUnidadBase"] != DBNull.Value)
                                    presentacion.cantidadUnidadBase = Convert.ToInt32(dr["cantidadUnidadBase"].ToString());

                                data_Presentacion.listPresentacion.Add(presentacion);
                                presentacion = new CEN_Presentacion();
                            }
                            else
                            {
                                data_Presentacion.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                                data_Presentacion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                                data_Presentacion.ErrorWebSer.CodigoErr = Convert.ToInt32(dr["codigo"].ToString());
                                data_Presentacion.ErrorWebSer.DescripcionErr = Convert.ToString(dr["mensaje"]);
                            }
                            
                        }
                        if (data_Presentacion.listPresentacion.Count > CEN_Constantes.g_const_0)
                        {

                            data_Presentacion.DescripcionResp = CEN_Constantes.g_const_vacio;
                            data_Presentacion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            data_Presentacion.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                            data_Presentacion.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_vacio;
                        }
                    }
                    dr.Close();
                }
                return data_Presentacion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }
    }
}
