﻿using CEN;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CAD_Ubigeo
    {
        private SqlConnection Connection;
        public List<CEN_Departamento> ObtenerDepartamentos(string p_codDep)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader

            List<CEN_Departamento> listDepartamento = new List<CEN_Departamento>();
            CEN_Departamento departamento = new CEN_Departamento();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_departamentos", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codDep", SqlDbType.VarChar, CEN_Constantes.g_const_2).Value = p_codDep;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            departamento = new CEN_Departamento();
                            departamento.codDepartamento = dr["codDepartamento"].ToString();
                            departamento.nombre = dr["nombre"].ToString();
                            listDepartamento.Add(departamento);
                        }
                    }
                    dr.Close();
                }
                return listDepartamento;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public List<CEN_Provincia> ObtenerProvincias(string p_codDep,string p_codProv)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader

            List<CEN_Provincia> listProvincia = new List<CEN_Provincia>();
            CEN_Provincia provincia = new CEN_Provincia();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_provincias", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codDep", SqlDbType.VarChar, CEN_Constantes.g_const_2).Value = p_codDep;
                        Command.Parameters.Add("@p_codProv", SqlDbType.VarChar, CEN_Constantes.g_const_2).Value = p_codProv;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            provincia = new CEN_Provincia();
                            provincia.codDepartamento = dr["codDepartamento"].ToString();
                            provincia.codProvincia = dr["codProvincia"].ToString();
                            provincia.nombre = dr["nombre"].ToString();
                            listProvincia.Add(provincia);
                        }
                    }
                    dr.Close();
                }
                return listProvincia;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }

        public List<CEN_Distrito> ObtenerDistritos(string p_codDep, string p_codProv, string p_codDis)
        {
            CAD_Conector conexion = new CAD_Conector();
            SqlDataReader dr; //Data reader

            List<CEN_Distrito> listDistrito = new List<CEN_Distrito>();
            CEN_Distrito distrito = new CEN_Distrito();
            try
            {
                using (Connection = new SqlConnection(conexion.CxSQL()))
                {
                    conexion.AbrirConexion(Connection);
                    using (SqlCommand Command = new SqlCommand("pa_listar_distritos", Connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@p_codDep", SqlDbType.VarChar, CEN_Constantes.g_const_2).Value = p_codDep;
                        Command.Parameters.Add("@p_codProv", SqlDbType.VarChar, CEN_Constantes.g_const_2).Value = p_codProv;
                        Command.Parameters.Add("@p_codDis", SqlDbType.VarChar, CEN_Constantes.g_const_2).Value = p_codDis;
                        Command.CommandTimeout = CEN_Constantes.g_const_0;
                        dr = Command.ExecuteReader();

                        while (dr.Read())
                        {
                            distrito = new CEN_Distrito();
                            distrito.codDepartamento = dr["codDepartamento"].ToString();
                            distrito.codProvincia = dr["codProvincia"].ToString();
                            distrito.codDistrito = dr["codDistrito"].ToString();
                            distrito.nombre = dr["nombre"].ToString();
                            distrito.ubigeo = dr["ubigeo"].ToString();
                            listDistrito.Add(distrito);
                        }
                    }
                    dr.Close();
                }
                return listDistrito;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conexion.CerrarConexion(Connection);
            }
        }


    }
}
