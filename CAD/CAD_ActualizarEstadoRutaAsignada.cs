﻿using CEN;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class CAD_ActualizarEstadoRutaAsignada
    {
        public CEN_RespuestaEstadoRutasAsigWS ActualizarEstadoRutaAsignada(CEN_RequestEstadoRutasAsig ObjEsRuA)
        {

            SqlConnection con = null;
            SqlCommand cmd = null;
            CEN_RespuestaEstadoRutasAsig respRuAs = null;
            CEN_RespuestaEstadoRutasAsigWS RespuestaEs = new CEN_RespuestaEstadoRutasAsigWS();
            CAD_Conector CadCx = new CAD_Conector();
            int result = 0;
            string mensaje = "";
            int codRutaResputa = 0;


            try
            {

                con = new SqlConnection(CadCx.CxSQL());
                cmd = new SqlCommand("pa_actualizar_estado_rutas_asignadas", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@codRuta", ObjEsRuA.codRuta);
                cmd.Parameters.AddWithValue("@estado", ObjEsRuA.estado);
                cmd.Parameters.AddWithValue("@codUsuario", ObjEsRuA.codUsuario);

                cmd.Parameters.Add("@resultado", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@msje", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@codRutaResputa", SqlDbType.Int).Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                result = Convert.ToInt32(cmd.Parameters["@resultado"].Value);
                mensaje = Convert.ToString(cmd.Parameters["@msje"].Value);
                codRutaResputa = Convert.ToInt32(cmd.Parameters["@codRutaResputa"].Value);

                if (cmd.Parameters["@resultado"].Value != DBNull.Value && result == 0)
                {
                    respRuAs = new CEN_RespuestaEstadoRutasAsig();
                    respRuAs.codRuta = codRutaResputa;
                    RespuestaEs.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                    RespuestaEs.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    RespuestaEs.ErrorWebSer.DescripcionErr = mensaje;
                    RespuestaEs.ResultadoEstadoRA = new { respRuAs };
                }
                else
                {

                    RespuestaEs.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                    RespuestaEs.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                    RespuestaEs.ErrorWebSer.DescripcionErr = mensaje;
                    return RespuestaEs;
                }



                return RespuestaEs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }


        }
    }
}
