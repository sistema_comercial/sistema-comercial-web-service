﻿using CAD;
using CEN;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLN_Preventa
    {
        public CEN_RespuestaWSRegPreventa registro_modificacion_preventa(CEN_RequestPreventa requestPreventa, string strObj)
        {
            //DESCRIPCION: LOGICA REGISTRAR CLIENTE
            CEN_RespuestaWS respuestaWS = new CEN_RespuestaWS();    //clase respuestaWS
            CEN_RespuestaWSRegPreventa respuestaWSPreventa = new CEN_RespuestaWSRegPreventa();    //clase respuestaWSRegPreventa
            CAD_Preventa CADPreventa = new CAD_Preventa();          //clase acceso a datos preventa
            CAD_Cliente CADCliente = new CAD_Cliente();             //clase acceso a datos cliente
            CAD_Consulta CADConsulta = new CAD_Consulta();             //clase acceso a datos consultas
            CAD_PuntoEntrega CADEntrega = new CAD_PuntoEntrega();             //clase acceso a datos punto entrega
            CEN_Request cliente;
            CEN_RequestPuntoEntrega entrega;
            RptaRegistroPreventa rp;

            RptaRegistroPuntoEntrega re = new RptaRegistroPuntoEntrega();

            string numDoc = CEN_Constantes.g_const_vacio;
            int codPersona = CEN_Constantes.g_const_0;
            int ntraPuntoEntrega = CEN_Constantes.g_const_0;
            int contador = CEN_Constantes.g_const_0;
            try
            {
                if (requestPreventa.proceso == CEN_Constantes.g_const_1)
                {
                    //buscar existencia de cliente
                    cliente = requestPreventa.cliente;
                    entrega = requestPreventa.entrega;


                    contador = CEN_Constantes.g_const_0;
                    if (cliente.tipoPersona == CEN_Constantes.g_const_2)
                    {
                        numDoc = cliente.ruc;
                        contador = CADCliente.countPorFlag(CEN_Constantes.g_const_3, cliente.ruc);
                    }
                    else
                    {
                        numDoc = cliente.numDocumento;
                        contador = CADCliente.countPorFlag(CEN_Constantes.g_const_2, cliente.numDocumento);
                    }

                    //si cliente no existe, lo registramos
                    if (contador == CEN_Constantes.g_const_0)
                    {
                        respuestaWS = CADCliente.registro_modificacion_cliente(cliente);
                        if (respuestaWS.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                        {
                            //string data = Convert.ToString(respuestaWS.Resultado);
                            //codPersona = JsonConvert.DeserializeObject<int>(data);
                            string jsonData = JsonConvert.SerializeObject(respuestaWS.Resultado);
                            RootObjectRegMod dataRegMod = JsonConvert.DeserializeObject<RootObjectRegMod>(jsonData);
                            codPersona = dataRegMod.reg_mod.codCliente;
                            ntraPuntoEntrega = dataRegMod.reg_mod.codPuntoEntrega;
                        }
                        else
                        {
                            respuestaWSPreventa.ErrorWebSer = respuestaWS.ErrorWebSer;
                            return respuestaWSPreventa;
                        }
                        requestPreventa.codPuntoEntrega = ntraPuntoEntrega;
                    }
                    else //si cliente existe obtenemos codigo de persona
                    {
                        //verificamos que exista el codigo punto entrega
                        contador = CEN_Constantes.g_const_0;
                        contador = CADCliente.countPorFlag(CEN_Constantes.g_const_5, requestPreventa.codPuntoEntrega.ToString());
                        if (contador == CEN_Constantes.g_const_0)
                        {
                            respuestaWSPreventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            respuestaWSPreventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1041;
                            respuestaWSPreventa.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1041);
                            return respuestaWSPreventa;
                        }

                        codPersona = CADCliente.obtenerCodigoPersona(cliente.tipoDocumento, numDoc);

                        //verificamos que el codigo punto entrega le pertenezca al cliente
                        contador = CEN_Constantes.g_const_0;
                        contador = CADConsulta.countPorFlag(CEN_Constantes.g_const_1, requestPreventa.codPuntoEntrega.ToString(), codPersona.ToString().Trim());
                        if (contador == CEN_Constantes.g_const_0)
                        {
                            respuestaWSPreventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                            respuestaWSPreventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1042;
                            respuestaWSPreventa.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1042);
                            return respuestaWSPreventa;
                        }
                    }
                    requestPreventa.codCliente = codPersona;


                }
                else
                {
                    //verificar estado de preventa


                    //verificar si existe la preventa
                    contador = CEN_Constantes.g_const_0;
                    contador = CADConsulta.countPorFlag(CEN_Constantes.g_const_2, requestPreventa.ntraPreventa.ToString(), CEN_Constantes.g_const_vacio);
                    if (contador == CEN_Constantes.g_const_0) // no existe preventa
                    {
                        respuestaWSPreventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWSPreventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1043;
                        respuestaWSPreventa.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1043);
                        return respuestaWSPreventa;
                    }

                    //verificamos que exista el codigo punto entrega
                    contador = CEN_Constantes.g_const_0;
                    contador = CADCliente.countPorFlag(CEN_Constantes.g_const_5, requestPreventa.codPuntoEntrega.ToString());
                    if (contador == CEN_Constantes.g_const_0)
                    {
                        respuestaWSPreventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWSPreventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1041;
                        respuestaWSPreventa.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1041);
                        return respuestaWSPreventa;
                    }

                    //verificar si punto entrega pertenece a cliente
                    contador = CEN_Constantes.g_const_0;
                    contador = CADConsulta.countPorFlag(CEN_Constantes.g_const_1, requestPreventa.codPuntoEntrega.ToString(), requestPreventa.codCliente.ToString().Trim());
                    if (contador == CEN_Constantes.g_const_0)
                    {
                        respuestaWSPreventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWSPreventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1042;
                        respuestaWSPreventa.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1042);
                        return respuestaWSPreventa;
                    }

                    //verificar si cliente existe (codPersona)
                    contador = CEN_Constantes.g_const_0;
                    contador = CADConsulta.countPorFlag(CEN_Constantes.g_const_3, requestPreventa.codCliente.ToString(), CEN_Constantes.g_const_vacio);
                    if (contador == CEN_Constantes.g_const_0)
                    {
                        respuestaWSPreventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWSPreventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1030;
                        respuestaWSPreventa.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1030);
                        return respuestaWSPreventa;
                    }

                    //verificar si preventa pertenece a cliente
                    contador = CEN_Constantes.g_const_0;
                    contador = CADConsulta.countPorFlag(CEN_Constantes.g_const_4, requestPreventa.ntraPreventa.ToString(), requestPreventa.codCliente.ToString());
                    if (contador == CEN_Constantes.g_const_0)
                    {
                        respuestaWSPreventa.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWSPreventa.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1044;
                        respuestaWSPreventa.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1044);
                        return respuestaWSPreventa;
                    }


                }
                //requestPreventa.entrega.codPersona = codPersona;

                //registramos punto entrega
                /*if(entrega.ntraPuntoEntrega == CEN_Constantes.g_const_0)
                {
                    respuestaWS = CADEntrega.registro_modificacion_punto_entrega(entrega);
                    if (respuestaWS.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                    {
                        string data = respuestaWS.Resultado.ToString();
                        string jsonData = JsonConvert.SerializeObject(respuestaWS.Resultado);
                        re = new RptaRegistroPuntoEntrega();
                        RootObject data2 = JsonConvert.DeserializeObject<RootObject>(jsonData);
                        int valor = data2.result.ntraPuntoEntrega;
                        ntraPuntoEntrega = data2.result.ntraPuntoEntrega;
                        
                    }
                    else
                    {
                        respuestaWSPreventa.ErrorWebSer = respuestaWS.ErrorWebSer;
                        return respuestaWSPreventa;
                    }
                }
                else
                {
                    ntraPuntoEntrega = entrega.ntraPuntoEntrega;
                }

                requestPreventa.codPuntoEntrega = ntraPuntoEntrega;
                */

                //registramos preventa
                respuestaWSPreventa = CADPreventa.registro_modificacion_preventa(requestPreventa);
                if (respuestaWSPreventa.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    string jsonData = JsonConvert.SerializeObject(respuestaWSPreventa.ResultadoRegPreventa);
                    //string data = Convert.ToString(respuestaWSPreventa.ResultadoRegPreventa);
                    RootObject_2 ntraPreventa_2 = JsonConvert.DeserializeObject<RootObject_2>(jsonData);
                    rp = new RptaRegistroPreventa();
                    rp.ntraPreventa = ntraPreventa_2.rp.ntraPreventa;
                    rp.ntraPuntoEntrega = ntraPuntoEntrega;
                    rp.codPersona = codPersona;

                    respuestaWSPreventa.ResultadoRegPreventa = new { rp };
                }

                return respuestaWSPreventa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class Result
        {
            public int ntraPuntoEntrega { get; set; }
        }

        public class RootObject
        {
            public Result result { get; set; }
        }


        public class Rp
        {
            public int ntraPreventa { get; set; }
            public int codPersona { get; set; }
            public int ntraPuntoEntrega { get; set; }
        }

        public class RootObject_2
        {
            public Rp rp { get; set; }
        }

        public class RegMod
        {
            public int codCliente { get; set; }
            public int codPuntoEntrega { get; set; }
        }

        public class RootObjectRegMod
        {
            public RegMod reg_mod { get; set; }
        }

        public CEN_RespuestaWS anular_preventa(CEN_RequestAnularPreventa requestAP, string strObj)
        {
            //DESCRIPCION: LOGICA ANULAR PREVENTA
            CEN_RespuestaWS respuestaWS = new CEN_RespuestaWS();    //clase respuestaWS
            CAD_Preventa CADPreventa = new CAD_Preventa();          //clase acceso a datos preventa
            CAD_Cliente CADCliente = new CAD_Cliente();            //clase cliente
            CAD_Consulta CADConsulta = new CAD_Consulta();          //clase consultas
            int contador;
            try
            {
                //validar si preventa existe 
                contador = CEN_Constantes.g_const_0;
                contador = CADConsulta.countPorFlag(CEN_Constantes.g_const_2, requestAP.ntraPreventa.ToString(), CEN_Constantes.g_const_vacio);
                if (contador == CEN_Constantes.g_const_0) // no existe preventa
                {
                    respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1043;
                    respuestaWS.ErrorWebSer.DescripcionErr = CADCliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1043);
                    return respuestaWS;
                }

                respuestaWS = CADPreventa.anular_preventa(requestAP);
                return respuestaWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
