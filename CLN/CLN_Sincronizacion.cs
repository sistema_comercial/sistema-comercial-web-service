﻿using CAD;
using CEN;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CLN.CLN_Preventa;

namespace CLN
{
    public class CLN_Sincronizacion
    {
        public CEN_RespuestaWSSinc Sincronizacion(CEN_Request_Sincronizacion datos, int respuesta)
        {
            CEN_RespuestaWSSinc rpta_sincronizacion = new CEN_RespuestaWSSinc();
            CEN_Response_Sincronizacion data_sincronizacion = new CEN_Response_Sincronizacion();

            CEN_Data_Vendedor data_vendedor = new CEN_Data_Vendedor();
            CEN_Data_Cliente data_cliente = new CEN_Data_Cliente();
            CEN_Data_Ruta data_ruta = new CEN_Data_Ruta();
            CEN_Data_Categoria data_categoria = new CEN_Data_Categoria();
            CEN_Data_Producto data_producto = new CEN_Data_Producto();
            CEN_Data_Presentacion data_Presentacion = new CEN_Data_Presentacion();
            CEN_Data_Promocion data_Promocion = new CEN_Data_Promocion();
            CEN_Data_Promocion data_Promocion_Desc = new CEN_Data_Promocion();
            CEN_Data_Almacen data_almacen = new CEN_Data_Almacen();
            CEN_Data_Inventario data_inventario = new CEN_Data_Inventario();
            CEN_Data_Descuento data_descuento = new CEN_Data_Descuento();
            CEN_Data_PuntosEntrega data_puntoent = new CEN_Data_PuntosEntrega();
            CEN_Data_Preventa data_preventa = new CEN_Data_Preventa();
            CEN_Data_Detalle_Preventa data_detPreventa = new CEN_Data_Detalle_Preventa();
            CEN_Data_Preventa_Promocion data_preventa_promo = new CEN_Data_Preventa_Promocion();
            CEN_Data_Preventa_Descuento data_preventa_desc = new CEN_Data_Preventa_Descuento();
            CEN_Data_Parametro data_parametro = new CEN_Data_Parametro();
            CEN_Data_Precio data_precio = new CEN_Data_Precio();
            CEN_Data_Concepto data_concepto = new CEN_Data_Concepto();
            CEN_Data_Localizacion data_Localizacion = new CEN_Data_Localizacion();
            List<CEN_Departamento> listDepartamentos = new List<CEN_Departamento>();
            List<CEN_Provincia> listProvincias = new List<CEN_Provincia>();
            List<CEN_Distrito> listDistritos = new List<CEN_Distrito>();

            int band = CEN_Constantes.g_const_0;
            string listError = CEN_Constantes.g_const_vacio;

            CAD_Vendedor cAD_Vendedor = new CAD_Vendedor();
            CAD_Cliente cad_Cliente = new CAD_Cliente();
            CAD_Sincronizacion cad_sincronizacion = new CAD_Sincronizacion();
            CAD_Productos cad_producto = new CAD_Productos();
            CAD_Promociones cad_promociones = new CAD_Promociones();
            CAD_Descuentos cad_descuentos = new CAD_Descuentos();
            CAD_Preventa cad_preventa = new CAD_Preventa();
            CAD_Ubigeo cad_ubigeo = new CAD_Ubigeo();
            CAD_BitacorasRutas cad_bitacora = new CAD_BitacorasRutas();


            try
            {
                //Listar categorias
                data_categoria = cad_sincronizacion.obtenerCategorias();
                if (data_categoria.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listCategorias = data_categoria.listCategoria;
                }
                else if (data_categoria.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_categoria.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }

                //Obtener vendedor
                data_vendedor = cAD_Vendedor.obtenerVendedor(datos);
                if (data_vendedor.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.vendedor = data_vendedor.vendedor;

                }
                else if (data_vendedor.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_vendedor.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }

                //Obtener ruta actual de vendedor
                data_ruta = cad_Cliente.obtenerRutaVendedor(datos);
                if (data_ruta.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    //buscar clientes por la ruta del vendedor
                    data_sincronizacion.vendedor.codRuta = data_ruta.ruta.ntraRutas;
                    data_sincronizacion.vendedor.descRuta = data_ruta.ruta.descRuta;
                    data_cliente = cad_Cliente.obtenerClientesxRutas(data_ruta.ruta.ntraRutas);
                    if (data_ruta.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                    {
                        data_sincronizacion.listClientes = data_cliente.listCliente;
                        if (data_cliente.listCliente.Count > CEN_Constantes.g_const_0)
                        {
                            //buscamos las localizaciones de los clientes
                            data_Localizacion = cad_bitacora.LocalizacionxClienteSinc(data_cliente.listCliente);
                            if (data_ruta.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                            {
                                data_sincronizacion.listLocalizacion = data_Localizacion.listLocalizacion;
                            }
                        }
                    }
                    else if (data_cliente.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                    {
                        band++;
                        listError = listError + data_cliente.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                    }
                }
                else if (data_ruta.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_ruta.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }


                //productos
                data_producto = cad_producto.obtenerListaProductos();
                if (data_producto.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listProductos = data_producto.listProducto;
                }
                else if (data_producto.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_producto.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }

                //presentaciones de productos
                data_Presentacion = cad_producto.obtenerListaPresentaciones();
                if (data_Presentacion.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listPresentaciones = data_Presentacion.listPresentacion;
                }
                else if (data_Presentacion.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_Presentacion.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }
                //precios
                data_precio = cad_sincronizacion.obtenerlistaPrecios();
                if (data_precio.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listPrecios = data_precio.listPrecio;
                }
                else if (data_precio.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_precio.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }
                //conceptps
                data_concepto = cad_sincronizacion.obtenerlistaConceptos();
                if (data_concepto.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listConceptos = data_concepto.listConcepto;
                }
                else if (data_concepto.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_concepto.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }

                //promociones
                data_Promocion = cad_promociones.obtenerPromociones(DateTime.Now, CEN_Constantes.g_const_1, CEN_Constantes.g_const_0);
                if (data_Promocion.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listPromociones = data_Promocion.listPromocion;
                }
                else if (data_Promocion.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_Promocion.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }

                //Descripcion de todas las promociones
                data_Promocion_Desc = cad_promociones.obtenerPromocionesDescripcion();
                if (data_Promocion_Desc.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listPromocionesDesc = data_Promocion_Desc.listPromocion;
                }

                //descuentos
                data_descuento = cad_descuentos.obtenerDescuentos(DateTime.Now, CEN_Constantes.g_const_1, CEN_Constantes.g_const_0);
                if (data_descuento.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listDescuentos = data_descuento.listDescuento;
                }
                else if (data_descuento.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_descuento.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }
                //almacen
                data_almacen = cad_sincronizacion.obtenerAlmacenes();
                if (data_Promocion.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listAlmacenes = data_almacen.listAlmacen;
                }
                else if (data_almacen.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_almacen.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }
                //inventario
                data_inventario = cad_sincronizacion.obtenerInventarios();
                if (data_inventario.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listInventarios = data_inventario.listInventario;
                }
                else if (data_inventario.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_inventario.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }
                //Puntos de entrega
                data_puntoent = cad_sincronizacion.obtenerlistaPuntosEntrega();
                if (data_puntoent.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listEntregas = data_puntoent.listEntrega;
                }
                else if (data_puntoent.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_puntoent.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }

                //Parametros
                data_parametro = cad_sincronizacion.obtenerParametros();
                if (data_parametro.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listParametros = data_parametro.listParametro;
                }
                else if (data_parametro.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_parametro.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }

                /*
                string codDep = CEN_Constantes.g_const_vacio;
                string codProv = CEN_Constantes.g_const_vacio;
                string codDis = CEN_Constantes.g_const_vacio;
                if (datos.codUbigeo.Trim().Length == CEN_Constantes.g_const_6 )
                {
                    //Departamentos
                    codDep = datos.codUbigeo.Substring(CEN_Constantes.g_const_0, CEN_Constantes.g_const_2);
                    codProv = datos.codUbigeo.Substring(CEN_Constantes.g_const_2, CEN_Constantes.g_const_2);
                    codDis = CEN_Constantes.g_const_00;
                    listDepartamentos = cad_ubigeo.ObtenerDepartamentos(codDep);
                    data_sincronizacion.listDepartamentos = listDepartamentos;
                    //Provincias
                    listProvincias = cad_ubigeo.ObtenerProvincias(codDep, codProv);
                    data_sincronizacion.listProvincias = listProvincias;
                    //Distritos
                    listDistritos = cad_ubigeo.ObtenerDistritos(codDep, codProv, codDis);
                    data_sincronizacion.listDistritos = listDistritos;
                }
                */


                //preventa
                DateTime fecha_preventa = DateTime.Now;
                fecha_preventa = fecha_preventa.AddMonths(CEN_Constantes.g_const_menos1);
                data_preventa = cad_preventa.listaPreventasVendedor(datos.codVendedor, fecha_preventa);
                if (data_preventa.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    data_sincronizacion.listPreventas = data_preventa.listPreventa;
                }
                else if (data_preventa.ErrorWebSer.CodigoErr != CEN_Constantes.g_const_1000)
                {
                    band++;
                    listError = listError + data_preventa.ErrorWebSer.DescripcionErr + CEN_Constantes.g_const_espacio + CEN_Constantes.g_const_guion + CEN_Constantes.g_const_vacio;
                }
                if (band > CEN_Constantes.g_const_0)
                {
                    rpta_sincronizacion.ErrorWebSer.DescripcionErr = listError;
                }
                else
                {
                    rpta_sincronizacion.ErrorWebSer.DescripcionErr = "Consulta exitosa";
                }
                rpta_sincronizacion.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                rpta_sincronizacion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                data_sincronizacion.DescripcionResp = "Consulta exitosa";

                data_sincronizacion.estadoUsuario = respuesta;
                /*
                if (band > CEN_Constantes.g_const_0)
                {
                    rpta_sincronizacion.ErrorWebSer.DescripcionErr = listError;
                    rpta_sincronizacion.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_3000;
                    rpta_sincronizacion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_1;
                    data_sincronizacion.DescripcionResp = "HUBO UN PROBLEMA INTERNO";
                }
                else
                {
                    rpta_sincronizacion.ErrorWebSer.DescripcionErr = "Consulta exitosa";
                    rpta_sincronizacion.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                    rpta_sincronizacion.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    data_sincronizacion.DescripcionResp = "Consulta exitosa";
                }
                */
                rpta_sincronizacion.ResultadoSincro = new { data_sincronizacion };
                return rpta_sincronizacion;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public List<CEN_Data_Entrega> registroPunEntregaSincro(List<CEN_RequestPuntoEntrega> listPuntoEntrea)
        {
            CEN_RespuestaWS respuestaWS = new CEN_RespuestaWS();    //clase respuestaWS
            CAD_Cliente CADCliente = new CAD_Cliente();             //clase acceso a datos cliente
            CAD_PuntoEntrega CADEntrega = new CAD_PuntoEntrega();             //clase acceso a datos punto entrega
            RptaRegistroPuntoEntrega re = new RptaRegistroPuntoEntrega();
            List<CEN_Data_Entrega> lsit_data_ent = new List<CEN_Data_Entrega>();
            CEN_Data_Entrega data_ent = new CEN_Data_Entrega();
            try
            {
                for (int i = 0; i < listPuntoEntrea.Count; i++)
                {
                    listPuntoEntrea[i].codPersona = CADCliente.obtenerCodigoPersona(listPuntoEntrea[i].tipoDocumento, listPuntoEntrea[i].numeroDocumento);
                    if (listPuntoEntrea[i].ntraPuntoEntrega == CEN_Constantes.g_const_0)
                    {
                        respuestaWS = CADEntrega.registro_modificacion_punto_entrega(listPuntoEntrea[i]);
                        if (respuestaWS.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                        {
                            data_ent = new CEN_Data_Entrega();
                            int ntraPuntoEntrega = CEN_Constantes.g_const_0;
                            string data = respuestaWS.Resultado.ToString();
                            string jsonData = JsonConvert.SerializeObject(respuestaWS.Resultado);
                            re = new RptaRegistroPuntoEntrega();
                            RootObject data2 = JsonConvert.DeserializeObject<RootObject>(jsonData);
                            ntraPuntoEntrega = data2.result.ntraPuntoEntrega;
                            data_ent.ntraAnt = listPuntoEntrea[i].ntraPuntoEntrega;
                            data_ent.ntraLocal = listPuntoEntrea[i].idLocal;
                            data_ent.ntraServ = ntraPuntoEntrega;
                            lsit_data_ent.Add(data_ent);
                        }
                    }
                    else
                    {
                        data_ent.ntraAnt = listPuntoEntrea[i].ntraPuntoEntrega;
                        data_ent.ntraLocal = listPuntoEntrea[i].idLocal;
                        data_ent.ntraServ = listPuntoEntrea[i].ntraPuntoEntrega;
                        lsit_data_ent.Add(data_ent);
                    }


                }
                return lsit_data_ent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CEN_RespuestaWS registroPreventaSincro(List<CEN_Preventa> listPreventas, List<CEN_Data_Entrega> list_entregas, List<CEN_Data_ClienteSinc> lisClientesSinc)
        {
            CEN_RespuestaWS respuestaWS = new CEN_RespuestaWS();    //clase respuestaWS
            CEN_RespuestaWSRegPreventa respuestaWSPreventa = new CEN_RespuestaWSRegPreventa();    //clase respuestaWSRegPreventa
            CEN_RequestPreventa det_prev = new CEN_RequestPreventa();
            CAD_Cliente CADCliente = new CAD_Cliente();             //clase acceso a datos cliente
            CAD_Preventa CADPreventa = new CAD_Preventa();          //clase acceso a datos preventa

            CEN_RequestAnularPreventa requestAP = new CEN_RequestAnularPreventa();
            CLN_Preventa cln_preventa = new CLN_Preventa();
            CEN_RespuestaWS cEN_RespuestaWS = new CEN_RespuestaWS();

            RptaRegistroPreventa rp;
            try
            {
                for (int i = CEN_Constantes.g_const_0; i < listPreventas.Count; i++)
                {
                    det_prev = LlenarDataPreventa(listPreventas[i]);
                    //Obtener codigo persona
                    det_prev.codCliente = CADCliente.obtenerCodigoPersona(listPreventas[i].tipoDocumento, listPreventas[i].numeroDocumento);

                    //Verificar su el cliente es nuevo para modificar el punto de entrega
                    for (int y = CEN_Constantes.g_const_0; y < lisClientesSinc.Count; y++)
                    {
                        if (det_prev.numeroDocumento == lisClientesSinc[y].numDocumento || det_prev.numeroDocumento == lisClientesSinc[y].ruc)
                        {
                            det_prev.codPuntoEntrega = lisClientesSinc[y].codPuntoEntrega;
                        }
                    }
                    /*
                    //Obtener punto de entrega
                    //verifico si existe el id local para poder cambiarlo con el id del servidor 
                    for (int y = CEN_Constantes.g_const_0; y < list_entregas.Count; y++)
                    {
                        //si no existiera se qeudaria con us id punto de entrega
                        if (det_prev.codPuntoEntrega == list_entregas[y].ntraLocal && list_entregas[y].ntraAnt == CEN_Constantes.g_const_0)
                        {
                            det_prev.codPuntoEntrega = list_entregas[y].ntraServ;
                        }
                    }
                    */

                    if (det_prev.proceso == CEN_Constantes.g_const_3)
                    {
                        //anular preventa
                        requestAP.ntraPreventa = det_prev.ntraPreventa;
                        cEN_RespuestaWS = cln_preventa.anular_preventa(requestAP, JsonConvert.SerializeObject(requestAP));
                    }
                    else
                    {
                        //Registrar/modificar preventa
                        respuestaWSPreventa = CADPreventa.registro_modificacion_preventa(det_prev);
                        if (respuestaWSPreventa.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                        {
                            string jsonData = JsonConvert.SerializeObject(respuestaWSPreventa.ResultadoRegPreventa);
                            //string data = Convert.ToString(respuestaWSPreventa.ResultadoRegPreventa);
                            RootObject_2 ntraPreventa_2 = JsonConvert.DeserializeObject<RootObject_2>(jsonData);
                            rp = new RptaRegistroPreventa();
                            rp.ntraPreventa = ntraPreventa_2.rp.ntraPreventa;
                            //rp.ntraPuntoEntrega = ntraPuntoEntrega;
                            //rp.codPersona = codPersona;

                            respuestaWSPreventa.ResultadoRegPreventa = new { rp };
                        }
                    }




                }
                return respuestaWS;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private CEN_RequestPreventa LlenarDataPreventa(CEN_Preventa request)
        {
            CEN_RequestPreventa preventa = new CEN_RequestPreventa();
            try
            {
                preventa.proceso = request.proceso;
                preventa.codCliente = request.codCliente;
                preventa.codPuntoEntrega = request.codPuntoEntrega;
                preventa.codUsuario = request.codUsuario;
                preventa.estado = request.estado;
                preventa.fecha = request.fecha;
                preventa.fechaEntrega = request.fechaEntrega;
                preventa.fechaPago = request.fechaPago;
                preventa.flagRecargo = request.flagRecargo;
                preventa.igv = request.igv;
                preventa.ip = request.ip;
                preventa.isc = request.isc;
                preventa.listDetPreventa = request.listDetPreventa;
                preventa.listPrevDescuento = request.listPrevDescuento;
                preventa.listPrevPromocion = request.listPrevPromocion;
                preventa.mac = request.mac;
                preventa.ntraPreventa = request.ntraPreventa;
                preventa.recargo = request.recargo;
                preventa.tipoDocumentoVenta = request.tipoDocumentoVenta;
                preventa.tipoMoneda = request.tipoMoneda;
                preventa.tipoVenta = request.tipoVenta;
                preventa.total = request.total;
                preventa.usuario = request.usuario;
                preventa.codSucursal = request.codSucursal;
                preventa.horaEntrega = request.horaEntrega;
                preventa.origenVenta = request.origenVenta;
                preventa.numeroDocumento = request.numeroDocumento;
                return preventa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CEN_Data_ClienteSinc> registroClienteSincro(List<CEN_Request> listClientes)
        {
            //DESCRIPCION: LOGICA REGISTRAR CLIENTE
            CEN_RespuestaWS respuestaWS = new CEN_RespuestaWS();    //clase respuestaWS
            CAD_Cliente CADCliente = new CAD_Cliente();             //clase acceso a datos cliente
            CLN_Cliente cliente = new CLN_Cliente();
            List<CEN_Data_ClienteSinc> listCliente = new List<CEN_Data_ClienteSinc>();
            CEN_Data_ClienteSinc data_clientSinc = new CEN_Data_ClienteSinc();
            RptaRegistroCliente rc = new RptaRegistroCliente();
            try
            {
                for (int i = CEN_Constantes.g_const_0; i < listClientes.Count; i++)
                {
                    respuestaWS = cliente.registro_modificacion_cliente(listClientes[i], JsonConvert.SerializeObject(listClientes[i]));
                    if (respuestaWS.Resultado != null)
                    {
                        string jsonData = JsonConvert.SerializeObject(respuestaWS.Resultado);
                        RootObjectClie dataRegMod = JsonConvert.DeserializeObject<RootObjectClie>(jsonData);
                        data_clientSinc = new CEN_Data_ClienteSinc();
                        data_clientSinc.codCliente = dataRegMod.reg_mod.codCliente;
                        data_clientSinc.codPuntoEntrega = dataRegMod.reg_mod.codPuntoEntrega;
                        data_clientSinc.tipoPersona = listClientes[i].tipoPersona;
                        data_clientSinc.numDocumento = listClientes[i].numDocumento;
                        data_clientSinc.ruc = listClientes[i].ruc;
                        listCliente.Add(data_clientSinc);
                    }

                }

                return listCliente;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private class RegMod
        {
            public int codCliente { get; set; }
            public int codPuntoEntrega { get; set; }
        }

        private class RootObjectClie
        {
            public RegMod reg_mod { get; set; }
        }

    }
}
