﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;

namespace CLN
{
    public class CLN_Ubigeo
    {
        public CEN_ResponseUbigeo ObtenerUbigeoData(CEN_RequestUbigeo request)
        {
            CEN_ResponseUbigeo data = new CEN_ResponseUbigeo();
            List<CEN_Departamento> listDepartamentos = new List<CEN_Departamento>();
            List<CEN_Provincia> listProvincias = new List<CEN_Provincia>();
            List<CEN_Distrito> listDistritos = new List<CEN_Distrito>();
            CAD_Ubigeo cad_ubigeo = new CAD_Ubigeo();
            string codDep = CEN_Constantes.g_const_vacio;
            string codProv = CEN_Constantes.g_const_vacio;
            string codDis = CEN_Constantes.g_const_vacio;
            try
            {
                codDep = request.codUbigeo.Substring(CEN_Constantes.g_const_0, CEN_Constantes.g_const_2);
                codProv = CEN_Constantes.g_const_00;
                codDis = CEN_Constantes.g_const_00;

                listDepartamentos = cad_ubigeo.ObtenerDepartamentos(codDep);
                data.listDepartamentos = listDepartamentos;
                //Provincias
                listProvincias = cad_ubigeo.ObtenerProvincias(codDep, codProv);
                data.listProvincias = listProvincias;
                //Distritos
                listDistritos = cad_ubigeo.ObtenerDistritos(codDep, codProv, codDis);
                data.listDistritos = listDistritos;
                data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                data.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                data.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_consultaExitosa;

                return data;

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
