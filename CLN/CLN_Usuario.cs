﻿using CAD;
using CEN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLN_Usuario
    {
        public int ValidarUsuarioActivo(int codUsu)
        {
            int respuesta = CEN_Constantes.g_const_0;
            CAD_Usuario cad_usuario = new CAD_Usuario();
            try
            {
                respuesta = cad_usuario.ValidarUsuarioActivo(codUsu);
                return respuesta;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
