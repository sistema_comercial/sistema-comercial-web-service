﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;

namespace CLN
{
    public class CLN_Sucursal
    {
        public CEN_RespuestaWSSucursal ObtenerSucursales()
        {
            //DESCRIPCION: LOGICA REGISTRAR RUTAS BITACORAS
            CEN_RespuestaWSSucursal respuestaWS = new CEN_RespuestaWSSucursal();    //clase respuestaWS
            CAD_Sucursal cad_sucursal = new CAD_Sucursal();             //clase acceso a datos rutas bitacoras

            try
            {

                respuestaWS = cad_sucursal.ObtenerSucursales();


                return respuestaWS;


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
