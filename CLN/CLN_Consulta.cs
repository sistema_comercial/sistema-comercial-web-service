﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLN
{
    public class CLN_Consulta
    {
        public string ConvertFechaDateToString(DateTime fecha)
        {
            try
            {
                return fecha.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
    }
}
