﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;

namespace CLN
{
    public class CLN_BitacorasRutas
    {


        public CEN_RespuestaBitacoraWS RegistrarRutasBitacoras(CEN_RequestBitacorasRutas ObjBitRu, string strObj)
        {
            //DESCRIPCION: LOGICA REGISTRAR RUTAS BITACORAS
            CEN_RespuestaBitacoraWS respuestaWS = new CEN_RespuestaBitacoraWS();    //clase respuestaWS
            CAD_BitacorasRutas CADBitRut = new CAD_BitacorasRutas();             //clase acceso a datos rutas bitacoras

            try
            {

                    respuestaWS = validarRutasBitacoras(ObjBitRu);
                    if (respuestaWS.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                    {
                        respuestaWS = CADBitRut.RegistrarRutasBitacoras(ObjBitRu);
                    }
                

                return respuestaWS;


            }
            catch(Exception ex)
            {

                throw ex;
            }


        }

        public CEN_RespuestaBitacoraWS validarRutasBitacoras(CEN_RequestBitacorasRutas ObjBitRu)
        {


            CEN_RespuestaBitacoraWS respuestaWS = new CEN_RespuestaBitacoraWS();
            CEN_Concepto desConcepto = new CEN_Concepto();
            CLN_Concepto clnConcepto = new CLN_Concepto();
            desConcepto.codConcepto = CEN_Constantes.g_const_100; //codigo de concepto para los errores
            short codErr = CEN_Constantes.g_const_2000;             //codigo de error
            short flag = CEN_Constantes.g_const_0;                  //flag de error
            

            try
            {
                /*respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;*/
                //codigo de ruta
                if (ObjBitRu == null)
                {

                    desConcepto.correlativo = CEN_Constantes.g_const_1037;
                    flag = CEN_Constantes.g_const_1;
                    codErr = CEN_Constantes.g_const_1037;

                    respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    respuestaWS.ErrorWebSer.CodigoErr = codErr;
                    desConcepto.correlativo = codErr;
                    respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                    return respuestaWS;

                }

                if (flag == CEN_Constantes.g_const_0)
                {
                    //para validar el formato de la fecha
                    CultureInfo culture;
                    DateTimeStyles styles;
                    DateTime dateResult;
                    culture = CultureInfo.CreateSpecificCulture("es-PE");
                    styles = DateTimeStyles.None;
                    string pattern = "dd/MM/yyyy";
                   
                    //codigo de ruta
                    if (ObjBitRu.codRuta <= 0 || Convert.ToString(ObjBitRu.codRuta) == "")
                    {

                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1036;
                        desConcepto.correlativo = CEN_Constantes.g_const_1036;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;
                    }
                    //fecha
                   
                    if ( !(DateTime.TryParseExact(ObjBitRu.fecha, pattern, culture, styles, out dateResult)))
                    {
                        
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1032;
                        desConcepto.correlativo = CEN_Constantes.g_const_1032;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;
                        
                    }
                    //visita
                    if (!(ObjBitRu.visita == 1 || ObjBitRu.visita == 2 ) )
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1033;
                        desConcepto.correlativo = CEN_Constantes.g_const_1033;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;

                        
                    }
                    //motivo
                    if (!(ObjBitRu.motivo is string))
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1035;
                        desConcepto.correlativo = CEN_Constantes.g_const_1035;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;

                    }

                    if (ObjBitRu.motivo.Length > 500)
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1034;
                        desConcepto.correlativo = CEN_Constantes.g_const_1034;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;
                      
                    }

                    //usuario

                    if (ObjBitRu.usuario == null || ObjBitRu.usuario.Length == CEN_Constantes.g_const_0)
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1024;
                        desConcepto.correlativo = CEN_Constantes.g_const_1024;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;
                      

                    }

                    //ip

                    if (ObjBitRu.ip == null || ObjBitRu.ip.Length == CEN_Constantes.g_const_0)
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1025;
                        desConcepto.correlativo = CEN_Constantes.g_const_1025;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;
                       

                    }


                    //mac

                    if (ObjBitRu.mac == null || ObjBitRu.mac.Length == CEN_Constantes.g_const_0)
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1026;
                        desConcepto.correlativo = CEN_Constantes.g_const_1026;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;

                        

                    }
                    //Cordenada X

                  /*  if (ObjBitRu.cordenadaX == null || ObjBitRu.cordenadaX.Length == CEN_Constantes.g_const_0)
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1038;
                        desConcepto.correlativo = CEN_Constantes.g_const_1038;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;



                    }
                    //Cordenada Y

                    if (ObjBitRu.cordenadaY == null || ObjBitRu.cordenadaY.Length == CEN_Constantes.g_const_0)
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1039;
                        desConcepto.correlativo = CEN_Constantes.g_const_1039;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;



                    }*/

                }


                if (codErr == CEN_Constantes.g_const_2000)
                {
                    respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    respuestaWS.ErrorWebSer.CodigoErr = codErr;
                    respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return respuestaWS;

        }

    }
}
