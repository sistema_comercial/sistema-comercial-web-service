﻿using System;
using CAD;
using CEN;

namespace CLN
{
    public class CLN_Login
    {
        public CEN_RespuestaWSLogin ValidarLogin(CEN_Request_Login request, int ntraUsuario)
        {
            CEN_RespuestaWSLogin data = new CEN_RespuestaWSLogin();
            CAD_Login cad_login = new CAD_Login();
            CEN_DetalleSesionUsuario data_detSesion = new CEN_DetalleSesionUsuario();
            string desc_concepto = CEN_Constantes.g_const_vacio;
            CAD_Cliente cad_cliente = new CAD_Cliente();
            CEN_LoginFallido log1 = new CEN_LoginFallido();
            CEN_LoginFallido data_log = new CEN_LoginFallido();
            CEN_Parametro parametro = new CEN_Parametro();
            CEN_Parametro par_int_fall = new CEN_Parametro();
            CEN_Parametro par_sesiones_abi = new CEN_Parametro();
            CAD_Sincronizacion cAD_Sincronizacion = new CAD_Sincronizacion();
            CEN_DataSalidaLogin login_salida = new CEN_DataSalidaLogin();
            CEN_DataSalidaLogin bloqueo_usu = new CEN_DataSalidaLogin();
            int intentos = CEN_Constantes.g_const_0;
            try
            {
                data = cad_login.ValidarLogin(request, ntraUsuario);
                log1.codUsu = data.ntraUsuario;

                log1.fecha = DateTime.Now;
                log1.ip = CEN_Constantes.g_const_vacio;
                log1.mac = CEN_Constantes.g_const_vacio;

                if (data.ntraUsuario > CEN_Constantes.g_const_0 &&
                    data.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000 &&
                    data.respuesta == CEN_Constantes.g_const_0)
                {
                    //LOGIN CORRECTO
                    //Registro de intentos fallidos a 0

                    //VALIDAR PARAMETRIZACIÓN DE INTENTOS FALLIDOS
                    par_int_fall = cAD_Sincronizacion.obtenerParametroIndividual(CEN_Constantes.g_const_6);
                    if (par_int_fall.valorEntero1 == CEN_Constantes.g_const_1)
                    {
                        intentos = CEN_Constantes.g_const_0;
                        log1.cantFall = (short)intentos;
                        login_salida = cad_login.RegistrarLoginFallido(log1);
                    }


                    //VALIDAR PARAMETRIZACIÓN DE SESIONES ABIERTAS
                    par_sesiones_abi = cAD_Sincronizacion.obtenerParametroIndividual(CEN_Constantes.g_const_5);
                    if (par_sesiones_abi.valorEntero1 == CEN_Constantes.g_const_1)
                    {
                        data_detSesion = cad_login.ValidarSesionAbiertaUsu(data.ntraUsuario);
                        if (data_detSesion.contador > CEN_Constantes.g_const_0)
                        {
                            //YA EXISTE UNA SESION ABIERTA, Y NO PUEDES ENTRAR A LA APLICACION
                            desc_concepto = cad_cliente.obtener_descripcion_concepto(CEN_Constantes.g_const_100, CEN_Constantes.g_const_1040);
                            data.DescripcionResp = desc_concepto;
                            data.ErrorWebSer.DescripcionErr = desc_concepto;
                            data.respuesta = CEN_Constantes.g_const_1040;
                        }
                    }

                }
                else if (data.ntraUsuario > CEN_Constantes.g_const_0 &&
                    data.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_1000 &&
                    data.respuesta == CEN_Constantes.g_const_1)
                {
                    //LOGIN INCORRECTO CORRECTO, AGREGAR INTENTOS FALLIDOS

                    //VALIDAR PARAMETRIZACIÓN DE INTENTOS FALLIDOS
                    par_int_fall = cAD_Sincronizacion.obtenerParametroIndividual(CEN_Constantes.g_const_6);
                    if (par_int_fall.valorEntero1 == CEN_Constantes.g_const_1)
                    {
                        //Traer parametro de intentos fallidos
                        parametro = cAD_Sincronizacion.obtenerParametroIndividual(CEN_Constantes.g_const_4);
                        data_log = cad_login.ListarLoginFallidoUsu(data.ntraUsuario);
                        intentos = data_log.cantFall;
                        log1.cantFall = (short)intentos;
                        if (intentos + CEN_Constantes.g_const_1 > parametro.valorEntero1)
                        {
                            //BLOQUEAR USUARIO
                            bloqueo_usu = cad_login.BloquearUsuario(data.ntraUsuario);
                            //REINICIAR FALLIDOS A 0
                            intentos = CEN_Constantes.g_const_0;
                            log1.cantFall = (short)intentos;
                            login_salida = cad_login.RegistrarLoginFallido(log1);
                            data = cad_login.ValidarLogin(request, ntraUsuario);
                        }
                        else
                        {
                            //SUMAR 1 INTENTO FALLIDOS

                            intentos = intentos + CEN_Constantes.g_const_1;
                            log1.cantFall = (short)intentos;
                            login_salida = cad_login.RegistrarLoginFallido(log1);
                            data.DescripcionResp = data.DescripcionResp + CEN_Constantes.g_const_var_intento + intentos + CEN_Constantes.g_const_slash + parametro.valorEntero1;
                        }
                    }
                }

                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CEN_RespuestaWSLoginUsu ControlSesioninUsu(CEN_RequestLoginUsu request)
        {
            CEN_RespuestaWSLoginUsu data = new CEN_RespuestaWSLoginUsu();
            CEN_DetalleSesionUsuario data_login = new CEN_DetalleSesionUsuario();
            CAD_Login cad_login = new CAD_Login();

            try
            {
                //VALIDAMOS SESIONES ABIERTAS

                if (request.tipoRegistro == CEN_Constantes.g_const_1)
                {
                    data_login = cad_login.RegistroSesionUsu(request.codUsuario, request.tipoLogueo, DateTime.Now);
                }
                else
                {
                    data_login = cad_login.CerrarSesionUsu(request.codUsuario, DateTime.Now);
                }


                data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                data.ErrorWebSer.CodigoErr = data_login.codigo;
                data.ErrorWebSer.DescripcionErr = data_login.mensaje;
                data.DescripcionResp = data_login.mensaje;
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
