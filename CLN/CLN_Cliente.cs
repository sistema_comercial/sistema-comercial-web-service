﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;
namespace CLN
{
    public class CLN_Cliente
    {
        public CEN_RespuestaWS registro_modificacion_cliente(CEN_Request DatosCliente, string strObj)
        {
            //DESCRIPCION: LOGICA REGISTRAR CLIENTE
            CEN_RespuestaWS respuestaWS = new CEN_RespuestaWS();    //clase respuestaWS
            CAD_Cliente CADCliente = new CAD_Cliente();             //clase acceso a datos cliente
            try
            {
                if (DatosCliente != null)
                {
                    if (DatosCliente.ordenAtencion == CEN_Constantes.g_const_0)
                        DatosCliente.ordenAtencion = null;

                    if (DatosCliente.perfilCliente == CEN_Constantes.g_const_0)
                        DatosCliente.perfilCliente = null;

                    if (DatosCliente.clasificacion == CEN_Constantes.g_const_0)
                        DatosCliente.clasificacion = null;
                }
                respuestaWS = validarDatos(DatosCliente);
                if (respuestaWS.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    respuestaWS = CADCliente.registro_modificacion_cliente(DatosCliente);
                }
                
                return respuestaWS;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public CEN_RespuestaWS validarDatos(CEN_Request DatosCliente)
        {
            //DESCRIPCION: VALIDAR PARAMETROS DE ENTRADA
            CEN_RespuestaWS respuestaWS = new CEN_RespuestaWS();    //objeto respuesta
            CAD_Cliente c = new CAD_Cliente();                      //objeto acceso a datos cliente
            short codErr = CEN_Constantes.g_const_2000;             //codigo de error
            short flag = CEN_Constantes.g_const_0;                  //flag de error
            int cont = CEN_Constantes.g_const_0;                    //contador

            try
            {
                respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                if (DatosCliente == null)
                {
                    codErr = CEN_Constantes.g_const_1000;
                }
                else
                {
                    //proceso
                    if(!(DatosCliente.proceso == 1 || DatosCliente.proceso == 2)){
                        codErr = CEN_Constantes.g_const_1029;
                        flag = CEN_Constantes.g_const_1;
                    }

                    //codigo persona
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        cont = CEN_Constantes.g_const_0;
                        cont = c.countPorFlag(CEN_Constantes.g_const_4, DatosCliente.codPersona.ToString());
                        if (DatosCliente.proceso == CEN_Constantes.g_const_2)
                        {
                            if (cont == CEN_Constantes.g_const_0)
                            {
                                codErr = CEN_Constantes.g_const_1031;
                                flag = CEN_Constantes.g_const_1;
                            }
                        }
                    }

                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.tipoPersona == CEN_Constantes.g_const_1) //NATURAL
                        {
                            if (DatosCliente.numDocumento == null || DatosCliente.numDocumento.Length == CEN_Constantes.g_const_0)
                            {
                                codErr = CEN_Constantes.g_const_1003;
                                flag = CEN_Constantes.g_const_1;
                            }
                            else
                            {
                                //validar solo numeros
                                if (!validarSoloNumero(DatosCliente.numDocumento))
                                {
                                    codErr = CEN_Constantes.g_const_1004;
                                    flag = CEN_Constantes.g_const_1;
                                }
                                else
                                {
                                    if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_1 && DatosCliente.numDocumento.Length != CEN_Constantes.g_const_8)
                                    {
                                        codErr = CEN_Constantes.g_const_1005;
                                        flag = CEN_Constantes.g_const_1;
                                    }
                                    else
                                    {
                                        if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_2 && DatosCliente.numDocumento.Length != CEN_Constantes.g_const_12)
                                        {
                                            codErr = CEN_Constantes.g_const_1006;
                                            flag = CEN_Constantes.g_const_1;
                                        }
                                    }
                                }
                            }

                            if (flag == CEN_Constantes.g_const_0 && DatosCliente.proceso == CEN_Constantes.g_const_1)
                            {
                                if (c.countPorFlag(CEN_Constantes.g_const_2, DatosCliente.numDocumento) != CEN_Constantes.g_const_0)
                                {
                                    codErr = CEN_Constantes.g_const_1027;
                                    flag = CEN_Constantes.g_const_1;
                                }
                            }

                            //NOMBRES
                            if (flag == CEN_Constantes.g_const_0)
                            {
                                if (DatosCliente.nombres == null || DatosCliente.nombres.Length == CEN_Constantes.g_const_0)
                                {
                                    codErr = CEN_Constantes.g_const_1011;
                                    flag = CEN_Constantes.g_const_1;
                                }
                                else
                                {
                                    if (!validarSoloLetras(DatosCliente.nombres))
                                    {
                                        codErr = CEN_Constantes.g_const_1012;
                                        flag = CEN_Constantes.g_const_1;
                                    }
                                }
                            }

                            //APELLIDOS
                            if (flag == CEN_Constantes.g_const_0)
                            {
                                if (DatosCliente.apePaterno != null)
                                {
                                    if (!validarSoloLetras(DatosCliente.apePaterno))
                                    {
                                        codErr = CEN_Constantes.g_const_1013;
                                        flag = CEN_Constantes.g_const_1;
                                    }

                                }

                                if (flag == CEN_Constantes.g_const_0)
                                {
                                    if (DatosCliente.apeMaterno != null)
                                    {
                                        if (!validarSoloLetras(DatosCliente.apeMaterno))
                                        {
                                            codErr = CEN_Constantes.g_const_1014;
                                            flag = CEN_Constantes.g_const_1;
                                        }
                                    }
                                }
                            }

                        }
                        else // JURIDICA
                        {
                            if (DatosCliente.ruc == null || DatosCliente.ruc.Length == CEN_Constantes.g_const_0)
                            {
                                codErr = CEN_Constantes.g_const_1007;
                                flag = CEN_Constantes.g_const_1;
                            }
                            else
                            {
                                //validar solo numeros
                                if (!validarSoloNumero(DatosCliente.ruc))
                                {
                                    codErr = CEN_Constantes.g_const_1008;
                                    flag = CEN_Constantes.g_const_1;
                                }
                                else
                                {
                                    if (DatosCliente.ruc.Length != CEN_Constantes.g_const_11)
                                    {
                                        codErr = CEN_Constantes.g_const_1009;
                                        flag = CEN_Constantes.g_const_1;
                                    }
                                }
                            }
                            if (flag == CEN_Constantes.g_const_0 && DatosCliente.proceso == CEN_Constantes.g_const_1)
                            {
                                if (c.countPorFlag(CEN_Constantes.g_const_3, DatosCliente.ruc) != CEN_Constantes.g_const_0)
                                {
                                    codErr = CEN_Constantes.g_const_1028;
                                    flag = CEN_Constantes.g_const_1;
                                }
                            }

                            if (flag == CEN_Constantes.g_const_0)
                            {
                                if (DatosCliente.razonSocial == null || DatosCliente.razonSocial.Length == CEN_Constantes.g_const_0)
                                {
                                    codErr = CEN_Constantes.g_const_1010;
                                    flag = CEN_Constantes.g_const_1;
                                }
                            }

                        }

                    }

                    /*
                    //tipo persona
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        cont = CEN_Constantes.g_const_0;
                        cont = c.verificarExistenciaConceptos(CEN_Constantes.g_const_1, DatosCliente.tipoPersona);
                        if (cont == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1001;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //tipo documento
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        cont = CEN_Constantes.g_const_0;
                        cont = c.verificarExistenciaConceptos(CEN_Constantes.g_const_2, DatosCliente.tipoDocumento);
                        if (cont == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1002;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //Numero Documento y ruc
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if(DatosCliente.tipoDocumento == CEN_Constantes.g_const_1 || DatosCliente.tipoDocumento == CEN_Constantes.g_const_2)
                        {
                            if (DatosCliente.numDocumento == null || DatosCliente.numDocumento.Length == CEN_Constantes.g_const_0)
                            {
                                codErr = CEN_Constantes.g_const_1003;
                                flag = CEN_Constantes.g_const_1;
                            }
                            else
                            {
                                //validar solo numeros
                                if (!validarSoloNumero(DatosCliente.numDocumento))
                                {
                                    codErr = CEN_Constantes.g_const_1004;
                                    flag = CEN_Constantes.g_const_1;
                                }
                                else
                                {
                                    if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_1 && DatosCliente.numDocumento.Length != CEN_Constantes.g_const_8)
                                    {
                                        codErr = CEN_Constantes.g_const_1005;
                                        flag = CEN_Constantes.g_const_1;
                                    }
                                    else
                                    {
                                        if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_2 && DatosCliente.numDocumento.Length != CEN_Constantes.g_const_12)
                                        {
                                            codErr = CEN_Constantes.g_const_1006;
                                            flag = CEN_Constantes.g_const_1;
                                        }
                                    }
                                }
                            }
                            if (flag == CEN_Constantes.g_const_0 && DatosCliente.proceso == CEN_Constantes.g_const_1)
                            {
                                if(c.countPorFlag(CEN_Constantes.g_const_2, DatosCliente.numDocumento) != CEN_Constantes.g_const_0)
                                {
                                    codErr = CEN_Constantes.g_const_1027;
                                    flag = CEN_Constantes.g_const_1;
                                }
                            }

                            //validar que ruc sea nulo
                        }
                        else
                        {
                            if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_3)
                            {
                                if (DatosCliente.ruc == null || DatosCliente.ruc.Length == CEN_Constantes.g_const_0)
                                {
                                    codErr = CEN_Constantes.g_const_1007;
                                    flag = CEN_Constantes.g_const_1;
                                }
                                else
                                {
                                    //validar solo numeros
                                    if (!validarSoloNumero(DatosCliente.ruc))
                                    {
                                        codErr = CEN_Constantes.g_const_1008;
                                        flag = CEN_Constantes.g_const_1;
                                    }
                                    else
                                    {
                                        if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_3 && DatosCliente.ruc.Length != CEN_Constantes.g_const_11)
                                        {
                                            codErr = CEN_Constantes.g_const_1009;
                                            flag = CEN_Constantes.g_const_1;
                                        }
                                    }
                                }
                                if (flag == CEN_Constantes.g_const_0 && DatosCliente.proceso == CEN_Constantes.g_const_1)
                                {
                                    if (c.countPorFlag(CEN_Constantes.g_const_3, DatosCliente.ruc) != CEN_Constantes.g_const_0)
                                    {
                                        codErr = CEN_Constantes.g_const_1028;
                                        flag = CEN_Constantes.g_const_1;
                                    }
                                }
                                //validar que numero documento sea nulo
                            }
                        }


                        
                        
                    }

                    //Razon Social
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if(DatosCliente.tipoDocumento == CEN_Constantes.g_const_3)
                        {
                            if(DatosCliente.razonSocial == null || DatosCliente.razonSocial.Length == CEN_Constantes.g_const_0)
                            {
                                codErr = CEN_Constantes.g_const_1010;
                                flag = CEN_Constantes.g_const_1;
                            }
                        }
                    }

                    //Nombres
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_1 || DatosCliente.tipoDocumento == CEN_Constantes.g_const_2)
                        {
                            if (DatosCliente.nombres == null || DatosCliente.nombres.Length == CEN_Constantes.g_const_0)
                            {
                                codErr = CEN_Constantes.g_const_1011;
                                flag = CEN_Constantes.g_const_1;
                            }
                            else
                            {
                                if (!validarSoloLetras(DatosCliente.nombres))
                                {
                                    codErr = CEN_Constantes.g_const_1012;
                                    flag = CEN_Constantes.g_const_1;
                                }
                            }
                        }
                    }

                    //Apellidos
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.tipoDocumento == CEN_Constantes.g_const_1 || DatosCliente.tipoDocumento == CEN_Constantes.g_const_2)
                        {
                            if (DatosCliente.apePaterno != null)
                            {
                                if (!validarSoloLetras(DatosCliente.apePaterno))
                                {
                                    codErr = CEN_Constantes.g_const_1013;
                                    flag = CEN_Constantes.g_const_1;
                                }
                                
                            }

                            if (flag == CEN_Constantes.g_const_0)
                            {
                                if (DatosCliente.apeMaterno != null)
                                {
                                    if (!validarSoloLetras(DatosCliente.apeMaterno))
                                    {
                                        codErr = CEN_Constantes.g_const_1014;
                                        flag = CEN_Constantes.g_const_1;
                                    }
                                }
                            }
                                
                        }
                    }*/

                    //Direccion
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.direccion == null || DatosCliente.direccion.Length == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1015;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //Telefono
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.telefono != null)
                        {
                            if (!validarSoloNumero(DatosCliente.telefono))
                            {
                                codErr = CEN_Constantes.g_const_1016;
                                flag = CEN_Constantes.g_const_1;
                            }
                        }
                    }

                    //Celular
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.celular != null)
                        {
                            if (!validarSoloNumero(DatosCliente.celular))
                            {
                                codErr = CEN_Constantes.g_const_1017;
                                flag = CEN_Constantes.g_const_1;
                            }
                        }
                    }

                    //Ubigeo
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.ubigeo != null)
                        {
                            if (!validarSoloNumero(DatosCliente.ubigeo))
                            {
                                codErr = CEN_Constantes.g_const_1018;
                                flag = CEN_Constantes.g_const_1;
                            }
                        }
                    }

                    //ordenAtencion
                    //Perfil cliente
                    //clasificacion cliente

                    //frecuencia cliente
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        cont = CEN_Constantes.g_const_0;
                        cont = c.verificarExistenciaConceptos(CEN_Constantes.g_const_5, DatosCliente.frecuencia);
                        if (cont == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1021;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //lista precio
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        cont = CEN_Constantes.g_const_0;
                        cont = c.verificarExistenciaConceptos(CEN_Constantes.g_const_7, DatosCliente.tipoListaPrecio);
                        if (cont == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1022;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //codigo de ruta
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        cont = CEN_Constantes.g_const_0;
                        cont = c.countPorFlag(CEN_Constantes.g_const_1, DatosCliente.codRuta.ToString());
                        if (cont == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1023;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //usuario
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if(DatosCliente.usuario == null || DatosCliente.usuario.Length == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1024;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //ip
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.ip == null || DatosCliente.ip.Length == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1025;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }

                    //mac
                    if (flag == CEN_Constantes.g_const_0)
                    {
                        if (DatosCliente.mac == null || DatosCliente.mac.Length == CEN_Constantes.g_const_0)
                        {
                            codErr = CEN_Constantes.g_const_1026;
                            flag = CEN_Constantes.g_const_1;
                        }
                    }
                }

                if (codErr != CEN_Constantes.g_const_2000)
                {
                    respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    respuestaWS.ErrorWebSer.CodigoErr = codErr;
                    respuestaWS.ErrorWebSer.DescripcionErr = c.obtener_descripcion_concepto(CEN_Constantes.g_const_100, codErr).Trim();
                }

                return respuestaWS;

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Boolean validarSoloNumero(string cadena)
        {
            //DESCRIPCION: VALIDAR QUE LA CADENA RECIBIDA SOLO CONTENGA NUMEROS
            string l_caracter = CEN_Constantes.g_const_vacio;
            try
            {
                for (int i = CEN_Constantes.g_const_0; i < cadena.Length; i++)
                {
                    l_caracter = cadena.Substring(i, CEN_Constantes.g_const_1);
                    if (!l_caracter.All(char.IsDigit))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }

        public Boolean validarSoloLetras(string cadena)
        {
            //DESCRIPCION: VALIDAR QUE LA CADENA RECIBIDA SOLO CONTENGA LETRAS
            string l_caracter = CEN_Constantes.g_const_vacio;
            try
            {
                for (int i = CEN_Constantes.g_const_0; i < cadena.Length; i++)
                {
                    l_caracter = cadena.Substring(i, CEN_Constantes.g_const_1);
                    if (!(l_caracter.All(char.IsLetter) || (l_caracter.All(char.IsWhiteSpace))))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
