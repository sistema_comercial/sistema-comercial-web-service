﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;
namespace CLN
{
   public class CLN_ActualizarEstadoRutaAsignada
    {
        public CEN_RespuestaEstadoRutasAsigWS ActualizarEstadoRutaAsignada(CEN_RequestEstadoRutasAsig ObjEsRuA, string strObj)
        {
            //DESCRIPCION: LOGICA REGISTRAR RUTAS BITACORAS
            CEN_RespuestaEstadoRutasAsigWS respuestaWS = new CEN_RespuestaEstadoRutasAsigWS();    //clase respuestaWS
            CAD_ActualizarEstadoRutaAsignada AcRutAS = new CAD_ActualizarEstadoRutaAsignada();             //clase acceso a datos rutas bitacoras

            try
            {

                respuestaWS = validarEstadoRutasAsignadas(ObjEsRuA);
                if (respuestaWS.ErrorWebSer.CodigoErr == CEN_Constantes.g_const_2000)
                {
                    respuestaWS = AcRutAS.ActualizarEstadoRutaAsignada(ObjEsRuA);
                }


                return respuestaWS;


            }
            catch (Exception ex)
            {

                throw ex;
            }


        }


        public CEN_RespuestaEstadoRutasAsigWS validarEstadoRutasAsignadas(CEN_RequestEstadoRutasAsig ObjEsRuA)
        {


            CEN_RespuestaEstadoRutasAsigWS respuestaWS = new CEN_RespuestaEstadoRutasAsigWS();
            CEN_Concepto desConcepto = new CEN_Concepto();
            CLN_Concepto clnConcepto = new CLN_Concepto();
            desConcepto.codConcepto = CEN_Constantes.g_const_100; //codigo de concepto para los errores
            short codErr = CEN_Constantes.g_const_2000;             //codigo de error
            short flag = CEN_Constantes.g_const_0;                  //flag de error


            try
            {
                /*respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;*/
                //codigo de ruta
                if (ObjEsRuA == null)
                {

                    desConcepto.correlativo = CEN_Constantes.g_const_1037;
                    flag = CEN_Constantes.g_const_1;
                    codErr = CEN_Constantes.g_const_1037;

                    respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    respuestaWS.ErrorWebSer.CodigoErr = codErr;
                    desConcepto.correlativo = codErr;
                    respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                    return respuestaWS;

                }

                if (flag == CEN_Constantes.g_const_0)
                {
                    

                    //codigo de ruta
                    if (ObjEsRuA.codRuta <= 0 || Convert.ToString(ObjEsRuA.codRuta) == "")
                    {

                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1036;
                        desConcepto.correlativo = CEN_Constantes.g_const_1036;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;
                    }
                  

                 
                    //visita
                    if (!(ObjEsRuA.estado == 1 || ObjEsRuA.estado == 0))
                    {
                        respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                        respuestaWS.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_1033;
                        desConcepto.correlativo = CEN_Constantes.g_const_1033;
                        respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);

                        return respuestaWS;


                    }                

                }


                if (codErr == CEN_Constantes.g_const_2000)
                {
                    respuestaWS.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    respuestaWS.ErrorWebSer.CodigoErr = codErr;
                    respuestaWS.ErrorWebSer.DescripcionErr = clnConcepto.obtener_descripcion_concepto_general(desConcepto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return respuestaWS;

        }
    }
}
