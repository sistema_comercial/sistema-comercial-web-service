﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CAD;
using CEN;

namespace CLN
{
    public class CLN_Sunat
    {
        public CEN_ResponseClienSunat ObtenerDataSunatBD(CEN_RequestSunat request)
        {
            CEN_ResponseClienSunat data = new CEN_ResponseClienSunat();
            CAD_Sunat cad_sunat = new CAD_Sunat();
            CEN_Sunat dataCliente = new CEN_Sunat();
            CLN_Cliente clnCliente = new CLN_Cliente();
            bool estadoVald = false;
            try
            {
                //Validar numero de documento
                estadoVald = clnCliente.validarSoloNumero(request.numDocumento);
                if (estadoVald)
                {
                    dataCliente = cad_sunat.ObtenerDataSunatBD(request.numDocumento);
                    data.dataSunat = dataCliente;
                    data.ErrorWebSer.TipoErr = CEN_Constantes.g_const_0;
                    data.ErrorWebSer.CodigoErr = CEN_Constantes.g_const_2000;
                    data.ErrorWebSer.DescripcionErr = CEN_Constantes.g_const_consultaExitosa;
                }
                else
                {

                }

                

                return data;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
